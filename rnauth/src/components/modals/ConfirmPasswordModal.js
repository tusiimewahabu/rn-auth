import React, {useState} from 'react';
import {Modal} from 'react-native-paper';
import {Alert, TouchableOpacity, PixelRatio, View} from 'react-native';
import {Row, List, ListItem, Text} from 'native-base';
import Feather from 'react-native-vector-icons/Feather';
import Constants from '../../config/Constants';
import {TextInput, HelperText} from 'react-native-paper';
import Spinner from 'react-native-loading-spinner-overlay';
import rnauth_api from '../../api/rnauth_api';
import auth from '@react-native-firebase/auth';
import {useNavigation} from '@react-navigation/native';
import AsyncStorage from '@react-native-community/async-storage';

const ug_flag =
  'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACgAAAAeCAMAAABpA6zvAAAABGdBTUEAALGPC/xhBQAAAAFzUkdCAK7OHOkAAAAgY0hSTQAAeiYAAICEAAD6AAAAgOgAAHUwAADqYAAAOpgAABdwnLpRPAAAASxQTFRFAAAAHxwA4MsA/+kA/+cA/+gA+9IB+9EA+tAA+tME+tIC+s8A4k4H4kwF32Eg341c0pJG4bBy5Jlp32Af3jgI3TYG3F888NHL4NjbalUyvaGC/fn978/K3jkI3jgG20sg79DI////zsfHXFNS29nb4DkI3zYE4YRp/fz+//3/nJybdHVz8e/x//7//fz9wzIHwjEG3qiZ+/n7a2xrSU1Jur2618nG9+7tGwcBGQYAn5eWwL7AR0hHT01Kn0Ms3XVe/vf4n5iXZWRl7uzusbCxS0xNpnlv0lI19eflZWVmGhoax8bH9/f3paOlsa+xp6ep2cO/47ClxsTFNTU3xMPG8vDzs7K1sbCz+ff6xMTHHBkAODUagHxhjIhtn5uBgn5j38oA3cgA4cwC4MsBYexgUAAAAAFiS0dEIcRsDRYAAAAJcEhZcwAAAEgAAABIAEbJaz4AAADaSURBVDjLY2AYAoCRSMDARCRgYCYSMLAQCRhYiQQMbOiAnYOTi5sdQ5iBBw3w8vELCAoJ86KLM4igAlExcQlJKWkZMVE0CQZZFCAnr6CopKyiqKggL4cqw6CKAtTUNTS1tHV0dfXU1VBlGPRRgIGhopGxiamZuaKhAaoMgwUKsLRSVLS2sbWzd3C0RJVBj3onPU1nF1c3dw9PAmnEy9vH188/IDDIi1BqCg4JDQuPiAwmmMyiomNi4+Kjowgns4TEpOTEhMGdzHAAjGSGC6AnM5wALZnhBtRXCAA5O0ad99aTXAAAACV0RVh0ZGF0ZTpjcmVhdGUAMjAxMy0xMC0wN1QxMzoxNDoyNyswMjowMB60MsYAAAAldEVYdGRhdGU6bW9kaWZ5ADIwMTMtMTAtMDdUMTM6MTQ6MjcrMDI6MDBv6Yp6AAAAAElFTkSuQmCC';

const ConfirmPasswordModal = ({visible, transaction_data, _hideModal}) => {
  // const containerStyle = {
  //     width: '90%',
  //     height: '70%',
  //     borderRadius: 15,
  //     backgroundColor: '#FFFFFF',
  //     position: 'absolute',
  //     paddingHorizontal: 5,
  //     top: -100,
  //     alignSelf: 'center'
  // };

  const [spinner, setSpinner] = useState(false);
  const [Password, setPassword] = useState('');
  const [showPassword, setShowPassword] = useState(true);
  const [open_modal, setOpenModal] = useState(visible);
  const navigation = useNavigation();

  const checkPasswordStatus = () => {
    if (!showPassword) {
      setShowPassword(true);
    } else {
      setShowPassword(false);
    }
  };

  // const openModal = () => {
  //     setVisible(true);
  //   };

  const hideModal = () => {
    setOpenModal(false);
  };

  // const DATA = [
  //     { recipient_name: recipientName, exchange_rate: exchangeRate, converted_val: conertedVal, fees: fees, total_pay: totalPay, to_recipient: toRecipient },
  // ];

  const requestAccess = async () => {
    console.log('transaction data ', transaction_data);
    console.log('user password ', Password);
    setSpinner(true);
    try {
      let id = await AsyncStorage.getItem('userid');
      const response = await rnauth_api.post('/confirm_transaction.php', {
        password: Password,
      });

      if (response.data.message === 'Confirmation Successfull') {
        const response_status = await rnauth_api.post(
          '/get_response.php',
          // {
          //     "recipient_name": transaction_data[0].recipient_name,
          //     "exchange_rate": transaction_data[0].exchange_rate,
          //     "converted_val": transaction_data[0].converted_val,
          //     "fees": transaction_data[0].fees,
          //     "total_pay": transaction_data[0].total_pay,
          //     "to_recipient": transaction_data[0].to_recipient
          // },
          {
            base_currency: transaction_data[0].base_currency,
            conversion_currency: transaction_data[0].conversion_currency,
            send_amount: transaction_data[0].send_amount,
            conversion_rate: transaction_data[0].conversion_rate,
            receive_amount: transaction_data[0].receive_amount,
            fees: transaction_data[0].fees,
            customer_id: id,
            recipient: transaction_data[0].recipient,
            payment_method: transaction_data[0].payment_method,
            payout_method: transaction_data[0].payout_method,
          },
        );

        if (response_status.data.status === 'success') {
          setSpinner(false);
          setPassword(''); // clear password field after submitting
          navigation.navigate('_successPage', {
            RESPONSE_DATA: response_status.data,
          });
        } else {
          setSpinner(false);
          Alert.alert('Sorry!,', 'try again');
        }
      } else {
        setSpinner(false);
        Alert.alert('Sorry! ', response.data.message);
      }
    } catch (err) {
      setSpinner(false);
      alert('Sorry! Something went wrong');
    }
  };

  return (
    <Modal
      visible={visible}
      onDismiss={_hideModal}
      contentContainerStyle={styles.containerStyle}>
      <Spinner
        visible={spinner}
        textContent={'Loading...'}
        textStyle={{color: '#FFFFFF'}}
      />

      {/* <View style={{ height: '15%', position: 'absolute', top: 8, right: 10, alignItems: 'center' }}>
                <View style={{ justifyContent: 'flex-end', flexDirection: 'row', padding: 5, }}>
                    <Feather name="x" style={styles.iconClose}
                            onPress={_hideModal}
                        // onPress={() => console.log("Close button pressed")}
                        />
                </View>
            </View> */}

      <View
        style={{
          height: '15%',
          padding: 5,
          marginTop: -15,
          justifyContent: 'center',
        }}>
        <Row style={{justifyContent: 'center'}}>
          <View style={{alignSelf: 'center', alignItems: 'center'}}>
            <Feather name="lock" style={styles.icon} />
          </View>
        </Row>
      </View>

      <View style={{height: '25%', justifyContent: 'center', padding: 10}}>
        <Text style={styles.highlight}>Enter your password</Text>
        <Text note numberOfLines={2} style={styles.highlight_}>
          For security reasons, your password is required to complete this
          transaction.
        </Text>
      </View>

      <View
        style={{
          height: '30%',
          justifyContent: 'center',
          marginTop: 10,
          padding: 10,
        }}>
        <TextInput
          value={Password}
          onChangeText={setPassword}
          mode="outlined"
          label="Enter Password"
          outlineColor={Constants.PrimaryColor}
          placeholder="Enter password"
          secureTextEntry={showPassword}
          // right={<TextInput.Icon name="eye" onPress={checkPasswordStatus} />}
          right={
            <TextInput.Icon
              name={showPassword ? 'eye' : 'eye-off'}
              onPress={checkPasswordStatus}
            />
          }
        />
        <Text style={styles.highlight_}>Forgot password</Text>
      </View>

      <View style={{height: '15%', marginTop: 10}}>
        {Password !== '' ? (
          <TouchableOpacity onPress={requestAccess}>
            <View
              style={{
                backgroundColor: Constants.PrimaryColor,
                width: '80%',
                height: 40,
                alignItems: 'center',
                justifyContent: 'center',
                borderRadius: 30,
                alignSelf: 'center',
              }}>
              <Text style={{color: 'white'}}>Send</Text>
            </View>
          </TouchableOpacity>
        ) : (
          <TouchableOpacity>
            <View
              style={{
                backgroundColor: Constants.PrimaryColor,
                width: '80%',
                height: 40,
                alignItems: 'center',
                justifyContent: 'center',
                borderRadius: 30,
                alignSelf: 'center',
                opacity: 0.5,
              }}>
              <Text style={{color: 'white'}}>Send</Text>
            </View>
          </TouchableOpacity>
        )}
      </View>
    </Modal>
  );
};

const styles = {
  container: {
    backgroundColor: '#EEEEEE',
  },
  listItem: {
    borderWidth: 0.8,
    borderRadius: 8,
    marginLeft: 0,
    marginBottom: 10,
    paddingLeft: 10,
    flexDirection: 'column',
    backgroundColor: '#FFFFFF',
  },
  logo: {
    backgroundColor: '#EEEEEE',
    borderRadius: 50,
    marginVertical: 10,
    marginTop: 2,
    marginRight: 10,
  },
  logoSize: {
    resizeMode: 'cover',
    width: 40,
    height: 40,
    borderWidth: 1 / PixelRatio.get(),
    borderColor: '#eee',
    opacity: 0.8,
    // borderRadius: 25
    borderRadius: 5,
  },
  sectionContainer: {
    marginLeft: 10,
    marginRight: 10,
    // alignItems: 'center',
    padding: 5,
    borderRadius: 2,
    // marginBottom: 20
  },
  highlight: {
    fontFamily: Constants.Bold,
    color: Constants.PrimaryColor,
    alignSelf: 'center',
  },
  highlight_: {
    fontFamily: Constants.Regular,
    fontSize: 12,
    color: Constants.PrimaryColor,
  },
  highlight_m: {
    fontFamily: Constants.Regular,
  },
  bodyStyle: {
    // flexDirection: 'row',
    justifyContent: 'center',
  },
  icon_: {
    color: '#FFFFFF',
    fontSize: 22,
  },
  icon: {
    color: '#000000',
    fontSize: 30,
  },
  iconClose: {
    color: '#000000',
    fontSize: 20,
  },
  cardItem: {
    borderRadius: 8,
  },
  sectionTitle: {
    fontFamily: Constants.Bold,
    marginLeft: 5,
    fontSize: 13,
  },
  textLabel: {
    fontFamily: Constants.Regular,
  },
  containerStyle: {
    width: '90%',
    height: '70%',
    borderRadius: 15,
    backgroundColor: '#FFFFFF',
    position: 'absolute',
    paddingHorizontal: 5,
    top: -100,
    alignSelf: 'center',
  },
};

export default ConfirmPasswordModal;
