import React, { useState, useRef } from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    Text,
    View,
    StatusBar,
    // TextInput,
    Button,
    Image,
    Alert,
    TouchableOpacity,
    KeyboardAvoidingView
} from 'react-native';
import Spinner from 'react-native-loading-spinner-overlay';
import { TextInput } from 'react-native-paper';
import AsyncStorage from '@react-native-community/async-storage';
import { Container, Picker } from 'native-base';
import Feather from 'react-native-vector-icons/Feather';
import PhoneInput from "react-native-phone-number-input";
import rnauth_api from '../api/rnauth_api';
import Constants from '../config/Constants';

const AddPaymentMethod = ({ navigation }) => {

    const [cardName, setCardName] = useState("");
    const [cardNumber, setCardNumber] = useState("");
    const [moMoNumber, setMoMoNumber] = useState("");
    const [paymet_method, setPaymentMethod] = useState("");
    const [spinner, setSpinner] = useState(false);
    const [userId, setUserId] = useState("");
    const [method_status, setMethodStatus] = useState(false);
    const [expirlyMonth, setExpirlyMonth] = useState("");
    const [expirlyYear, setExpirlyYear] = useState("");
    const [formattedValue, setFormattedValue] = useState('');
    const [countryCode, setCountryCode] = useState('');
    const [disabled, setDisabled] = useState(false);
    const [valid, setValid] = useState(false);
    const phoneInput = useRef(null);

    const validatePhoneNumber = () => {
        var regexp = /^\+[0-9]?()[0-9](\s|\S)(\d[0-9]{8,16})$/
        return regexp.test("+" + moMoNumber)
    }

    const signUp = async (CardName, CardNumber, MoMoNumber, PayMethod, ExpirlyMonth, ExpirlyYear) => {
        if (PayMethod === '') {
            Alert.alert('Payment method is required');
        } else {
            if (CardName === '' && PayMethod === 'Debdt/Credid') {
                Alert.alert('Card Name is required');
            } else {
                if (CardNumber === '' && PayMethod === 'Debdt/Credid') {
                    Alert.alert('Card Number is required');
                } else {
                    if (ExpirlyMonth === '' && PayMethod === 'Debdt/Credid') {
                        Alert.alert('Expirly month is required');
                    } else {
                        if (ExpirlyYear === '' && PayMethod === 'Debdt/Credid') {
                            Alert.alert('Expirly year is required');
                        } else {
                            const checkValid = phoneInput.current?.isValidNumber(MoMoNumber);
                            // setShowMessage(true);
                            setValid(checkValid ? checkValid : false);
                            setCountryCode(phoneInput.current?.getCountryCode() || '');
                            let getNumberAfterPossiblyEliminatingZero = phoneInput.current?.getNumberAfterPossiblyEliminatingZero();
                            // console.log("Phone ",getNumberAfterPossiblyEliminatingZero.formattedNumber);

                            if (!checkValid && PayMethod === 'Mobile Money') {
                                Alert.alert('Sorry! Ivalide phone number');
                            } else {
                                setSpinner(true);
                                try {
                                    // console.log('ID', user_id);
                                    let user_id = await AsyncStorage.getItem('userid');
                                    const response = await rnauth_api.post('/add_pay_method.php',
                                        {
                                            "method": PayMethod,
                                            "card_name": PayMethod === 'Debdt/Credid' ? CardName : 'NA',
                                            "card_number": PayMethod === 'Debdt/Credid' ? CardNumber : 'NA',
                                            "phone_number": PayMethod === 'Mobile Money' ? MoMoNumber : 'NA',
                                            "user_id": user_id,
                                            "expirly_date": PayMethod === 'Debdt/Credid' ? ExpirlyMonth + "/" + ExpirlyYear : 'NA',
                                        }
                                    );
                                    if (response.data.message === "User added successfully") {
                                        setSpinner(false);
                                        Alert.alert(response.data.message);
                                        this.props.navigation.navigate('_managePayments');
                                    } else {
                                        setSpinner(false);
                                        Alert.alert(response.data.message);
                                    }
                                } catch (error) {
                                    setSpinner(false);
                                    // alert(error);
                                    Alert.alert('Sorry! Something went wrong');
                                }
                            }
                        }
                    }
                }
            }
        }
    };

    return (
        <Container>
            <StatusBar backgroundColor={Constants.PrimaryColor} hidden={false} />
            <Spinner
                visible={spinner}
                textContent={'Loading...'}
                textStyle={{ color: '#FFFFFF' }}
            />

            <KeyboardAvoidingView
                style={{ flex: 1 }}
                behavior={Platform.OS == 'ios' ? 'padding' : 'height'}
                keyboardVerticalOffset={Platform.OS === 'ios' ? 40 : 0}>

                <View style={styles.container}>

                    <View style={{
                        width: '100%',
                        height: '10%',
                        alignItems: 'center',
                        justifyContent: 'center',
                        backgroundColor: Constants.PrimaryColor,
                        flexDirection: 'row',
                        marginBottom: 5
                    }}>
                        <Feather name='chevron-left' style={styles.icon_} onPress={() => { navigation.goBack() }} />
                        <Text style={{ fontSize: 20, fontFamily: Constants.Bold, color: '#FFFFFF' }}>Add Payment Method</Text>
                    </View>

                    <View style={{
                        width: '100%',
                        height: '75%',
                        backgroundColor: '#FFFFFF',
                        justifyContent: 'center',
                        // alignItems: 'center'
                    }}>

                        <View style={styles.sectionContainerPicker}>
                            <Picker
                                mode="dropdown"
                                placeholder="Select Payment Method"
                                note={true}
                                selectedValue={paymet_method}
                                onValueChange={setPaymentMethod}
                            >
                                <Picker.Item label="Select Payment Method" value="" />
                                <Picker.Item label={"Debdt/Credid"} value={"Debdt/Credid"} />
                                <Picker.Item label={"Mobile Money"} value={"Mobile Money"} />
                            </Picker>
                        </View>

                        {paymet_method === 'Debdt/Credid' && (
                            <View>
                                <View style={styles.sectionContainer}>
                                    <TextInput
                                        // style={styles.input}
                                        autoCapitalize="none"
                                        autoCorrect={false}
                                        value={cardName}
                                        onChangeText={setCardName}
                                        // keyboardType="email"
                                        mode="outlined"
                                        label="Card Name"
                                        outlineColor={Constants.PrimaryColor}
                                        placeholder="Type card name"
                                    />
                                </View>

                                <View style={styles.sectionContainer}>
                                    <TextInput
                                        // style={styles.input}
                                        autoCapitalize="none"
                                        autoCorrect={false}
                                        keyboardType={'numeric'}
                                        value={cardNumber}
                                        onChangeText={setCardNumber}
                                        mode="outlined"
                                        label="Card Number"
                                        outlineColor={Constants.PrimaryColor}
                                        placeholder="Type card number"
                                    />
                                </View>
                                <View style={styles.row1}>

                                    <TextInput
                                        style={styles.input}
                                        autoCapitalize="none"
                                        autoCorrect={false}
                                        keyboardType={'numeric'}
                                        maxLength={2}
                                        value={expirlyMonth}
                                        onChangeText={setExpirlyMonth}
                                        mode="outlined"
                                        label="Expirly Month"
                                        outlineColor={Constants.PrimaryColor}
                                        placeholder="Type expirly month"
                                    />

                                    <TextInput
                                        style={styles.input}
                                        autoCapitalize="none"
                                        autoCorrect={false}
                                        keyboardType={'numeric'}
                                        maxLength={2}
                                        value={expirlyYear}
                                        onChangeText={setExpirlyYear}
                                        mode="outlined"
                                        label="Expirly year (21)"
                                        outlineColor={Constants.PrimaryColor}
                                        placeholder="Type expirly year"
                                    />

                                </View>
                            </View>
                        )}
                        {paymet_method === 'Mobile Money' && (
                            <View>
                                <View style={styles.sectionContainer}>
                                    <PhoneInput
                                        ref={phoneInput}
                                        defaultValue={moMoNumber}
                                        defaultCode="UG"
                                        layout="first"
                                        onChangeText={(text) => {
                                            setMoMoNumber(text);
                                        }}
                                        onChangeFormattedText={(text) => {
                                            setFormattedValue(text);
                                            setCountryCode(phoneInput.current?.getCountryCode() || '');
                                        }}
                                        countryPickerProps={{ withAlphaFilter: true }}
                                        disabled={disabled}
                                        withDarkTheme
                                        withShadow
                                    />
                                </View>
                            </View>
                        )}
                    </View>
                    <View style={{
                        width: '100%',
                        height: '15%',
                        backgroundColor: '#FFFFFF',
                        justifyContent: 'center',
                        padding: 5,
                    }}>

                        <View style={styles.sectionContainer}>
                            {paymet_method === "" ? <TouchableOpacity
                                onPress={() => signUp(cardName, cardNumber, moMoNumber, paymet_method, expirlyMonth, expirlyYear)}
                                style={{ width: '100%', height: 50, justifyContent: 'center', padding: 10, alignItems: 'center' }}>
                                <View style={{ backgroundColor: '#576CA8', width: '100%', height: 50, alignItems: 'center', justifyContent: 'center', borderRadius: 30, opacity: 0.5 }}>
                                    <Text style={{ color: 'white', fontFamily: Constants.PrimaryColor, }}>Save</Text>
                                </View>
                            </TouchableOpacity> :
                                <TouchableOpacity
                                    onPress={() => signUp(cardName, cardNumber, moMoNumber, paymet_method, expirlyMonth, expirlyYear)}
                                    style={{ width: '100%', height: 50, justifyContent: 'center', padding: 10, alignItems: 'center' }}>
                                    <View style={{ backgroundColor: '#576CA8', width: '100%', height: 50, alignItems: 'center', justifyContent: 'center', borderRadius: 30 }}>
                                        <Text style={{ color: 'white', fontFamily: Constants.PrimaryColor, }}>Save</Text>
                                    </View>
                                </TouchableOpacity>}
                        </View>
                    </View>
                </View>


            </KeyboardAvoidingView>
        </Container>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        // backgroundColor: Constants.PrimaryColor
    },
    scrollView: {
        backgroundColor: '#EEEEEE',
    },
    logo: {
        backgroundColor: '#EEEEEE',
        alignItems: 'center',
    },
    logoSize: {
        height: 100,
        width: 100,
    },
    sectionContainer: {
        marginLeft: 20,
        marginRight: 20,
        padding: 5,
        borderRadius: 2,
    },
    sectionContainerPicker: {
        marginLeft: 25,
        marginRight: 25,
        borderColor: Constants.PrimaryColor,
        borderWidth: 1,
        borderRadius: 5,
        fontFamily: Constants.Regular
    },
    input: {
        width: '49%',
    },
    highlight: {
        fontWeight: '700',
    },
    textLebal: {
        marginTop: 8,
        fontSize: 18,
        fontWeight: '400',
        color: 'black',
    },
    row1: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginLeft: 20,
        marginRight: 20,
        padding: 5,
        borderRadius: 2,
    },
    screen: {
        margin: 5,
        paddingHorizontal: 15,
        padding: 5,
    },
    _input: {
        marginTop: 8,
        fontSize: 18,
        fontWeight: '400',
        borderColor: 'lightblue',
        marginVertical: 30,
        borderWidth: 1,
        borderRadius: 5,
    },
    text: {
        fontWeight: '700',
        alignSelf: 'center'
    },
    icon_: {
        color: "#FFFFFF",
        fontSize: 30,
        position: 'absolute',
        left: 10
    },
});

export default AddPaymentMethod;