import React, { useState } from "react";
import { StyleSheet, TextInput, View, Button, Image, PixelRatio } from 'react-native';
import { Container, Content, Card, CardItem, Text, Body, Row, Footer, FooterTab, Item } from "native-base";
import CurrencyConvertor from '../config/CurrencyConvertor';

const CurrencyComponent = ({ user_rate, user_currency, user_flag }) => {
    const [originalValue, setOriginalValue] = useState('');
    const [convertedValue, setConvertedValue] = useState('');

    return (
        <View>
        <View style={{flexDirection: 'row', height: '25%'}}>
            <View style={{
                width: '50%',
                backgroundColor: 'orange',
            }}><Text style={{padding: 10, fontWeight: '700'}} note>SEND</Text></View>

            <View style={{
                width: '50%',
                backgroundColor: 'green',
            }}><Text style={{padding: 10,fontWeight: '700'}} note>RECEIVE</Text></View>
        </View>
        <View style={{flexDirection: 'row',height: '35%'}}>
            <View style={{
                width: '50%',
                backgroundColor: 'yellow',
            }}><Text style={{padding: 10,}}>Field One</Text></View>

            <View style={{
                width: '50%',
                backgroundColor: 'tomato',
            }}><Text style={{padding: 10,}}>Field Two</Text></View>
        </View>
        </View>
    );
};

export default CurrencyComponent;

const styles = StyleSheet.create({
    textRowHeader: {
        justifyContent: 'space-around'
    },
    textRowFooter: {
        justifyContent: 'center',
    },
    headerText: {
        color: '#000000'
    },
    footerText: {
        color: '#000000',
        marginRight: 10
    },
    containerRow: {
        justifyContent: 'space-between',
    },
    childRow: {
        marginHorizontal: 8
    },
    input: {
        fontSize: 18,
        fontWeight: '400',
        borderColor: 'black',
        borderWidth: 0.3,
        borderRadius: 1,
        marginLeft: 5,
    },
    inputView: {
        flex: 1,
    },
    labelView: {
        justifyContent: 'center',
        borderWidth: 0.3,
        borderRadius: 1,
        padding: 5,
        margin: 0,
        backgroundColor: '#EEEEEE',
        // flexDirection: 'row',
    },
    footerButton: {
        flex: 1,
        marginHorizontal: 5,
        borderBottomLeftRadius: 5,
        borderBottomRightRadius: 5,
        justifyContent: 'flex-end',
        marginBottom: 5,
    },
    imgStyle: {
        resizeMode: 'contain',
        width: 25,
        height: 19,
        borderWidth: 1 / PixelRatio.get(),
        borderColor: '#eee',
        opacity: 0.8
    }
});




// import React, { useState } from "react";
// import { StyleSheet, TextInput, View, Button, Image, PixelRatio } from 'react-native';
// import { Container, Content, Card, CardItem, Text, Body, Row, Footer, FooterTab, Item } from "native-base";
// import CurrencyConvertor from '../config/CurrencyConvertor';

// const CurrencyComponent = ({ user_rate, user_currency, user_flag }) => {
//     const [originalValue, setOriginalValue] = useState('');
//     const [convertedValue, setConvertedValue] = useState('');

//     return (
//         <Container>
//             <Content>
//                 <Card>
//                     <CardItem header bordered>
//                         <Row style={styles.textRowHeader}>
//                             <Text style={styles.headerText}>ORIGINAL</Text>
//                             <Text style={styles.headerText}>CONVERTED</Text>
//                         </Row>
//                     </CardItem>
//                     <CardItem bordered>
//                         <Body>
//                             <Row style={styles.containerRow}>
//                                 <Row style={styles.childRow}>
//                                     <View style={styles.labelView}>
//                                         <Text>
//                                             UGX
//                                         </Text>
//                                         <Image
//                                             style={styles.imgStyle}
//                                             source={{ uri: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACgAAAAeCAMAAABpA6zvAAAABGdBTUEAALGPC/xhBQAAAAFzUkdCAK7OHOkAAAAgY0hSTQAAeiYAAICEAAD6AAAAgOgAAHUwAADqYAAAOpgAABdwnLpRPAAAASxQTFRFAAAAHxwA4MsA/+kA/+cA/+gA+9IB+9EA+tAA+tME+tIC+s8A4k4H4kwF32Eg341c0pJG4bBy5Jlp32Af3jgI3TYG3F888NHL4NjbalUyvaGC/fn978/K3jkI3jgG20sg79DI////zsfHXFNS29nb4DkI3zYE4YRp/fz+//3/nJybdHVz8e/x//7//fz9wzIHwjEG3qiZ+/n7a2xrSU1Jur2618nG9+7tGwcBGQYAn5eWwL7AR0hHT01Kn0Ms3XVe/vf4n5iXZWRl7uzusbCxS0xNpnlv0lI19eflZWVmGhoax8bH9/f3paOlsa+xp6ep2cO/47ClxsTFNTU3xMPG8vDzs7K1sbCz+ff6xMTHHBkAODUagHxhjIhtn5uBgn5j38oA3cgA4cwC4MsBYexgUAAAAAFiS0dEIcRsDRYAAAAJcEhZcwAAAEgAAABIAEbJaz4AAADaSURBVDjLY2AYAoCRSMDARCRgYCYSMLAQCRhYiQQMbOiAnYOTi5sdQ5iBBw3w8vELCAoJ86KLM4igAlExcQlJKWkZMVE0CQZZFCAnr6CopKyiqKggL4cqw6CKAtTUNTS1tHV0dfXU1VBlGPRRgIGhopGxiamZuaKhAaoMgwUKsLRSVLS2sbWzd3C0RJVBj3onPU1nF1c3dw9PAmnEy9vH188/IDDIi1BqCg4JDQuPiAwmmMyiomNi4+Kjowgns4TEpOTEhMGdzHAAjGSGC6AnM5wALZnhBtRXCAA5O0ad99aTXAAAACV0RVh0ZGF0ZTpjcmVhdGUAMjAxMy0xMC0wN1QxMzoxNDoyNyswMjowMB60MsYAAAAldEVYdGRhdGU6bW9kaWZ5ADIwMTMtMTAtMDdUMTM6MTQ6MjcrMDI6MDBv6Yp6AAAAAElFTkSuQmCC' }}
//                                         />
//                                     </View>
//                                     <View style={styles.inputView}>
//                                         <TextInput
//                                             style={styles.input}
//                                             keyboardType="numeric"
//                                             value={originalValue}
//                                             onChangeText={setOriginalValue}
//                                         />
//                                     </View>
//                                 </Row>
//                                 <Row style={styles.childRow}>
//                                     <View style={styles.labelView}>
//                                         <Text>
//                                             {user_currency}
//                                         </Text>
//                                         <Image
//                                             style={styles.imgStyle}
//                                             source={{ uri: user_flag }}
//                                         />
//                                     </View>
//                                     <View style={styles.inputView}>
//                                         <CurrencyConvertor
//                                             rate={user_rate}
//                                             convertedValue={convertedValue}
//                                             originalValue={originalValue}
//                                             setConvertedValue={setConvertedValue} />
//                                     </View>
//                                 </Row>
//                             </Row>
//                         </Body>
//                     </CardItem>
//                     <CardItem footer bordered>
//                         <Row style={styles.textRowFooter}>
//                             <Text style={styles.footerText}>Conversion Rate:</Text>
//                             <Text>{user_rate}</Text>
//                         </Row>
//                     </CardItem>
//                 </Card>
//             </Content>
//             {/* <Footer style={{ backgroundColor: '#FFFFFF' }}>
//                 <FooterTab style={{ backgroundColor: '#FFFFFF' }}>
//                     <View style={styles.footerButton}>
//                         <Button title={'BUTTON'} />
//                     </View>
//                 </FooterTab>
//             </Footer> */}
//         </Container>
//     );
// };

// export default CurrencyComponent;

// const styles = StyleSheet.create({
//     textRowHeader: {
//         justifyContent: 'space-around'
//     },
//     textRowFooter: {
//         justifyContent: 'center',
//     },
//     headerText: {
//         color: '#000000'
//     },
//     footerText: {
//         color: '#000000',
//         marginRight: 10
//     },
//     containerRow: {
//         justifyContent: 'space-between',
//     },
//     childRow: {
//         marginHorizontal: 8
//     },
//     input: {
//         fontSize: 18,
//         fontWeight: '400',
//         borderColor: 'black',
//         borderWidth: 0.3,
//         borderRadius: 1,
//         marginLeft: 5,
//     },
//     inputView: {
//         flex: 1,
//     },
//     labelView: {
//         justifyContent: 'center',
//         borderWidth: 0.3,
//         borderRadius: 1,
//         padding: 5,
//         margin: 0,
//         backgroundColor: '#EEEEEE',
//         // flexDirection: 'row',
//     },
//     footerButton: {
//         flex: 1,
//         marginHorizontal: 5,
//         borderBottomLeftRadius: 5,
//         borderBottomRightRadius: 5,
//         justifyContent: 'flex-end',
//         marginBottom: 5,
//     },
//     imgStyle: {
//         resizeMode: 'contain',
//         width: 25,
//         height: 19,
//         borderWidth: 1 / PixelRatio.get(),
//         borderColor: '#eee',
//         opacity: 0.8
//     }
// });
