import React, { useEffect } from 'react';
import { Alert, StatusBar, Image, View, BackHandler, Platform } from 'react-native';
import { Container, Header, Left, Body, Right, Title, Text, Button } from 'native-base';
import Constants from '../config/Constants';
import Onboarding from 'react-native-onboarding-swiper';
import DeviceInfo from 'react-native-device-info';
import AsyncStorage from '@react-native-community/async-storage';

const app_logo = require('../../assets/icon_512.png');

var uniqueId = null;

const IntroScreen = ({ navigation }) => {

    useEffect(() => {
        uniqueId = DeviceInfo.getUniqueId();
        BackHandler.addEventListener('hardwareBackPress', handleBackButton);
    }, []);

        // HardWare Backbutton Controller
        const handleBackButton = () => {
            BackHandler.exitApp();
            return true;
        }

    return (
        <Container>
        {/* <Header backgroundColor={Constants.PrimaryColor}>
        </Header> */}
        <StatusBar backgroundColor={Constants.PrimaryColor} hidden={false} />
      
        <Onboarding
            showDone={false}
            onSkip={() => {
                AsyncStorage.setItem('device_id', uniqueId);
                console.log('message', 'Skipped' + ' ID ' + uniqueId);
                navigation.navigate('_login');
            }}
            pages={[
                {
                    title: 'Instant',
                    subtitle: 'Send and Receive money from anywhere to anywhere instantly',
                    backgroundColor: Constants.PrimaryColor,
                    image: (
                        <Image source={app_logo} style={styles.logoSize} />
                    ),
                },
                {
                    title: 'Flexibility',
                    subtitle: 'Use a variety of methods to receive and send money',
                    backgroundColor: Constants.PrimaryColor,
                    image: (
                        <Image source={app_logo} style={styles.logoSize} />
                    ),
                },
                {
                    title: 'No fees, No commissions.',
                    subtitle: 'Your money is all yours, we do not charge any extra fees ',
                    backgroundColor: Constants.PrimaryColor,
                    image: (
                        <Image source={app_logo} style={styles.logoSize} />
                    ),
                },
                {
                    title: "That's Enough",
                    subtitle: (
                        <View style={{ justifyContent: 'center' }}>
                            <Button
                                success iconRight
                                rounded
                                style={{ marginTop: 20, marginHorizontal: 15, flex: Platform.OS === 'android'? 1 : 0 }}
                                textStyle={{ color: '#003c8f' }}
                                onPress={() => {
                                    AsyncStorage.setItem('device_id', uniqueId);
                                    console.log('message', 'Done' + ' ID ' + uniqueId);
                                    StatusBar.setBarStyle('default');
                                    navigation.navigate('_signup');
                                }}
                            >
                                <Text style={{fontFamily: Constants.Bold}}>Get Started</Text>
                            </Button>
                            <Button
                                info iconRight
                                rounded
                                style={{ marginTop: 20, marginHorizontal: 15, flex: Platform.OS === 'android'? 1 : 0 }}
                                textStyle={{ color: '#003c8f' }}
                                onPress={() => {
                                    AsyncStorage.setItem('device_id', uniqueId);
                                    console.log('message', 'Done' + ' ID ' + uniqueId);
                                    StatusBar.setBarStyle('default');
                                    navigation.navigate('_login');
                                }}
                            >
                                <Text style={{fontFamily: Constants.Bold}}>Sign In</Text>
                            </Button>
                        </View>
                    ),
                    backgroundColor: Constants.PrimaryColor,
                    image: (
                        // <Icon name="rocket" type="font-awesome" size={100} color="white" />
                        <Image source={app_logo} style={styles.logoSize} />
                    ),
                },
            ]}
        />
        </Container>
    );
};


const styles = {
    logo: {
        backgroundColor: '#EEEEEE',
        alignItems: 'center',
    },
    logoSize: {
        height: 100,
        width: 100,
    },
};

export default IntroScreen;