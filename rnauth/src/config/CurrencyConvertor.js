import React from 'react';
import { Input, View } from "native-base";
import {TextInput} from 'react-native';

const CurrencyConvertor = ({ originalValue, convertedValue, rate, setConvertedValue }) => {
    // let result = originalValue * rate;
    let result = originalValue / rate;
    setConvertedValue(Math.round(result * 100)/100);

    return (
        <View>
            <TextInput
                style={{
                    fontSize: 18,
                    fontFamily: "texgyreadventor-regular",
                    marginLeft: 5,
                }}
                keyboardType="numeric"
                placeholder={"0.00"}
                disabled={true}
                defaultValue={convertedValue.toString()}
            />
        </View>
    );
};

const styles = {
    inputView: {
        flex: 1,
    },
    input: {
        fontSize: 18,
        fontWeight: '400',
        borderColor: 'black',
        borderWidth: 0.3,
        borderRadius: 1,
        marginLeft: 5,
    },
}

export default CurrencyConvertor;


// export default CurrencyConvertor = (Component: any) => {
//     return (props: any) => {
//     const navigation = useNavigation();
//     return <Component navigation={navigation} {...props} />;
//     }
//     };