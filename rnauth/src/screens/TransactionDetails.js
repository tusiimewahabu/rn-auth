import React from 'react';
import { Image, PixelRatio, StatusBar } from 'react-native';
import { Container, Header, Content, Card, Row, Col, CardItem, List, ListItem, Text, View, Body, Title, Left, Right, Icon, Button } from 'native-base';
import Feather from 'react-native-vector-icons/Feather';
import { useNavigation } from '@react-navigation/native';
import Constants from '../config/Constants';

const TransactionDetails = ({ route }) => {
    const navigation = useNavigation();
    const amount_value = route.params.t_converted/route.params.t_baseCurrecnce;
    return (
        <Container>
            <Header backgroundColor={Constants.PrimaryColor} style={{backgroundColor: Constants.PrimaryColor}}>
                <Left>
                    <Button transparent>
                        <Feather name='chevron-left' style={styles.icon} onPress={() => { navigation.goBack() }} />
                    </Button>
                </Left>
                <Body>
                    <Title style={{fontFamily: "texgyreadventor-bold"}}>Details</Title>
                </Body>
                <Right />
            </Header>
            <StatusBar backgroundColor={Constants.PrimaryColor} hidden={false} />
            <Content padder>
                <Card>
                    <CardItem header style={{ flexDirection: 'column' }}>
                        <Text note style={{fontFamily: "texgyreadventor-bold"}}>{route.params.t_date}</Text>
                        <View style={styles.payStatus}>
                            <Text style={styles.headerText}>{route.params.t_status}</Text>
                        </View>
                    </CardItem>
                    <CardItem>
                        <Body>
                            <View style={{alignItems: 'center'}}>
                                <View style={styles.flagView}>
                                    <Image
                                        style={styles.imgStyle}
                                        source={{ uri: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACgAAAAeCAMAAABpA6zvAAAABGdBTUEAALGPC/xhBQAAAAFzUkdCAK7OHOkAAAAgY0hSTQAAeiYAAICEAAD6AAAAgOgAAHUwAADqYAAAOpgAABdwnLpRPAAAASxQTFRFAAAAHxwA4MsA/+kA/+cA/+gA+9IB+9EA+tAA+tME+tIC+s8A4k4H4kwF32Eg341c0pJG4bBy5Jlp32Af3jgI3TYG3F888NHL4NjbalUyvaGC/fn978/K3jkI3jgG20sg79DI////zsfHXFNS29nb4DkI3zYE4YRp/fz+//3/nJybdHVz8e/x//7//fz9wzIHwjEG3qiZ+/n7a2xrSU1Jur2618nG9+7tGwcBGQYAn5eWwL7AR0hHT01Kn0Ms3XVe/vf4n5iXZWRl7uzusbCxS0xNpnlv0lI19eflZWVmGhoax8bH9/f3paOlsa+xp6ep2cO/47ClxsTFNTU3xMPG8vDzs7K1sbCz+ff6xMTHHBkAODUagHxhjIhtn5uBgn5j38oA3cgA4cwC4MsBYexgUAAAAAFiS0dEIcRsDRYAAAAJcEhZcwAAAEgAAABIAEbJaz4AAADaSURBVDjLY2AYAoCRSMDARCRgYCYSMLAQCRhYiQQMbOiAnYOTi5sdQ5iBBw3w8vELCAoJ86KLM4igAlExcQlJKWkZMVE0CQZZFCAnr6CopKyiqKggL4cqw6CKAtTUNTS1tHV0dfXU1VBlGPRRgIGhopGxiamZuaKhAaoMgwUKsLRSVLS2sbWzd3C0RJVBj3onPU1nF1c3dw9PAmnEy9vH188/IDDIi1BqCg4JDQuPiAwmmMyiomNi4+Kjowgns4TEpOTEhMGdzHAAjGSGC6AnM5wALZnhBtRXCAA5O0ad99aTXAAAACV0RVh0ZGF0ZTpjcmVhdGUAMjAxMy0xMC0wN1QxMzoxNDoyNyswMjowMB60MsYAAAAldEVYdGRhdGU6bW9kaWZ5ADIwMTMtMTAtMDdUMTM6MTQ6MjcrMDI6MDBv6Yp6AAAAAElFTkSuQmCC' }}
                                    />
                                </View>

                                <View style={styles.labelView}>
                                    <Text style={{ color: '#003087', fontFamily: "texgyreadventor-bold" }}>
                                        {"You have sent"} {route.params.t_baseCurrecncy} to {route.params.t_name}
                                    </Text>
                                </View>
                            </View>
                        </Body>
                    </CardItem>
                    <CardItem footer>
                        <Row style={styles.textRowFooter}>
                            <Text note style={{fontFamily: "texgyreadventor-rgular"}}>Airtel Confirmation: UGA-200110-P4FP88</Text>
                        </Row>
                    </CardItem>
                </Card>
                <List>

                    <Card>
                        <ListItem avatar style={styles.listItem}
                        >
                            <Row>
                                <Body>
                                    <Text note style={styles.titleText}>{"SEND AMOUNT"}</Text>
                                    <Text style={styles.highlight}>{route.params.t_baseCurrecncy}</Text>
                                </Body>
                            </Row>
                        </ListItem>
                        <ListItem avatar style={styles.listItem}
                        >
                            <Row>
                                <Body>
                                    <Text note style={styles.titleText}>{"EXCHANGE RATE"}</Text>
                                    <Text style={styles.highlight}> 1 {route.params.t_currency} = {parseInt(amount_value)} UGX</Text>
                                </Body>
                            </Row>
                        </ListItem>

                        <ListItem avatar style={styles.listItem}
                        >
                            <Row>
                                <Body>
                                    <Text note style={styles.titleText}>{"FEES"}</Text>
                                    <Text style={styles.highlight}> 0.00 {route.params.t_currency}</Text>
                                </Body>
                            </Row>
                        </ListItem>

                        <ListItem avatar style={styles.listItem}
                        >
                            <Row>
                                <Body>
                                    <Text note style={styles.titleText}>{"TOTAL PAID"}</Text>
                                    <Text style={styles.highlight}>{route.params.t_baseCurrecncy}</Text>
                                </Body>
                            </Row>
                        </ListItem>
                        <ListItem avatar style={styles.listItem}
                        >
                            <Row>
                                <Body>
                                    <Text note style={styles.titleText}>{"RECEIVED AMOUNT"}</Text>
                                    <Text style={styles.highlight}>{route.params.t_convertedCurrency}</Text>
                                </Body>
                            </Row>
                        </ListItem>

                        <ListItem avatar style={styles.listItem}
                        >
                            <Row>
                                <Body>
                                    <Text note style={styles.titleText}>{"FUNDS DELIVERED"}</Text>
                                    <Text style={styles.highlight}>{route.params.t_date}</Text>
                                </Body>
                            </Row>
                        </ListItem>

                    </Card>

                </List>
            </Content>
        </Container>
    );
};

const styles = {
    container: {
        backgroundColor: '#EEEEEE'
    },
    listItem: {
        // borderWidth: 0.8,
        // borderRadius: 8,
        marginLeft: 0,
        marginBottom: 10,
        paddingLeft: 10,
        flexDirection: 'column',
        backgroundColor: '#FFFFFF'
    },

    highlight: {
        fontFamily: "texgyreadventor-bold",
        color: '#003087'
    },
    highlight_: {
        fontWeight: '700',
    },
    highlight_m: {
        fontWeight: '700',
    },
    icon_: {
        color: "#FFFFFF",
        fontSize: 30
    },
    icon: {
        color: "#FFFFFF",
        fontSize: 30
    },
    textRowHeader: {
        flexDirection: 'column',
        justifyContent: 'space-around',
    },
    textRowFooter: {
        justifyContent: 'center',
    },
    headerText: {
        color: 'green',
        fontSize: 13,
        fontFamily: "texgyreadventor-bold"
    },
    footerText: {
        color: '#000000',
        marginRight: 10
    },
    containerRow: {
        justifyContent: 'space-between',
    },
    childRow: {
        marginHorizontal: 8
    },
    flagView: {
        justifyContent: 'center',
        padding: 5,
        margin: 0,
        borderRadius: 1.5
    },
    labelView: {
        justifyContent: 'center',
        padding: 5,
        margin: 0,
    },
    imgStyle: {
        resizeMode: 'contain',
        width: 45,
        height: 29,
        borderWidth: 1 / PixelRatio.get(),
        borderColor: '#eee',
        opacity: 0.8,
    },
    payStatus: {
        marginTop: 10,
        backgroundColor: '#EEEEEE',
        padding: 3,
        borderRadius: 15
    },
    titleText: {
        fontFamily: "texgyreadventor-rgular"
    }
}

export default TransactionDetails;