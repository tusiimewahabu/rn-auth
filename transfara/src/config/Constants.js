
    // export const PrimaryColor = '#576CA8';
    // export const Bold = 'texgyreadventor-bold';
    // export const Italic = 'texgyreadventor-italic';
    // export const BoldItalic = 'texgyreadventor-bolditalic';
    // export const Regular ='texgyreadventor-regular';

    const Constants = {
        PrimaryColor:'#576CA8',
        Bold:'texgyreadventor-bold',
        Italic:'texgyreadventor-italic',
        BoldItalic:'texgyreadventor-bolditalic',
        Regular:'texgyreadventor-regular'
    }

    export default Constants;