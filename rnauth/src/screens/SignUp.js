import React, {useState, useRef, useEffect} from 'react';
import {
  PixelRatio,
  StatusBar,
  View,
  TouchableOpacity,
  KeyboardAvoidingView,
  BackHandler,
  Alert,
} from 'react-native';
import {Container, Text} from 'native-base';
import Feather from 'react-native-vector-icons/Feather';
import Constants from '../config/Constants';
import Spinner from 'react-native-loading-spinner-overlay';
import {TextInput, Checkbox, HelperText} from 'react-native-paper';
import PhoneInput from 'react-native-phone-number-input';
import Communications from 'react-native-communications';
import {useNavigation, useNavigationState} from '@react-navigation/native';

const terms = 'https://transfara.com/terms';
const privacy_policy = 'https://transfara.com/privacy_policy';

const SignUp = ({navigation}) => {
  // const navigation = useNavigation();
  const [spinner, setSpinner] = useState(false);
  const [formattedValue, setFormattedValue] = useState('');
  const [valid, setValid] = useState(false);
  const [disabled, setDisabled] = useState(false);
  const [showMessage, setShowMessage] = useState(false);
  const phoneInput = useRef(null);
  const [mobileNumber, setMobileNumber] = useState('');
  const [countryCode, setCountryCode] = useState('UG');
  const [confirm, setConfirm] = useState(null);
  const [checked, setChecked] = useState(false);
  const [Email, setEmail] = useState('');
  const [Password, setPassword] = useState('');
  const [showPassword, setShowPassword] = useState(true);

  const routes = useNavigationState((state) => state.routes);
  const currentRoute = routes[routes.length - 1].name;
  console.log('currentRoute: ', currentRoute);

  useEffect(() => {
    if (currentRoute === '_signup' && navigation.isFocused()) {
      BackHandler.removeEventListener('hardwareBackPress', () => {});
    }
  }, [navigation]);

  const validateEmail = (email) => {
    var re =
      /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  };

  const signUp = async (PhoneNumber, email, password) => {
    if (!validateEmail(email)) {
      Alert.alert('Please Enter a valid email');
    } else {
      if (PhoneNumber === '') {
        Alert.alert('Phone Number is required');
      } else {
        if (password === '') {
          Alert.alert('Password is required');
        } else {
          setSpinner(true);
          try {
            const checkValid = phoneInput.current?.isValidNumber(PhoneNumber);
            // setShowMessage(true);
            setValid(checkValid ? checkValid : false);
            // setCountryCode(phoneInput.current?.getCountryCode() || '');
            setCountryCode(phoneInput.current?.getCountryCode());
            let getNumberAfterPossiblyEliminatingZero =
              phoneInput.current?.getNumberAfterPossiblyEliminatingZero();
            console.log(
              'Phone ',
              getNumberAfterPossiblyEliminatingZero.formattedNumber,
            );
            if (checkValid) {
              setSpinner(false);
              navigation.navigate('_otp', {
                Email: email,
                Phone: getNumberAfterPossiblyEliminatingZero.formattedNumber,
                Password: password,
                Country: countryCode,
                // Country: phoneInput.current?.getCountryCode()
              });
            } else {
              setSpinner(false);
              Alert.alert('Sorry! Ivalide phone number');
            }
          } catch (error) {
            setSpinner(false);
            // alert(error.message);
            Alert.alert('Sorry! Something went wrong');
          }
        }
      }
    }
  };
  const checkPasswordStatus = () => {
    if (!showPassword) {
      setShowPassword(true);
    } else {
      setShowPassword(false);
    }
  };

  const goToLogin = () => {
    navigation.navigate('_login');
  };

  return (
    <Container>
      <StatusBar backgroundColor={Constants.PrimaryColor} hidden={false} />
      {/* <StatusBar backgroundColor={'#FFFFFF'} hidden={false} /> */}

      <Spinner
        visible={spinner}
        textContent={'Loading...'}
        textStyle={{color: '#FFFFFF'}}
      />

      <KeyboardAvoidingView
        style={{flex: 1}}
        behavior={Platform.OS == 'ios' ? 'padding' : 'height'}
        keyboardVerticalOffset={Platform.OS === 'ios' ? 40 : 0}>
        <View style={styles.container}>
          <View
            style={{
              width: '100%',
              height: '10%',
              alignItems: 'center',
              justifyContent: 'center',
              backgroundColor: '#FFFFFF',
              flexDirection: 'row',
              borderWidth: 1.5,
              borderColor: '#EEEEEE',
            }}>
            <Feather
              name="chevron-left"
              style={styles.icon_}
              onPress={() => {
                navigation.goBack();
              }}
            />
            <Text
              style={{
                fontSize: 25,
                fontFamily: Constants.Bold,
                color: '#576CA8',
              }}>
              Let's get started
            </Text>
          </View>
          <View
            style={{
              width: '100%',
              height: '75%',
              backgroundColor: '#FFFFFF',
              justifyContent: 'center',
              // alignItems: 'center'
            }}>
            <View style={styles.sectionContainer}>
              <TextInput
                autoFocus
                value={Email}
                onChangeText={setEmail}
                mode="outlined"
                label="Email Address"
                outlineColor={Constants.PrimaryColor}
                placeholder="Type email"
              />
              {!validateEmail(Email) && Email !== '' ? (
                <Text style={{color: 'red', textAlign: 'center'}}>
                  Invalid Email
                </Text>
              ) : null}
            </View>

            <View style={styles.sectionContainer}>
              <PhoneInput
                ref={phoneInput}
                defaultValue={mobileNumber}
                defaultCode="UG"
                layout="first"
                onChangeText={setMobileNumber}
                onChangeFormattedText={(text) => {
                  setFormattedValue(text);
                  // setCountryCode(phoneInput.current?.getCountryCode() || '');
                  setCountryCode(phoneInput.current?.getCountryCode());
                  console.log('Country: ', countryCode);
                }}
                countryPickerProps={{withAlphaFilter: true}}
                disabled={disabled}
                withDarkTheme
                withShadow
                // autoFocus
              />
            </View>

            <View style={styles.sectionContainer}>
              <TextInput
                value={Password}
                onChangeText={setPassword}
                mode="outlined"
                label="Create Password"
                outlineColor={Constants.PrimaryColor}
                placeholder="Type password"
                secureTextEntry={showPassword}
                right={
                  <TextInput.Icon
                    name={showPassword ? 'eye' : 'eye-off'}
                    onPress={checkPasswordStatus}
                  />
                }
              />
            </View>

            <View style={styles.sectionContainer_}>
              <View
                style={{marginBottom: 5, flexDirection: 'row', marginRight: 5}}>
                <Checkbox
                  status={checked ? 'checked' : 'unchecked'}
                  onPress={() => {
                    setChecked(!checked);
                  }}
                />
                <Text style={styles.textLebal}>
                  I accept the{' '}
                  <Text
                    style={styles.highlight}
                    // onPress={() => navigation.navigate('_login')}
                    onPress={() => {
                      // Communications.web(terms);
                      navigation.navigate('_webview', {URL: terms, titleText: 'Terms & Conditions'});
                    }}>
                    Terms of Service{' '}
                  </Text>
                  <Text style={styles.textLebal}>& </Text>
                  <Text
                    style={styles.highlight}
                    // onPress={() => navigation.navigate('_login')}
                    onPress={() => {
                      // Communications.web(privacy_policy);
                      navigation.navigate('_webview', {URL: privacy_policy, titleText: 'Privacy Policy'});
                    }}>
                    Privacy Policy
                  </Text>
                </Text>
              </View>
            </View>
          </View>

          <View
            style={{
              width: '100%',
              height: '15%',
              backgroundColor: '#FFFFFF',
              justifyContent: 'center',
              padding: 5,
              // marginBottom: 15
            }}>
            {mobileNumber === '' ||
            Email === '' ||
            Password === '' ||
            !checked ? (
              <View style={styles.sectionContainer}>
                <TouchableOpacity
                  style={{
                    width: '100%',
                    height: 50,
                    justifyContent: 'center',
                    padding: 10,
                    alignItems: 'center',
                  }}>
                  <View
                    style={{
                      backgroundColor: '#576CA8',
                      width: '100%',
                      height: 50,
                      alignItems: 'center',
                      justifyContent: 'center',
                      borderRadius: 30,
                      opacity: 0.5,
                    }}>
                    <Text
                      style={{
                        color: 'white',
                        fontFamily: Constants.PrimaryColor,
                      }}>
                      SIGN UP
                    </Text>
                  </View>
                </TouchableOpacity>
              </View>
            ) : (
              <View style={styles.sectionContainer}>
                <TouchableOpacity
                  onPress={() => signUp(mobileNumber, Email, Password)}
                  // onPress={() => navigation.navigate('_dashboard')}
                  style={{
                    width: '100%',
                    height: 50,
                    justifyContent: 'center',
                    padding: 10,
                    alignItems: 'center',
                  }}>
                  <View
                    style={{
                      backgroundColor: '#576CA8',
                      width: '100%',
                      height: 50,
                      alignItems: 'center',
                      justifyContent: 'center',
                      borderRadius: 30,
                    }}>
                    <Text
                      style={{
                        color: 'white',
                        fontFamily: Constants.PrimaryColor,
                      }}>
                      SIGN UP
                    </Text>
                  </View>
                </TouchableOpacity>
              </View>
            )}

            <View style={{alignItems: 'center', marginBottom: 5}}>
              <Text style={styles.textLebal}>
                Already have an account?{' '}
                <Text style={styles.highlight} onPress={goToLogin}>
                  Sign In
                </Text>
              </Text>
            </View>
          </View>
        </View>
      </KeyboardAvoidingView>
    </Container>
  );
};

const styles = {
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    // backgroundColor: Constants.PrimaryColor
  },
  scrollView: {
    backgroundColor: '#EEEEEE',
  },
  icon: {
    color: '#FFFFFF',
    alignItems: 'center',
    fontSize: 30,
  },
  icon_: {
    color: '#576CA8',
    fontSize: 30,
    position: 'absolute',
    left: 0,
  },
  sectionContainer: {
    marginLeft: 20,
    marginRight: 20,
    // alignItems: 'center',
    padding: 5,
    borderRadius: 2,
  },
  sectionContainer_: {
    marginLeft: 15,
    marginRight: 28,
    // alignItems: 'center',
    // padding: 5,
    borderRadius: 2,
  },
  sectionContainerPicker: {
    margin: 5,
    paddingHorizontal: 15,
    padding: 5,
    borderWidth: 0.5,
    borderColor: '#BABABA',
    borderRadius: 5,
    backgroundColor: '#FFFFFF',
  },
  imgStyle: {
    resizeMode: 'contain',
    width: 35,
    height: 25,
    borderWidth: 1 / PixelRatio.get(),
    borderColor: '#eee',
    opacity: 0.8,
  },
  button: {
    marginTop: 20,
    height: 50,
    width: 300,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#7CDB8A',
    shadowColor: 'rgba(0,0,0,0.4)',
    shadowOffset: {
      width: 1,
      height: 5,
    },
    shadowOpacity: 0.34,
    shadowRadius: 6.27,
    elevation: 10,
  },
  buttonText: {
    color: 'white',
    fontSize: 14,
  },
  message: {
    borderWidth: 1,
    borderRadius: 5,
    padding: 20,
    marginBottom: 20,
    justifyContent: 'center',
    alignItems: 'flex-start',
  },
  screen: {
    marginLeft: 20,
    marginRight: 20,
  },
  highlight: {
    fontSize: 18,
    fontFamily: Constants.Bold,
    color: Constants.PrimaryColor,
  },
  textLebal: {
    marginBottom: 8,
    fontSize: 16,
    fontFamily: Constants.Regular,
    color: 'black',
  },
};

export default SignUp;
