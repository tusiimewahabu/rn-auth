import React, {useEffect, useState, useContext} from 'react';
import {
  PixelRatio,
  FlatList,
  View,
  Alert,
  StatusBar,
  LogBox,
  BackHandler
} from 'react-native';
import {
  Container,
  Header,
  Row,
  Content,
  List,
  ListItem,
  Body,
  Right,
  Text,
} from 'native-base';
import {useNavigation, useFocusEffect} from '@react-navigation/native';
import AsyncStorage from '@react-native-community/async-storage';
import Spinner from 'react-native-loading-spinner-overlay';
import rnauth_api from '../../api/rnauth_api';
import Constants from '../../config/Constants';
import {Context} from '../../context/utilsContext';

LogBox.ignoreLogs(['Warning: ']);
const TabTwo = () => {
  const [trasactions, setTransactions] = useState([]);
  const [spinner, setSpinner] = useState(false);
  const [isFetching, setIsFetching] = useState(false);
  const navigation = useNavigation();

  const {state, backButtonHandller} = useContext(Context);
  useEffect(() => {
    _loadTransactions();
    backButtonHandller();
    // console.log('check back button status: ', state.backButtonStatus);
  }, [navigation]);

  const _loadTransactions = async () => {
    setSpinner(true);
    try {
      let id = await AsyncStorage.removeItem('userid');
      const response = await rnauth_api.post('/transactions.php', {
        id: id == null ? 1 : id,
      });

      if (response.data !== null) {
        setTransactions(response.data);
        setSpinner(false);
        setIsFetching(false);
      }
    } catch (err) {
      Alert.alert('Sorry!', err);
      setSpinner(false);
    }
  };

  const onRefresh = () => {
    setIsFetching(true);
    _loadTransactions();
  };
      // useFocusEffect get called each time when screen comes in focus
      useFocusEffect(
        React.useCallback(() => {
          const onBackPress = () => {
              navigation.navigate('_dashboard');
            // Return true to stop default back navigaton
            // Return false to keep default back navigaton
            return true;
          };
    
          // Add Event Listener for hardwareBackPress
          BackHandler.addEventListener(
            'hardwareBackPress',
            onBackPress
          );
    
          return () => {
            // Once the Screen gets blur Remove Event Listener
            BackHandler.removeEventListener(
              'hardwareBackPress',
              onBackPress
            );
          };
        }, []),
      );

  return (
    <Container>
      <Header
        backgroundColor={Constants.PrimaryColor}
        style={{backgroundColor: Constants.PrimaryColor}}>
        <Text style={styles.headerText}>Transactions</Text>
      </Header>
      <StatusBar backgroundColor={Constants.PrimaryColor} hidden={false} />
      <Content padder style={styles.container}>
        <Spinner
          visible={spinner}
          textContent={'Loading...'}
          textStyle={{color: '#FFFFFF'}}
        />
        <List>
          <FlatList
            data={trasactions}
            renderItem={({item, index}) => {
              let number = trasactions.length - 1;
              return (
                <>
                  <ListItem
                    avatar
                    style={styles.listItem}
                    onPress={() =>
                      navigation.navigate('_transactionDetails', {
                        t_date: item.transaction_date,
                        t_status: item.status,
                        t_name: item.name,
                        t_baseCurrecncy:
                          item.base_currency + ' ' + item.currency,
                        t_convertedCurrency: item.converted_currency + ' UGX',
                        t_currency: item.currency,
                        t_converted: item.converted_currency,
                        t_baseCurrecnce: item.base_currency,
                      })
                    }>
                    <Row>
                      <Body>
                        <Text style={styles.highlight}>{item.name}</Text>
                        <View style={styles.bodyStyle}>
                          <Text note style={styles.highlight_}>
                            {item.transaction_date}
                          </Text>
                          {item.status === 'Completed' && (
                            <Text note style={styles.highlightStatus}>
                              {item.status}
                            </Text>
                          )}
                          {item.status === 'Paid' && (
                            <Text
                              note
                              style={
                                (styles.highlightStatus,
                                {
                                  color: '#000000',
                                  backgroundColor: '#EEEEEE',
                                  padding: 5,
                                  borderRadius: 15,
                                  fontFamily: 'texgyreadventor-regular',
                                  fontSize: 11,
                                })
                              }>
                              {item.status}
                            </Text>
                          )}
                          {item.status === 'Cancelled' && (
                            <Text
                              note
                              style={
                                (styles.highlightStatus,
                                {
                                  color: 'red',
                                  backgroundColor: '#EEEEEE',
                                  padding: 5,
                                  borderRadius: 15,
                                  fontFamily: 'texgyreadventor-regular',
                                  fontSize: 11,
                                })
                              }>
                              {item.status}
                            </Text>
                          )}
                          {item.status === 'Waiting' && (
                            <Text
                              note
                              style={
                                (styles.highlightStatus,
                                {
                                  color: 'orange',
                                  backgroundColor: '#EEEEEE',
                                  padding: 5,
                                  borderRadius: 15,
                                  fontFamily: 'texgyreadventor-regular',
                                  fontSize: 11,
                                })
                              }>
                              {item.status}
                            </Text>
                          )}
                        </View>
                      </Body>
                      <Right>
                        <Text style={styles.highlight}>
                          {item.base_currency + ' ' + item.currency}
                        </Text>
                        <Text note style={styles.highlight_m}>
                          {item.converted_currency + ' UGX'}
                        </Text>
                      </Right>
                    </Row>
                  </ListItem>

                  {index === number && <View style={{marginBottom: 40}}></View>}
                </>
              );
            }}
            keyExtractor={(item) => item.transaction_id.toString()}
            onRefresh={() => onRefresh()}
            refreshing={isFetching}
          />
        </List>
      </Content>
    </Container>
  );
};

const styles = {
  container: {
    backgroundColor: '#EEEEEE',
  },
  listItem: {
    borderWidth: 0.8,
    borderRadius: 8,
    marginLeft: 0,
    // marginBottom: 10,
    marginBottom: 5,
    // paddingLeft: 5,
    flexDirection: 'column',
    backgroundColor: '#FFFFFF',
  },
  imgStyle: {
    resizeMode: 'cover',
    width: 40,
    height: 40,
    borderWidth: 1 / PixelRatio.get(),
    borderColor: '#eee',
    opacity: 0.8,
    borderRadius: 25,
  },
  highlight: {
    fontFamily: 'texgyreadventor-bold',
    color: '#576CA8',
    fontSize: 14,
  },
  highlight_: {
    fontFamily: 'texgyreadventor-bold',
    fontSize: 13,
  },
  highlight_m: {
    fontFamily: 'texgyreadventor-bold',
    fontSize: 13,
  },
  payState: {
    position: 'absolute',
    padding: 8,
    margin: 8,
    marginTop: 5,
    right: 0,
  },
  bodyStyle: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
  },
  headerText: {
    padding: 5,
    margin: 5,
    fontSize: 20,
    color: '#FFFFFF',
    fontFamily: 'texgyreadventor-bold',
  },
  highlightStatus: {
    fontSize: 11,
    backgroundColor: '#EEEEEE',
    borderRadius: 15,
    padding: 5,
    fontFamily: 'texgyreadventor-regular',
    color: 'green',
  },
  icon: {
    color: '#FFFFFF',
    fontSize: 30,
  },
};
export default TabTwo;
