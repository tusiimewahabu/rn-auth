import React from 'react';
import { StatusBar, View, TouchableOpacity } from 'react-native';
import { Text } from 'native-base';
import Feather from 'react-native-vector-icons/Feather';
import { useNavigation } from '@react-navigation/native';
import RNRestart from 'react-native-restart';
import Constants from '../config/Constants';
const InternetRetry = () => {
  const navigation = useNavigation();
  return (
    <>
      <StatusBar backgroundColor={Constants.PrimaryColor} hidden={false} />

      <View style={styles.container}>
        <View style={{ alignItems: 'center' }}>
          <Feather name='cloud-off' style={styles.icon} onPress={() => { RNRestart.Restart() }} />
          <Text style={{ textAlign: 'center', fontFamily: Constants.Bold, color: '#FFFFFF' }}>Ooops!, Check your internet connection</Text>
          <TouchableOpacity
            onPress={() => RNRestart.Restart()}
          >
            <Text style={{ textAlign: 'center', fontFamily: Constants.Bold, color: '#000000' }}>Try Again</Text>
          </TouchableOpacity>
        </View>
      </View>

    </>
  );
};

const styles = {
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Constants.PrimaryColor
  },
  icon: {
    color: "#FFFFFF",
    alignItems: 'center',
    fontSize: 30
  },
};

export default InternetRetry;