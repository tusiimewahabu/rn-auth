import React, { useState, useEffect } from 'react';
import {
    StyleSheet,
    Text,
    View,
    Button,
    Alert,
    TextInput,
    FlatList,
    StatusBar,
    KeyboardAvoidingView,
    Platform,
    LogBox,
    TouchableOpacity,
} from 'react-native';
import { Footer, FooterTab } from "native-base";
import auth from '@react-native-firebase/auth';
import AsyncStorage from '@react-native-community/async-storage';
import Spinner from 'react-native-loading-spinner-overlay';
import rnauth_api from '../api/rnauth_api';

LogBox.ignoreLogs(["Warning: "]);
import CurrencyComponent from '../components/CurrencyComponent';

var user_name = null;

const Home = ({ navigation }) => {
    const [UserName, setUserName] = useState('');
    const [visibility, setVisibility] = useState(false);
    const [value, setValue] = useState('');
    const [data, setData] = useState(USER_DATA);
    const [user_rate, setUserRate] = useState(USER_DATA[0].rate);
    const [user_currency, setUserCurrency] = useState(USER_DATA[0].country);
    const [user_flag, setUserFlag] = useState(USER_DATA[0].flag);
    const [spinner, setSpinner] = useState(false);

    const showUserList = () => {
        setVisibility(true);
    };

    // const hideUserList = (data) => {
    //     // prevent breaking if no record is found for the searched user
    //     if (data && data.length) {
    //         setUserRate(data[0].rate);
    //         setUserCurrency(data[0].country);
    //         setUserFlag(data[0].flag);
    //         setVisibility(false);
    //         setValue(data[0].name);
    //     } else {
    //         setData(USER_DATA);
    //         setVisibility(false);
    //     }
    // };

    const searchFilterFunction = (text) => {
        setValue(text);
        const newData = data.filter((item) => {
            const itemData = `${item.name.toUpperCase()}`;
            const textData = text.toUpperCase();
            return itemData.indexOf(textData) > -1;
        });
        // controls what data should be displayed in the list
        if (text !== '') {
            setData(newData);
        } else {
            setData(USER_DATA);
        }
    };

    const renderSeparator = () => {
        return <View style={{ height: 1, backgroundColor: '#EAEAEA' }} />;
    };

    useEffect(() => {
        _loadInitialState();
    }, []);

    const _loadInitialState = async () => {
        user_name = await AsyncStorage.getItem('name');
        if (user_name !== null) {
            navigation.navigate('_dashboard');
            setUserName(user_name);
        } else {
            navigation.navigate('_login');
        }
    };

    const signOut = async () => {
        try {
            auth().signOut();
            await AsyncStorage.removeItem('phone');
            await AsyncStorage.removeItem('name');

            Alert.alert('Logged out successsfully');
            navigation.navigate('_login');
            return true;

        } catch (exception) {
            return false;
        }
    };

    const getSelectedUser = (name, user_rate, user_currency, user_flag) => {
        setValue(name);
        setUserRate(user_rate);
        setUserCurrency(user_currency);
        setUserFlag(user_flag);

        setVisibility(false);
    };

    const requestAccess = async () => {
        setSpinner(true);
        try {
            let phone = auth().currentUser.phoneNumber;
            const response = await rnauth_api.post('/login.php', { "phone": phone });

            if (response.data.message === "You have successfully logged in.") {
                // alert(response.data.message);
                setSpinner(false);
                let user_ref = "RN - " + Math.floor(Math.random() * 100000) + 1;
                Alert.alert("Your response number", user_ref.toString());

            } else {
                setSpinner(false);
                alert(response.data.message);
            }
        } catch (err) {
            setSpinner(false);
            //   console.log(err);
            alert('Sorry! Something went wrong');
        }
    };

    return (
        <>
            <StatusBar barStyle="dark-content" />
            <KeyboardAvoidingView
                style={{ flex: 1 }}
                behavior={Platform.OS == 'ios' ? 'padding' : 'height'}
                keyboardVerticalOffset={Platform.OS === 'ios' ? 40 : 0}>
                <View style={styles.container}>
                    <Spinner
                        visible={spinner}
                        textContent={'Loading...'}
                        textStyle={{ color: '#FFFFFF' }}
                    />
                    <View style={styles.textDiv}>
                        <TextInput
                            style={styles.textInput}
                            defaultValue={value}
                            value={value}
                            onChangeText={(text) => searchFilterFunction(text)}
                            placeholder="Enter User"
                            onFocus={showUserList}
                        // onBlur={() => hideUserList(data)}
                        />
                    </View>
                    <View style={styles.resultStyle}>
                        {visibility ? (
                            <View style={styles.resultsComponent}>
                                <FlatList
                                    showsVerticalScrollIndicator={false}
                                    data={data}
                                    renderItem={({ item }) => {
                                        return (
                                            <TouchableOpacity
                                                onPress={() => getSelectedUser(item.name, item.rate, item.country, item.flag)}
                                            >
                                                <View style={styles.listItems}>
                                                    <Text>
                                                        Name: {item.name} | Age: {item.age}
                                                    </Text>
                                                    {renderSeparator()}
                                                </View>
                                            </TouchableOpacity>

                                        );
                                    }}
                                    keyExtractor={(item) => item.id}
                                />
                            </View>
                        ) : (
                            <View style={styles.resultsComp}>
                                {/* Import the currency screen component */}
                                <CurrencyComponent
                                    user_rate={user_rate}
                                    user_currency={user_currency}
                                    user_flag={user_flag}
                                // user_rate={data[0].rate}
                                // user_currency={data[0].country}
                                // user_flag={data[0].flag}
                                />
                                <View style={{ justifyContent: 'space-between', flexDirection: 'row', margin: 5, padding: 5 }}>
                                    <Button title={'Send Request'} onPress={requestAccess} />
                                    {/* <Button title={'SignOut'} onPress={signOut} /> */}
                                </View>
                                {/* <Footer style={{ backgroundColor: '#EEEEEE', marginTop: 15 }}>
                                    <FooterTab style={{ backgroundColor: '#EEEEEE' }}>
                                        <View style={styles.footerButton}>
                                            <View style={{ justifyContent: 'center', flexDirection: 'row' }}>
                                                <Text style={styles.footerText} onPress={() => navigation.navigate('_home')}>{'Home'}</Text>
                                                <Text style={styles.footerText} onPress={() => navigation.navigate('_screenTwo')}>{'Page 2'}</Text>
                                                <Text style={styles.footerText} onPress={() => navigation.navigate('_screenThree')}>{'Page 3'}</Text>
                                            </View>
                                        </View>
                                    </FooterTab>
                                </Footer> */}
                                <Footer style={{ backgroundColor: '#EEEEEE', marginTop: 15 }}>
                                    <FooterTab style={{ backgroundColor: '#EEEEEE' }}>
                                        <View style={styles.footerButton}>
                                            <View style={{ justifyContent: 'center', flexDirection: 'row' }}>
                                                <Text numberOfLines={2} style={styles.footerText}>{'Hi: ' + user_name}</Text>
                                                <Text numberOfLines={2} style={styles.footerText}>{'Tel: ' + auth().currentUser.phoneNumber}</Text>
                                            </View>
                                        </View>
                                    </FooterTab>
                                </Footer>
                            </View>
                        )}
                    </View>
                </View>
            </KeyboardAvoidingView>
        </>
    );
};

const styles = StyleSheet.create({
    scrollView: {
        backgroundColor: '#EEEEEE',
    },
    container: {
        position: 'relative',
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
    textDiv: {
        flex: 2 / 3,
        alignSelf: 'stretch',
        paddingBottom: 8,
        marginBottom: 8,
        padding: 5,
        marginTop: 5,
    },
    resultStyle: {
        flex: 8,
        alignSelf: 'stretch',
    },
    buttonStyle: {
        flex: 2 / 3,
        alignSelf: 'stretch',
        marginHorizontal: 5,
        paddingBottom: 5,
        marginBottom: 5,
    },
    textInput: {
        flex: 1,
        position: 'absolute',
        height: 40,
        borderColor: 'gray',
        borderWidth: 1,
        marginHorizontal: 5,
        borderRadius: 5,
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
    },
    resultsComponent: {
        flex: 1,
        marginHorizontal: 5,
        borderBottomLeftRadius: 5,
        borderBottomRightRadius: 5,
    },
    resultsComp: {
        flex: 1,
        marginHorizontal: 5,
        borderBottomLeftRadius: 5,
        borderBottomRightRadius: 5,
        justifyContent: 'flex-end',
        marginBottom: 5,
    },
    listItems: {
        marginHorizontal: 1,
        height: 45,
    },
    altText: {
        height: 30,
        alignItems: 'center',
        justifyContent: 'center',
    },
    footerText: {
        padding: 5,
        margin: 5,
        fontSize: 14
    }
});

const USER_DATA = [
    { id: 1, name: '', age: 20, country: 'USD', rate: 1.10, flag: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACgAAAAeCAMAAABpA6zvAAAABGdBTUEAALGPC/xhBQAAAAFzUkdCAK7OHOkAAAAgY0hSTQAAeiYAAICEAAD6AAAAgOgAAHUwAADqYAAAOpgAABdwnLpRPAAAAj1QTFRF4bm96r/B6b/B3a6y5bO15bK1wltjyF1j8unr+vDx+vDwyHB2z3J31ZSZ3JicGS9dJTplITdjGjBdKDxnHjNhGzFfKT1oHzZjQjJXtzxFvT1EGjBedYKdXWyNFStaIDVih5OrRFV7FCpZLkJsjpmvdYKeGC5cWmyNQDFWuD5Hvj9GM0dwRlh9NEdwOEtzLEBqTF2BLUFrOUx0M0dvPVB2KT9pTFJ1HDJfeIWgFy1bJjpmhI+oNklxFi1bS12BY3KRFixbUmOF9/f5////aHaUVGSGIjdjPlF3Gi9dfYqkU2SGaHeVUGKFSk5ySlt/PlB3MkZvMUVuKz9qV2eJKz9pO093QDFVtjpCvDpBFSxbgo6nUWKEEilYJjtmj5qwOUxzEihYFCtaa3mXFCxbRDpfRFZ7KDxoJzxnXW2NKT5pYXCQQFJ5UF6BTl+DHjNgcH6aO051HjRhYnGQS1yAUGCD9fT1/vv8/vv7Fy1cfYmjT1+DiZSsZ3aUFS1cRkBkO011M0ZvMERtQVN5QlR6LEBrQ1V7Ok10N0pyL0VvQTBVtTc/uzc+coCcW2qLhJCpLkJri5atc4CcWGmLR0drHzRhITZiaXiWRVd8c4GdIDZiWGiKJTpmPE92YG+PL0NtKj5pJztnIDZjTlh76c/S8dbX8dXXdoOfXm2NiZWsEylZL0JskJuxd4SfFy5cW22OQDNXukZOwUZNwEZNJDllPVB3P1F4MkdwSzxfu0ZOzdLczNHbzdHbzNDb1tPb8NXXWNrergAAAA90Uk5T/v7+/v7+/v7+/v7+/v7+6a2FXwAAAAFiS0dEQ2fQDWIAAAAJcEhZcwAAAEgAAABIAEbJaz4AAAITSURBVDjLY+AXEOQXEhbhFxUT5RcRFuIXFODnFxCXkJRCAwzSMrJy8gqKSsoqykqKCvJysqpqaqrqGppaaICBX1tZR1dP38DQyNBAX09XR9nYxMTY1IyBkQkVMPDzm1sYWFpZ29jaWFtZ2lmYq9nbqzk4OjmjAQZpF1dpNwt3D0NPQw93CzdpL29zc28fX2YWVlTAwO/nryzmFRAYFBwUGOAlpuzvFxLiFxoWHoEGGPgjpaOiY2Lj4hPi42JjoqOkE5OS5JJT2NjRAIOQY2paeoZNpn6WfqZNRnpaqmNsrGN2DgcnFypg4M/KzXPLLyjUV9UvLMh3y8stKiwsKi4pLStHBQz8FaKVVYmx1XpKetWxiVWVog41Nba1ddw8aICBv77Bv7HJsLmltaXZsKnRv6Gtvb2to7OrGw0w8Pf0ysn3NSf2T+hPbO6Tl+udqKY2cdJkXj40wMA/Zeq06XnpM7TztGek502fNjNv1qxCaSwBzp8+e9YcMeG58+bPmyssNmfW7AXFxQsWLlq8ZCkqYOBftlxJfkXqylWrV61MXSGvtHzN2rVr1q3fsHETKmAQMNLdHLtFefP8rfM3K2+J3axrJCBgtG37DgyFO3ft2rlz1+6dO/fs3LkbxATjvfuWolvtTCRg4CMSMHQTCRh4iAQM5UQCBi4iAQM7kYAhgkjAwEokID7AmYgEDFpEAgYpIgEA2hc6qEvsr/QAAAAldEVYdGRhdGU6Y3JlYXRlADIwMTMtMTAtMDdUMTM6MTU6MTUrMDI6MDDoZk8yAAAAJXRFWHRkYXRlOm1vZGlmeQAyMDEzLTEwLTA3VDEzOjE1OjE1KzAyOjAwmTv3jgAAAABJRU5ErkJggg==' },
    { id: 2, name: 'Susan', age: 21, country: 'YEN', rate: 1.12, flag: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACgAAAAeCAMAAABpA6zvAAAABGdBTUEAALGPC/xhBQAAAAFzUkdCAK7OHOkAAAAgY0hSTQAAeiYAAICEAAD6AAAAgOgAAHUwAADqYAAAOpgAABdwnLpRPAAAAGlQTFRF//////7+//z8/vz8/PDw9cfH8a6u98/P4lpa1hIS0gAA0QAA41pa//396Hp60gEB0AAA0wEB1AMDzwAA0wQE1AQE0wAA41tb1AYG9svL0gMD0wIC4l1d/O/v1hcX9cjI8a+v6Ht799DQeS2ujAAAAAFiS0dEAIgFHUgAAAAJcEhZcwAAAEgAAABIAEbJaz4AAADRSURBVDjLzZPZDoMgEEUH0FrBBdz37f8/sqBJqynqPDW9LyTk5A7M3AH4IxHKGCW3GGXbyei1m8Yc9/FwHY2SS857+lwI7gfeBUlCiGLJVZIoLuMIwjOSQJBmuSq0VJ6lJZyADKK0WrEVrdIImI0LoW7aN6fJtqn1pc2wk3mxUy47myWBfuBqDyo+9JZnUhiPnCFHoJbKk0iOYCImS200iC6N/gy6PfiGf49wto/QPLNEhcLEbP7EbD6PmQnuEmzBLZe7iKNWAdDLtbri1vWHegEoyhjkyVR7IwAAACV0RVh0ZGF0ZTpjcmVhdGUAMjAxMy0xMC0wN1QxMzoxNDoyNSswMjowMIkrI+8AAAAldEVYdGRhdGU6bW9kaWZ5ADIwMTMtMTAtMDdUMTM6MTQ6MjUrMDI6MDD4dptTAAAAAElFTkSuQmCC' },
    { id: 3, name: 'Robert', age: 22, country: 'GBP', rate: 1.13, flag: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACgAAAAeCAMAAABpA6zvAAAABGdBTUEAALGPC/xhBQAAAAFzUkdCAK7OHOkAAAAgY0hSTQAAeiYAAICEAAD6AAAAgOgAAHUwAADqYAAAOpgAABdwnLpRPAAAAmFQTFRFAABhAwNoAgJnAABmAQFnAABjERBv7vL5//LwzhARywAAzAEB7/L5AABgIyN7AABcAwNpAAFgAABYJCd/+Pj7np7FIiJ6AABbAQFhAQFgAABXIyZ/obDX+Ozt/v7+////m5vDnq3V+e/w95WO0hoZ//39/v///f7++e7v+JeQ0RkYwQAAyAMD2kdH8r29/v7/+O7vyAAA20dHwgAA2kRE8sDAAgJiExJwExJxAgJh95aP1B0cyQAA2kNDJCR8AABZEhFq7vL67/L6AABVJSmBnq7XmqnT8+js9ZOO0BcXxQAA2Ds78bq6/f3++fn7l5fBNDOE6+/3//PxNDWGmaTO+err94+I0BMT/v398/P4l5fAMzWHscDg/O/v5oSE0RwczAICzQYG3FFR9c/P+/n6/v3+9vX4/P7//O7tzxER/O3t+/7/9/L0+qeg1SQkxwAAzQcH8r+//vv8/vr7rq3NMjGDAABl8/b85ejy39/s/Pz+/fj4/fT0/fX1+ePj++rq/P///O7u++3t/ff49d3f++zs/ff3/P3+3+Pw5un08/b7//Ty//by++nq/PDw/PLy+N7ezxAQ+N3d/PHx++7u/fb2++3u//Xy0BER0BISzAAA/fPz++np//Tx4OPw3+DtAQBlMzKDrq3O//7+/vr62kRD1SUk9vT43FJS0Rsc5oODssHhNDaH0BQT95CJ+evrmKPONDSG6+737O/3MzKElpbA2Dw89JON8+jrm6rT0hkY+JiRnq7WmprD9/f68sHB1B0bIyd/IiJ78r6+2khI0hsa9+ztoa/XIiZ/ISF6nZ3ECbA7hQAAAAFiS0dEHwUNEL0AAAAJcEhZcwAAAEgAAABIAEbJaz4AAAKASURBVDjLjdT3XxJxGAfwB3AwAnEUaMWjGFJpKoKWZalpQ3OkTStLUzNQczSwBPFQNLO8LjFXe++9h2W7/qpucKQir/z8cD88937dfcfz/QIIhCI+AYFBwWKJVCaTSsRzggIDvC+EAjkoQv5RP1CoDA0Lh4i581RqpdA/FCojo+YvWAgajIhWqNTsV2eADAuP0cYuAl2c3kt9IMcWL1kanwDLEjGJp4KpUMCyZJoZUhLBaDCl8jRNtNwLg1eI0nlGizhYucqYsToVNcxY12RmZfNwbVZO7jotzxBh/YaNefmGTQWoL8Si4s0lpRwsLdmyddv2HSzDnVi2C+S795Tv3VdRub8Kq2vwQO1BMwPNlrr6hkONTaZmxJbDNUeOHgNr6/ET2GaztzsQsUODBAed2Nnl6rbpkEnPyd5TIO47bTnTT5J2m5MgCBd5lmIgdW7APWgjSZeDLp4nhvrEIJFIzMNmiqLMbEYo2SgdGTXC1LjisJlGIJ2eUTY+ZZBNDwd9yrOHs/41Mxku7OAnTYYv0k9mMuK+IYJeGKfDRZJjg+4BfnlIF+EmnGN2kuy/YBm6KAZr76UedlV1tm5XVyc6uQV3o6aDLjra7bY27LncaoUrV69dv9GC2HyzqfFWQ32dxbOFtbexphqr7lRW3L1Xfv8BQHEZveeYajLEP3z0+MnTZ3xTPH/xsggL9VjwKiM/7/Wbt4B0DzHs3Xvth9ycyW2Wqfg4zrTfJ1OG8XMs8Cw5ZiIqfWrjpqm+RI+jPommBiMkpvAsUulzFNQ8/foNErxsxsPloXE6+B6r9TA/x5WjGvjx85eH+b0A1KrffyJgIixU+b8rRQAhCpDP7pKCv22M7TE/NUpRAAAAJXRFWHRkYXRlOmNyZWF0ZQAyMDEzLTEwLTA3VDEzOjE0OjM4KzAyOjAwJFZCsQAAACV0RVh0ZGF0ZTptb2RpZnkAMjAxMy0xMC0wN1QxMzoxNDozOCswMjowMFUL+g0AAAAASUVORK5CYII=' },
    { id: 4, name: 'Tom', age: 23, country: 'CAD', rate: 1.14, flag: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACgAAAAeCAMAAABpA6zvAAAABGdBTUEAALGPC/xhBQAAAAFzUkdCAK7OHOkAAAAgY0hSTQAAeiYAAICEAAD6AAAAgOgAAHUwAADqYAAAOpgAABdwnLpRPAAAATtQTFRF5Zysvwowvwkv/////vr7//7+/vz85Jmo9drg//3+9NTbxiNF34OX/v3978HL6rC82nOJvgYsyC1O67G95Jen/PP17LbBwxg7wRE2vwkwvwsxwBA11Ft09djexR9CvwguvgYt4Iea+u3w/ff47bnE89LZ4Y2f2WyD9+HmzDpZvgcuwA4067K+67O/1Fx189HY7bvF9tvh12Z9wxg8wRM4vwwx23eM0Etnvgctwxs+4Y6gxylKxB1A8MfQ56KwwRI3wA0zwhU5yjNT+urt2nCGvgguxSNF/fj5whY60VBr56OxwA4z2W2E+ert7LfCyCxM4Iqc/PX345KjvgUswA0yvQMqzT9c/PL045Sl6q262GmA5p+u34OW34aZ+/Dz//393oGV9tzh/vz923aL9NXc/vv88s/W8MTN+uvuEtZ6YwAAAAF0Uk5T/YPqVscAAAABYktHRAMRDEzyAAAACXBIWXMAAABIAAAASABGyWs+AAAA60lEQVQ4y2NghAImBmYsgIEJJs8wRBWysBKnkI2dgxiFnMxc3DzMvKwEFfLxCwgKCYuIElQoJi4hKSUtw0ZAoaiorJw8I6OCIrMSHoXKKqpq6hqaWoyM2jq6evoGhrgUshkZm5iamVswMllaWTPayNnitNpOkolJ2x7IcXBkYlRwcsalUNQFLgxkuLrhNNFdzcOVEc5n8uTAaTWbl5wWjOvt44sneFj9/CEyTI4BgfgDPCjYNUTbWzI0LJxAzPCbRURGRUfHxMYRUBjPzMybkAh0BCvhZJaUnEJcekxVTyNOYXx6xuDPhXRSCABQTCDtFbfICAAAACV0RVh0ZGF0ZTpjcmVhdGUAMjAxMy0xMC0wN1QxMzoxNToxNyswMjowMH/5XhsAAAAldEVYdGRhdGU6bW9kaWZ5ADIwMTMtMTAtMDdUMTM6MTU6MTcrMDI6MDAOpOanAAAAAElFTkSuQmCC' },
    { id: 5, name: 'Jane', age: 24, country: 'GHC', rate: 1.15, flag: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACgAAAAeCAMAAABpA6zvAAAABGdBTUEAALGPC/xhBQAAAAFzUkdCAK7OHOkAAAAgY0hSTQAAeiYAAICEAAD6AAAAgOgAAHUwAADqYAAAOpgAABdwnLpRPAAAAbBQTFRF6AAG5wAG7AAG6wAG6AEG5wEG7wEGyAAFzwAF7gEG5AAF+wAGbAADgAAD6hEI6REI6RAI8hIIJgIBPAMC9xII5xAI/eMc++Ec/+sd074YAAAADAkB4swZ/+kd//ge//kf//of+/Qe//8hiogRnJoT+/Ue//ke//wf//Me//Qe/PAe+Owd//ce//Ue/vQeOzgHSkYJ/vIe8uYc/vMefXcPHRwDMC4GMS8GLSsFKScFBQUBBwYBJyYFKCYFLCoFJiUEFxYDiYMQ//Ye/PMee3UOAQEAk4wR/vUe/fUeycEYJCIEBAMABAQAMC0G1s8a//8g+/Ad//4g8uwdV1IKAgIAZF4M+PMe+O0dnp0UBAQBAgMBAwMBpqQV//Yd/fQd//8fS0kJAwQBUE4K/PMd8Owh7+oh9fAi3NgeEREDAAABcXISbnASExMD3tof9PAhLoBFLn9EMYlKIlkvAw4IGFk1PJBIPI9IAw4JG3ZJG3VIHHxNCCQWCB4SHnNFIX9MGHJICSATIHlIIHhIIHpJHnJEFU4uIHxLIHtKH3ZHIH1LFlAwH3hIIH5LH3dHH3dI////gfZjOgAAAAFiS0dEj/UCg/kAAAAJcEhZcwAAAEgAAABIAEbJaz4AAAFUSURBVDjLY2AY5oCRkUh1TMzYVLKgA1Y2dg5OVgxhTJ1c3Dy83FyY4nxogF9AUEhYRJQfXZxBDA2IS0hKScvIiqOLM8ghgLwCkFBUUpaSUlFSVVNTU1dDkmTQgANNLW0FHV09eX0pKQN1LUNNXSMthKQGgyYCGJuYmplbWFpJSVnb2NrZOzjqGCMkkRQaOzm7SAGBKwi5Slm5KbhrYFWoqeGh5Okl5Q1S7CPl6+fvjKQORaGmXkBgULBUiBQQhYapByBLaTIYIwO9cKUIqcioqGipGP9wPRQphlgUEBefIJUoJRUplRSfjCrDkIIMUtPSM6Qys7IzpXJy81JRpBjykUFBYZFUcUlpWUm5VFFhAYoUQwUyqKyqrqmtq6+vq22orqpEkWJoRAJNzS2tbe0djY0d7Z1dLc1NyHIM3aigp7sXRPX29aBJoCvsxWDgUIgTDAWFADjaJV+3MAkgAAAAJXRFWHRkYXRlOmNyZWF0ZQAyMDEzLTEwLTA3VDEzOjE0OjQ4KzAyOjAwLpNLqAAAACV0RVh0ZGF0ZTptb2RpZnkAMjAxMy0xMC0wN1QxMzoxNDo0OCswMjowMF/O8xQAAAAASUVORK5CYII=' },
    { id: 6, name: 'Agnes', age: 25, country: 'EUR', rate: 1.16, flag: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACgAAAAeBAMAAACs80HuAAAABGdBTUEAALGPC/xhBQAAAAFzUkdCAK7OHOkAAAAgY0hSTQAAeiYAAICEAAD6AAAAgOgAAHUwAADqYAAAOpgAABdwnLpRPAAAACFQTFRFAAD/AAD+AADmAAALAAAAAAABAQEBCwsL5ubm/////v7+G0fY8QAAAAFiS0dECfHZpewAAAAJcEhZcwAAAEgAAABIAEbJaz4AAAA7SURBVCjPY2AYfEAQC8CuUgkLYDDGAhhcsACGUCwAu0oKBdOwAOwqy7EAhg4sgGEmFsCwCgvArnJABQESX7dJCCjvGwAAACV0RVh0ZGF0ZTpjcmVhdGUAMjAxMy0xMC0wN1QxMzoxNDo0OCswMjowMC6TS6gAAAAldEVYdGRhdGU6bW9kaWZ5ADIwMTMtMTAtMDdUMTM6MTQ6NDgrMDI6MDBfzvMUAAAAAElFTkSuQmCC' },
    { id: 7, name: 'Aisha', age: 26, country: 'KSH', rate: 1.20, flag: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACgAAAAeCAMAAABpA6zvAAAABGdBTUEAALGPC/xhBQAAAAFzUkdCAK7OHOkAAAAgY0hSTQAAeiYAAICEAAD6AAAAgOgAAHUwAADqYAAAOpgAABdwnLpRPAAAAsRQTFRFAAAABAQEBQUFMDAwFBQUAgEBQ0NDDw8PPz8/hoaGCwsLHwkLcyIpNxATXFxch4eHAgICGBgYz8/Pf39/AgMDJAgLoktS02hxuFliPBAUQEBA4+PjYGBgmZmZz9DQLBwdlCgx3pWc2oaN4qSpqzZAJggLb3Bw0tLSJSUlAQEBGxsbYV9fhjY9wztH57u+68bJyU1XjikyXVVVKCgopKWloqWlnHx/vDpGxD9K68XJ2oWM79DTyVVfwDZCqXZ6oqSk+uXn9uPlwnd9wTRAwjxI57y/7czPyFFbwjM/xF1m7tna++XnyElUyUpVrUFKWx4jvDVBwjZC3Jmf5bS4xEJNxTdDgzM6ykpViSYuIAkLrDE7wzVCy15n0nF60nd/wjdDxDhEWxofRRMXwTZCxThEcCAnCgMDmSs1wjhExkhTPxIWKgwOuTRAwzdDYRshjCgw7cPD13yCwjVBMQ4RHQgKsTI9XRoghyYvxTZCzVtk+/Pz4qCluzVALg0QGAcIrjE8ZBwjiycwxTdExUBM35Waz2JqMg4RGggJsDI9dyIpCgMEmCs0wDdCQBIWJQoNtzQ/xDVBkCcwIwkMqzE7wzVBzGJr03R9ylpjWxogPRAUvjM/35ed4Jieyo2SazM3vDRA36Cm3I6U3Zqgwz9Kiigwh2Bj4pmf/////P7+y4iO6sPH3I2U6Lm9x05ZvUZR5dfZbbxtbb5ubJRUsztAxD9L3IyT68LGyVdgwzNBiVxAabdoAIcAAIYALZMtk2FOwjlF6cDD6bzAyVBbtD1DXoVHAokDAIkACYsJXKddKIAalUAx4Jqh4KGnwEFLUl0cUKFRKZUpAIgAPJs8Qp1CKm4NrWVd1nJ9yGJqZlUjBYQBGZAaXKZcBooGAokCPpk+DowOK3IQi0w1Vl0eB4QCAIoAQZtBHZAdAogCCIIDAocBBIkEA4kDbtigRwAAAAFiS0dEp8C3KwMAAAAJcEhZcwAAAEgAAABIAEbJaz4AAAGhSURBVDjLY2AYJIARic3EiEchMwuIZAXrYWPHo5CDk4uBgZuHF8jk4xfAo1BQSFhEVExcQpJBSloGnxsFZOXkFRSVlFVU1dQ10OQ0kQGDlraOrp6SvoGhkbGAJqqUCQowNTO3sLSytrG1s0eVMGFwQAGOTs4urkpu7h6eXt6oMgw+yMDXzz8gMEgpOCTUMCwcRcaHIRAZhEZERkXHxMbFJyQmJaPIBDLEI4OU1LT0lIxMi0DbrOycXBQpNIV5rPmhPgWFRQHFJaX4FCaUMZRXVFZVF9XU1tXjNbGBtbGpuaW1KKCtvQOvws6u7pQMl+T4nt6+fjSFzshgwsRJk6dMnTY9PmHGzFkoMs4Ms5HBnLnz5icvWLhocdOSpctQZGYzLEcBK1Y6u6xavWZt0br1qBLLGTaggI2bNm9x27pt+46du1AlNjDsRgF79u7bf2D1wUOHjxxFldjNcAwFHD9x8tTp1WfOnjt/4SKqDKrCi5cuH7ty9dr1Gzdv3b6DT+Hde/ePHXvw8NHjJxefPsOn8NhzEPHiJZB49RqvQtyA+goBOCjWcpsYznAAAAAldEVYdGRhdGU6Y3JlYXRlADIwMTMtMTAtMDdUMTM6MTQ6NDArMDI6MDAdfAXPAAAAJXRFWHRkYXRlOm1vZGlmeQAyMDEzLTEwLTA3VDEzOjE0OjQwKzAyOjAwbCG9cwAAAABJRU5ErkJggg==' },
    { id: 8, name: 'Julius', age: 27, country: 'TZS', rate: 1.21, flag: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACgAAAAeCAMAAABpA6zvAAAABGdBTUEAALGPC/xhBQAAAAFzUkdCAK7OHOkAAAAgY0hSTQAAeiYAAICEAAD6AAAAgOgAAHUwAADqYAAAOpgAABdwnLpRPAAAAPxQTFRFAJkAA5oABJkAAJEALqoA0OoA//8A9fYAZ2cAAAAAAQEAAZkABJsAAJUAApcAc8YA9PkAxMQAKioAApoAA5kAKqkAxeYA9PYAcHEAAgIAApYAdccA9vkAwMIAJicAAwMAxOUA9PUABAQAKCgBc3UD9fYCJygBwcMD8vgKcXID9PYDwOQ7LarRwcME9fkHdceHApb9AJT/xOU4KqnUAJH/A5n8A5r8dMaIAJX/BJv7AZn+AJn/Apr9cHIDAJQAdscALaoAweQA8/kAcnQA9vkHJycAxOU3dceIcXIC9PYCxeY2KysAxcUB8/kJcsaKaGgA0OorLqrQBJn7////k/P2mwAAAAFiS0dEU3pnHQYAAAAJcEhZcwAAAEgAAABIAEbJaz4AAADxSURBVDjLjcx5WwFRGIZxjN56TrK00BDTKEubKGsnVEqFtHz/D9OcsV0xJ+/99339fD59/oAR3KDNLQgA+m07tBOORCm26376cW/fOIhT4lDAxD+jwyVTR5TOwJp80HPHZDtcVkw+k8cJCzzOwgmHywKnORaXL1CRwYmzc7q4ZHBXVLouVzjczW21VmdwjXKzdSdZ3L2UksfNRm+uveCm43rOHTmcGv0cTo2dZHc9p8aHRwanxicOp0a7hyXueZVT48sy1/fg1AjrL/fqxamRxbkih5uPJsTbOw203GwUGI7oY6znpqPAZ46+vqs/danvF4pIe3odrT25AAAAJXRFWHRkYXRlOmNyZWF0ZQAyMDEzLTEwLTA3VDEzOjE0OjM3KzAyOjAw0h4yWAAAACV0RVh0ZGF0ZTptb2RpZnkAMjAxMy0xMC0wN1QxMzoxNDozNyswMjowMKNDiuQAAAAASUVORK5CYII=' },
    { id: 9, name: 'Alex', age: 10, country: 'NRA', rate: 1.24, flag: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACgAAAAeBAMAAACs80HuAAAABGdBTUEAALGPC/xhBQAAAAFzUkdCAK7OHOkAAAAgY0hSTQAAeiYAAICEAAD6AAAAgOgAAHUwAADqYAAAOpgAABdwnLpRPAAAABVQTFRFNqEAOaIDKpsAvN+p/////P77u9+patqPNQAAAAFiS0dEBI9o2VEAAAAJcEhZcwAAAEgAAABIAEbJaz4AAAAdSURBVCjPY2AAAUZlVxcgCEkSYECAUcFRQRoJAgDKtUNjjeTzkwAAACV0RVh0ZGF0ZTpjcmVhdGUAMjAxMy0xMC0wN1QxMzoxNTowMCswMjowMHb0YAsAAAAldEVYdGRhdGU6bW9kaWZ5ADIwMTMtMTAtMDdUMTM6MTU6MDArMDI6MDAHqdi3AAAAAElFTkSuQmCC' },
    { id: 10, name: 'Allen', age: 28, country: 'YEN', rate: 1.25, flag: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACgAAAAeCAMAAABpA6zvAAAABGdBTUEAALGPC/xhBQAAAAFzUkdCAK7OHOkAAAAgY0hSTQAAeiYAAICEAAD6AAAAgOgAAHUwAADqYAAAOpgAABdwnLpRPAAAAGlQTFRF//////7+//z8/vz8/PDw9cfH8a6u98/P4lpa1hIS0gAA0QAA41pa//396Hp60gEB0AAA0wEB1AMDzwAA0wQE1AQE0wAA41tb1AYG9svL0gMD0wIC4l1d/O/v1hcX9cjI8a+v6Ht799DQeS2ujAAAAAFiS0dEAIgFHUgAAAAJcEhZcwAAAEgAAABIAEbJaz4AAADRSURBVDjLzZPZDoMgEEUH0FrBBdz37f8/sqBJqynqPDW9LyTk5A7M3AH4IxHKGCW3GGXbyei1m8Yc9/FwHY2SS857+lwI7gfeBUlCiGLJVZIoLuMIwjOSQJBmuSq0VJ6lJZyADKK0WrEVrdIImI0LoW7aN6fJtqn1pc2wk3mxUy47myWBfuBqDyo+9JZnUhiPnCFHoJbKk0iOYCImS200iC6N/gy6PfiGf49wto/QPLNEhcLEbP7EbD6PmQnuEmzBLZe7iKNWAdDLtbri1vWHegEoyhjkyVR7IwAAACV0RVh0ZGF0ZTpjcmVhdGUAMjAxMy0xMC0wN1QxMzoxNDoyNSswMjowMIkrI+8AAAAldEVYdGRhdGU6bW9kaWZ5ADIwMTMtMTAtMDdUMTM6MTQ6MjUrMDI6MDD4dptTAAAAAElFTkSuQmCC' },
    { id: 11, name: 'Annet', age: 30, country: 'GBP', rate: 1.26, flag: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACgAAAAeCAMAAABpA6zvAAAABGdBTUEAALGPC/xhBQAAAAFzUkdCAK7OHOkAAAAgY0hSTQAAeiYAAICEAAD6AAAAgOgAAHUwAADqYAAAOpgAABdwnLpRPAAAAmFQTFRFAABhAwNoAgJnAABmAQFnAABjERBv7vL5//LwzhARywAAzAEB7/L5AABgIyN7AABcAwNpAAFgAABYJCd/+Pj7np7FIiJ6AABbAQFhAQFgAABXIyZ/obDX+Ozt/v7+////m5vDnq3V+e/w95WO0hoZ//39/v///f7++e7v+JeQ0RkYwQAAyAMD2kdH8r29/v7/+O7vyAAA20dHwgAA2kRE8sDAAgJiExJwExJxAgJh95aP1B0cyQAA2kNDJCR8AABZEhFq7vL67/L6AABVJSmBnq7XmqnT8+js9ZOO0BcXxQAA2Ds78bq6/f3++fn7l5fBNDOE6+/3//PxNDWGmaTO+err94+I0BMT/v398/P4l5fAMzWHscDg/O/v5oSE0RwczAICzQYG3FFR9c/P+/n6/v3+9vX4/P7//O7tzxER/O3t+/7/9/L0+qeg1SQkxwAAzQcH8r+//vv8/vr7rq3NMjGDAABl8/b85ejy39/s/Pz+/fj4/fT0/fX1+ePj++rq/P///O7u++3t/ff49d3f++zs/ff3/P3+3+Pw5un08/b7//Ty//by++nq/PDw/PLy+N7ezxAQ+N3d/PHx++7u/fb2++3u//Xy0BER0BISzAAA/fPz++np//Tx4OPw3+DtAQBlMzKDrq3O//7+/vr62kRD1SUk9vT43FJS0Rsc5oODssHhNDaH0BQT95CJ+evrmKPONDSG6+737O/3MzKElpbA2Dw89JON8+jrm6rT0hkY+JiRnq7WmprD9/f68sHB1B0bIyd/IiJ78r6+2khI0hsa9+ztoa/XIiZ/ISF6nZ3ECbA7hQAAAAFiS0dEHwUNEL0AAAAJcEhZcwAAAEgAAABIAEbJaz4AAAKASURBVDjLjdT3XxJxGAfwB3AwAnEUaMWjGFJpKoKWZalpQ3OkTStLUzNQczSwBPFQNLO8LjFXe++9h2W7/qpucKQir/z8cD88937dfcfz/QIIhCI+AYFBwWKJVCaTSsRzggIDvC+EAjkoQv5RP1CoDA0Lh4i581RqpdA/FCojo+YvWAgajIhWqNTsV2eADAuP0cYuAl2c3kt9IMcWL1kanwDLEjGJp4KpUMCyZJoZUhLBaDCl8jRNtNwLg1eI0nlGizhYucqYsToVNcxY12RmZfNwbVZO7jotzxBh/YaNefmGTQWoL8Si4s0lpRwsLdmyddv2HSzDnVi2C+S795Tv3VdRub8Kq2vwQO1BMwPNlrr6hkONTaZmxJbDNUeOHgNr6/ET2GaztzsQsUODBAed2Nnl6rbpkEnPyd5TIO47bTnTT5J2m5MgCBd5lmIgdW7APWgjSZeDLp4nhvrEIJFIzMNmiqLMbEYo2SgdGTXC1LjisJlGIJ2eUTY+ZZBNDwd9yrOHs/41Mxku7OAnTYYv0k9mMuK+IYJeGKfDRZJjg+4BfnlIF+EmnGN2kuy/YBm6KAZr76UedlV1tm5XVyc6uQV3o6aDLjra7bY27LncaoUrV69dv9GC2HyzqfFWQ32dxbOFtbexphqr7lRW3L1Xfv8BQHEZveeYajLEP3z0+MnTZ3xTPH/xsggL9VjwKiM/7/Wbt4B0DzHs3Xvth9ycyW2Wqfg4zrTfJ1OG8XMs8Cw5ZiIqfWrjpqm+RI+jPommBiMkpvAsUulzFNQ8/foNErxsxsPloXE6+B6r9TA/x5WjGvjx85eH+b0A1KrffyJgIixU+b8rRQAhCpDP7pKCv22M7TE/NUpRAAAAJXRFWHRkYXRlOmNyZWF0ZQAyMDEzLTEwLTA3VDEzOjE0OjM4KzAyOjAwJFZCsQAAACV0RVh0ZGF0ZTptb2RpZnkAMjAxMy0xMC0wN1QxMzoxNDozOCswMjowMFUL+g0AAAAASUVORK5CYII=' },
];

export default Home;