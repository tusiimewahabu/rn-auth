import React, { useState, useEffect } from 'react';
import {
    StyleSheet,
    Text,
    View,
    StatusBar,
    Alert,
    KeyboardAvoidingView,
    TouchableOpacity
} from 'react-native';
import auth from '@react-native-firebase/auth';
import Spinner from 'react-native-loading-spinner-overlay';
import { TextInput } from 'react-native-paper';
import rnauth_api from '../api/rnauth_api';
import { Container } from 'native-base';
import { useNavigation } from '@react-navigation/native';
import Feather from 'react-native-vector-icons/Feather';
import Constants from '../config/Constants';

const Otp = ({ route }) => {
    const navigation = useNavigation();
    const [code, setCode] = useState("");
    const [spinner, setSpinner] = useState(false);
    const [userId, setUserId] = useState("");
    const [confirm, setConfirm] = useState(null);
    const [message, setMessage] = useState("");

    console.log("Props", route.params);

    useEffect(() => {
        senCode();
        // register();
    }, []);

    const senCode = async () => {
        setSpinner(true);
        try {
            if (route.params.Phone !== "") {
                const confirmation = await auth().signInWithPhoneNumber(route.params.Phone);
                setConfirm(confirmation);
                setSpinner(false);
            }
        } catch (error) {
            setSpinner(false);
            // alert(error.message);
            Alert.alert('Sorry! Something went wrong');
        }
    };

    const confirmVerificationCode = async (code) => {
        try {
            await confirm
                .confirm(code)
                .then(user => {
                    //   this.setState({ userId: user.uid });
                    setSpinner(user.uid);
                    if (userId) {
                        Alert.alert("SignUp process failed. Please try again");
                    } else {
                        if (confirm !== null) {
                            let user_ref = auth().currentUser.uid;
                            register(user_ref);
                            Alert.alert("UserId: " + auth().currentUser.uid);
                        }
                    }
                });

        } catch (error) {
            Alert.alert('Invalid code');
            setSpinner(false);
        }
    }

    const register = async (u_ref) => {
        try {
            const response = await rnauth_api.post('/register.php',
                {
                    "first_name": "NA",
                    "last_name": "NA",
                    "email": route.params.Email,
                    "phone": route.params.Phone,
                    "firebase_ref_id": u_ref,
                    // "firebase_ref_id": "FTY657huyj987UI",
                    "password": route.params.Password,
                    "country":route.params.Country
                }
            );
            //   this.setState({ message: response.data.message });
            setMessage(response.data.message);

            if (response.data.message === "Account created successfully" || response.data.message === "Account created but email was not sent") {
                // this.setState({ spinner: false });
                setSpinner(false);
                Alert.alert(response.data.message);
                navigation.navigate('_login');
            } else {
                // this.setState({ spinner: false });
                setSpinner(false);
                Alert.alert(response.data.message);
            }
        } catch (err) {
            //   this.setState({ spinner: false });
            setSpinner(false);
            console.log(err);
            Alert.alert(err);
        }
    };

    return (
        <Container>
            <StatusBar backgroundColor={Constants.PrimaryColor} hidden={false} />
            <Spinner
                visible={spinner}
                textContent={'Loading...'}
                textStyle={{ color: '#FFFFFF' }}
            />

            <KeyboardAvoidingView
                style={{ flex: 1 }}
                behavior={Platform.OS == 'ios' ? 'padding' : 'height'}
                keyboardVerticalOffset={Platform.OS === 'ios' ? 40 : 0}>

                <View style={styles.container}>

                    <View style={{
                        width: '100%',
                        height: '10%',
                        alignItems: 'center',
                        justifyContent: 'center',
                        backgroundColor: '#FFFFFF',
                        flexDirection: 'row',
                        marginBottom: 5,
                        borderWidth: 1.5,
                        borderColor: '#EEEEEE',
                    }}>
                        <Feather name='chevron-left' style={styles.icon_} onPress={() => { navigation.goBack() }} />
                        <Text style={{ fontSize: 20, fontFamily: Constants.Bold, color: Constants.PrimaryColor }}>Confirm Code</Text>
                    </View>

                    <View style={{
                        width: '100%',
                        height: '75%',
                        backgroundColor: '#FFFFFF',
                        justifyContent: 'center',
                    }}>

                        <View style={styles.sectionContainer}>
                            <TextInput
                                value={code}
                                onChangeText={setCode}
                                keyboardType="numeric"
                                maxLength={6}
                                mode="outlined"
                                label="Enter Code"
                                outlineColor={Constants.PrimaryColor}
                                placeholder="Type Code"
                            />
                        </View>
                    </View>
                    <View style={{
                        width: '100%',
                        height: '15%',
                        backgroundColor: '#FFFFFF',
                        justifyContent: 'center',
                        padding: 5,
                    }}>

                        <View style={styles.sectionContainer}>
                            {code === "" ? <TouchableOpacity
                                style={{ width: '100%', height: 50, justifyContent: 'center', padding: 10, alignItems: 'center' }}>
                                <View style={{ backgroundColor: '#576CA8', width: '100%', height: 50, alignItems: 'center', justifyContent: 'center', borderRadius: 30, opacity: 0.5 }}>
                                    <Text style={{ color: 'white', fontFamily: Constants.PrimaryColor, }}>Submit</Text>
                                </View>
                            </TouchableOpacity>
                                :
                                <TouchableOpacity
                                    onPress={() => confirmVerificationCode(code)}
                                    style={{ width: '100%', height: 50, justifyContent: 'center', padding: 10, alignItems: 'center' }}>
                                    <View style={{ backgroundColor: '#576CA8', width: '100%', height: 50, alignItems: 'center', justifyContent: 'center', borderRadius: 30 }}>
                                        <Text style={{ color: 'white', fontFamily: Constants.PrimaryColor, }}>Submit</Text>
                                    </View>
                                </TouchableOpacity>
                            }
                        </View>
                    </View>
                </View>
            </KeyboardAvoidingView>
        </Container>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    icon_: {
        color: Constants.PrimaryColor,
        fontSize: 30,
        position: 'absolute',
        left: 10
    },
    sectionContainer: {
        marginLeft: 20,
        marginRight: 20,
        padding: 5,
        borderRadius: 2,
    },
});

export default Otp;