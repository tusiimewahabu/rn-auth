import React, {useState} from 'react';
import {Container, Tab, Tabs, TabHeading, Text, View} from 'native-base';
import Feather from 'react-native-vector-icons/Feather';
import {SafeAreaView} from 'react-native-safe-area-context';
import Tab1 from './tabs/tabOne';
import Tab2 from './tabs/tabTwo';
import Tab3 from './tabs/tabThree';
import Constants from '../config/Constants';
import styles from './styles';

const Dashboard = () => {
  const [screenIndex, setScreenIndex] = useState(0);
  // console.log(screenIndex);

  return (
    <SafeAreaView
      style={{backgroundColor: Constants.PrimaryColor, flex: 1}}
      forceInset={{top: 'never'}}>
      <Container>
        <Tabs
          locked={true}
          tabBarPosition="bottom"
          tabBarUnderlineStyle={styles.tabBarUnderline}
          style={{backgroundColor: Constants.PrimaryColor}}
          onChangeTab={(g) => {
            // console.log(g.i);
            setScreenIndex(g.i);
          }}>
          <Tab
            heading={
              <TabHeading style={styles.tabHeading}>
                <View>
                  <Feather name="home" style={styles.icon} />
                  <Text
                    style={{color: '#FFFFFF', fontFamily: Constants.Regular}}>
                    Home
                  </Text>
                </View>
              </TabHeading>
            }>
            <Tab1 tabStatus={screenIndex} />
          </Tab>
          <Tab
            heading={
              <TabHeading style={styles.tabHeading}>
                <View>
                  <Feather name="list" style={styles.icon_} />
                  <Text
                    style={{color: '#FFFFFF', fontFamily: Constants.Regular}}>
                    Transactions
                  </Text>
                </View>
              </TabHeading>
            }>
            <Tab2 />
          </Tab>
          <Tab
            heading={
              <TabHeading style={styles.tabHeading}>
                <View>
                  <Feather name="settings" style={styles.icon} />
                  <Text
                    style={{color: '#FFFFFF', fontFamily: Constants.Regular}}>
                    Settings
                  </Text>
                </View>
              </TabHeading>
            }>
            <Tab3 />
          </Tab>
        </Tabs>
      </Container>
    </SafeAreaView>
  );
};

export default Dashboard;
