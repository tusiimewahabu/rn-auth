import React, { useState } from 'react';
import {
    StyleSheet,
    Text,
    View,
    StatusBar,
    Alert,
    KeyboardAvoidingView,
    TouchableOpacity,
    SafeAreaView
} from 'react-native';
import auth from '@react-native-firebase/auth';
import Spinner from 'react-native-loading-spinner-overlay';
import { TextInput } from 'react-native-paper';
import rnauth_api from '../api/rnauth_api';
import { Container } from 'native-base';
import Feather from 'react-native-vector-icons/Feather';
import Constants from '../config/Constants';

const ChangePassword = ({ navigation }) => {

    const [oldPassword, setOldPassword] = useState("");
    const [newPassword, setNewPassword] = useState("");
    const [spinner, setSpinner] = useState(false);

    const changePassword = async (OldPassword, NewPassword) => {
        if (OldPassword === '' && OldPassword.length < 6) {
            Alert.alert('Old Password is required or must be greater than 5 chars');
        } else {
            if (NewPassword === '' && NewPassword.length < 6) {
                Alert.alert('New Password is required or must be greater than 5 chars');
            } else {
                setSpinner(true);
                try {
                    let phone = auth().currentUser.phoneNumber;
                    const response = await rnauth_api.post('/change_password.php', {
                        "old_password": OldPassword,
                        "new_password": NewPassword,
                        "phone": phone
                    });

                    if (response.data.message === "password changed successfully") {
                        Alert.alert("Success ", response.data.message);
                        setSpinner(false);
                    } else {
                        setSpinner(false);
                        Alert.alert("Sorry ", response.data.message);
                    }
                } catch (err) {
                    setSpinner(false);
                    //   console.log(err);
                    alert('Sorry! Something went wrong');
                }
            }
        }
    };
    return (
        <SafeAreaView
        style={{backgroundColor: Constants.PrimaryColor, flex: 1}}
        forceInset={{top: 'never'}}>
        <Container>
            <StatusBar backgroundColor={Constants.PrimaryColor} hidden={false} />
            <Spinner
                visible={spinner}
                textContent={'Loading...'}
                textStyle={{ color: '#FFFFFF' }}
            />

            <KeyboardAvoidingView
                style={{ flex: 1 }}
                behavior={Platform.OS == 'ios' ? 'padding' : 'height'}
                keyboardVerticalOffset={Platform.OS === 'ios' ? 40 : 0}>

                <View style={styles.container}>

                    <View style={{
                        width: '100%',
                        height: '10%',
                        alignItems: 'center',
                        justifyContent: 'center',
                        backgroundColor: Constants.PrimaryColor,
                        flexDirection: 'row',
                        marginBottom: 5
                    }}>
                        <Feather name='chevron-left' style={styles.icon_} onPress={() => { navigation.goBack() }} />
                        <Text style={{ fontSize: 20, fontFamily: Constants.Bold, color: '#FFFFFF' }}>Change Password</Text>
                    </View>

                    <View style={{
                        width: '100%',
                        height: '75%',
                        backgroundColor: '#FFFFFF',
                        justifyContent: 'center',
                    }}>

                        <View style={styles.sectionContainer}>
                            <TextInput
                                autoCapitalize="none"
                                autoCorrect={false}
                                value={oldPassword}
                                onChangeText={setOldPassword}
                                mode="outlined"
                                label="Old Password"
                                outlineColor={Constants.PrimaryColor}
                                placeholder="Type old password"
                            />
                        </View>
                        <View style={styles.sectionContainer}>
                            <TextInput
                                autoCapitalize="none"
                                autoCorrect={false}
                                value={newPassword}
                                onChangeText={setNewPassword}
                                mode="outlined"
                                label="New Password"
                                outlineColor={Constants.PrimaryColor}
                                placeholder="Type new password"
                            />
                        </View>
                    </View>
                    <View style={{
                        width: '100%',
                        height: '15%',
                        backgroundColor: '#FFFFFF',
                        justifyContent: 'center',
                        padding: 5,
                    }}>

                        <View style={styles.sectionContainer}>
                            {oldPassword === "" || newPassword === "" ? <TouchableOpacity
                                // onPress={() => changePassword(oldPassword, newPassword)}
                                style={{ width: '100%', height: 50, justifyContent: 'center', padding: 10, alignItems: 'center' }}>
                                <View style={{ backgroundColor: '#576CA8', width: '100%', height: 50, alignItems: 'center', justifyContent: 'center', borderRadius: 30, opacity: 0.5 }}>
                                    <Text style={{ color: 'white', fontFamily: Constants.Bold, }}>Save</Text>
                                </View>
                            </TouchableOpacity> :
                                <TouchableOpacity
                                    onPress={() => changePassword(oldPassword, newPassword)}
                                    style={{ width: '100%', height: 50, justifyContent: 'center', padding: 10, alignItems: 'center' }}>
                                    <View style={{ backgroundColor: '#576CA8', width: '100%', height: 50, alignItems: 'center', justifyContent: 'center', borderRadius: 30 }}>
                                        <Text style={{ color: 'white', fontFamily: Constants.Bold, }}>Save</Text>
                                    </View>
                                </TouchableOpacity>}
                        </View>
                    </View>
                </View>
            </KeyboardAvoidingView>
        </Container>
        </SafeAreaView>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    icon_: {
        color: "#FFFFFF",
        fontSize: 25,
        position: 'absolute',
        left: 10
    },
    sectionContainer: {
        marginLeft: 20,
        marginRight: 20,
        padding: 5,
        borderRadius: 2,
    },
});

export default ChangePassword;