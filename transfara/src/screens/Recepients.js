import React, { useState, useRef, useEffect } from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    Text,
    View,
    StatusBar,
    // TextInput,
    Button,
    Image,
    Alert,
    TouchableOpacity,
    KeyboardAvoidingView,
    PixelRatio
} from 'react-native';
import Spinner from 'react-native-loading-spinner-overlay';
import AsyncStorage from '@react-native-community/async-storage';
import { Container, Header, Content, SwipeRow, Card, Row, List, ListItem, Body, Title, Left, Right, Icon, Picker } from 'native-base';
import Feather from 'react-native-vector-icons/Feather';
import rnauth_api from '../api/rnauth_api';
import Constants from '../config/Constants';
import { TextInput, Checkbox, HelperText } from 'react-native-paper';
import PhoneInput from "react-native-phone-number-input";
import Countries from '../config/Countries';
import cca from '../config/cca';

const app_logo = require('../../assets/diamond.png');

const Recepients = ({ navigation }) => {

    // let cca2 = cca[0].toString();

    // console.log("COUNTRY DATA ", Countries);

    // console.log("COUNTRY CODE ", cca.length);


    const [full_name, setFullName] = useState('');
    const [bank_name, setBankName] = useState('');
    const [bank_account, setBankAccount] = useState('');
    const [bank_address, setBankAddress] = useState('');
    const [CountryName, setCountryName] = useState('');
    const [mobileNumber, setMobileNumber] = useState('');
    const [callingCode, setCollingCode] = useState('');
    const [payment_method, setPaymentMethod] = useState('');
    const [message, setMessage] = useState('');
    const [spinner, setSpinner] = useState(false);
    const [userId, setUserId] = useState('');
    const [code, setCode] = useState('');
    const [method_status, setMethodStatus] = useState(false);
    const [ex_rate_id, setExRateId] = useState(1);
    const [currency_rates, setCurrencyRate] = useState([]);
    const [select_country, setSelectCountry] = useState('');
    const [formattedValue, setFormattedValue] = useState('');
    const [countryCode, setCountryCode] = useState('');
    const [disabled, setDisabled] = useState(false);
    const [showMessage, setShowMessage] = useState(false);
    const [confirmAccount, setConfirmAccount] = useState('');
    const [valid, setValid] = useState(false);

    const [flag, setFlag] = useState('');
    const [currency, setCurrency] = useState('');
    const [country, setCountry] = useState(Countries);

    const phoneInput = useRef(null);

    useEffect(() => {
        // { country === "Uganda"? setFlag(Countries.UG.flag):null }
        // { country === "Kenya"? setFlag(Countries.KE.flag):null }
        // { country === "Tanzania"? setFlag(Countries.TZ.flag):null }

        // { country === "Uganda"? setCurrency(Countries.UG.currency):null }
        // { country === "Kenya"? setCurrency(Countries.KE.currency):null }
        // { country === "Tanzania"? setCurrency(Countries.TZ.currency):null }
    }, []);

    const signUp = async (Country, PhoneNumber, FullName, BankAccount, PayMethod, ConfirmAccount, BankName, BankAddress) => {
        if (Country === '') {
            Alert.alert('Country name is required');
        } else {
            if (FullName === '') {
                Alert.alert('Full Name is required');
            } else {
                if (PhoneNumber === '' && PayMethod === 'Mobile Money') {
                    Alert.alert('Phone Number is required');
                } else {
                    if (PayMethod === '') {
                        Alert.alert('Payment methode is required');
                    } else {
                        if (BankName === '' && PayMethod === 'Bank') {
                            Alert.alert('Bank Name is required');
                        } else {
                            if (BankAccount === '' && PayMethod === 'Bank') {
                                Alert.alert('Bank Account is required');
                            } else {
                                if (BankAccount !== ConfirmAccount) {
                                    Alert.alert('Sorry!', "Account number mismatch");
                                } else {
                                    if (BankAddress === '' && PayMethod === 'Bank') {
                                        Alert.alert('Bank Address is required');
                                    } else {
                                        const checkValid = phoneInput.current?.isValidNumber(mobileNumber);
                                        // setShowMessage(true);
                                        setValid(checkValid ? checkValid : false);
                                        setCountryCode(phoneInput.current?.getCountryCode() || '');
                                        let getNumberAfterPossiblyEliminatingZero = phoneInput.current?.getNumberAfterPossiblyEliminatingZero();
                                        // console.log("Phone ",getNumberAfterPossiblyEliminatingZero.formattedNumber);

                                        // console.log('ID', user_id);

                                        if (!checkValid && PayMethod === 'Mobile Money') {
                                            Alert.alert('Sorry! Ivalide phone number');
                                        } else {
                                            setSpinner(true);
                                            try {
                                                let user_id = await AsyncStorage.getItem('userid');
                                                let b_account = `${PayMethod === 'Bank' ? BankAccount : 'NA'}`;
                                                let m_number = `${PayMethod === 'Mobile Money' ? getNumberAfterPossiblyEliminatingZero.formattedNumber : 'NA'}`;

                                                if (Country === "Uganda") {
                                                    setFlag(Countries.UG.flag);
                                                    setCurrency(Countries.UG.currency);
                                                }

                                                if (Country === "Kenya") {
                                                    setFlag(Countries.KE.flag);
                                                    setCurrency(Countries.KE.currency);
                                                }

                                                if (Country === "Tanzania") {
                                                    setFlag(Countries.TZ.flag);
                                                    setCurrency(Countries.TZ.currency);
                                                }

                                                console.log("DATA :", currency + " " + flag + " " + payment_method + " " + bank_account + " " + mobileNumber);

                                                // setSpinner(false);

                                                if (currency != "" && flag != "") {
                                                    const response = await rnauth_api.post('/add_user.php',
                                                        {
                                                            "full_name": full_name,
                                                            "country": country,
                                                            "method": payment_method,
                                                            "bank_name": PayMethod === 'Bank' ? bank_name : 'NA',
                                                            "bank_account": b_account,
                                                            "bank_address": PayMethod === 'Bank' ? bank_address : 'NA',
                                                            "phone_number": m_number,
                                                            "receiptient_number": m_number,
                                                            "user_id": user_id,
                                                            "currency": currency,
                                                            "flag": flag,
                                                            "ex_rate_id": ex_rate_id
                                                        }
                                                    );
                                                    if (response.data.message === "User added successfully") {
                                                        setSpinner(false);
                                                        Alert.alert(response.data.message);
                                                        navigation.navigate('_dashboard');
                                                    } else {
                                                        setSpinner(false);
                                                        Alert.alert(response.data.message);
                                                    }
                                                } else {
                                                    setSpinner(false);
                                                    Alert.alert("Sorry!, Please try again");
                                                }

                                            } catch (error) {
                                                setSpinner(false);
                                                alert(error);
                                                // Alert.alert('Sorry! Something went wrong');
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    };

    const renderCoutries = () => {
        let countries = [];
        for (var i = 0; i < cca.length; i++) {
            // typeof(cc)
            // Countries.filter((result) => result[cca[i]].toString() === "UG")

            if (Countries[cca[i]].name.common === 'Uganda') {
                countries.push(
                    <Picker.Item label={Countries[cca[i]].name.common} value={Countries[cca[i]].name.common} />
                )
            } else {
                if (Countries[cca[i]].name.common === 'Kenya') {
                    countries.push(
                        <Picker.Item label={Countries[cca[i]].name.common} value={Countries[cca[i]].name.common} />
                    )
                } else {

                    if (Countries[cca[i]].name.common === 'Tanzania') {
                        countries.push(
                            <Picker.Item label={Countries[cca[i]].name.common} value={Countries[cca[i]].name.common} />
                        )
                    } else {

                    }
                }
            }
        }
        return countries;
    }
    return (
        <SafeAreaView
        style={{backgroundColor: Constants.PrimaryColor, flex: 1}}
        forceInset={{top: 'never'}}>
        <Container>
            <StatusBar backgroundColor={Constants.PrimaryColor} hidden={false} />
            <KeyboardAvoidingView
                style={{ flex: 1 }}
                behavior={Platform.OS == 'ios' ? 'padding' : 'height'}
                keyboardVerticalOffset={Platform.OS === 'ios' ? 40 : 0}>

                <View style={{
                    width: '100%',
                    height: '10%',
                    alignItems: 'center',
                    justifyContent: 'center',
                    backgroundColor: '#FFFFFF',
                    flexDirection: 'row',
                    marginBottom: 5
                }}>
                    <Feather name='chevron-left' style={styles.icon_} onPress={() => { navigation.goBack() }} />
                    <Text style={{ fontSize: 20, fontFamily: "texgyreadventor-bold", color: '#576CA8' }}>Add recipient</Text>
                </View>

                <View style={{
                    width: '100%',
                    height: '80%',
                    backgroundColor: '#EEEEEE',
                }}>
                    <ScrollView
                        contentInsetAdjustmentBehavior="automatic"
                        style={styles.scrollView}>
                        <Spinner
                            visible={spinner}
                            textContent={'Loading...'}
                            textStyle={{ color: '#FFFFFF' }}
                        />
                        <View style={styles.body}>
                            <View style={styles.sectionContainer}>
                                <Text style={{ color: '#576CA8', fontFamily: "texgyreadventor-bold", fontSize: 13 }}>COUNTRY</Text>
                                <View style={{ flexDirection: 'row' }}>
                                    <View style={{ justifyContent: 'center' }}>

                                        {country === "Uganda" && <Image
                                            style={styles.imgStyle}
                                            source={{ uri: Countries.UG.flag }}
                                        />}

                                        {country === "Kenya" && <Image
                                            style={styles.imgStyle}
                                            source={{ uri: Countries.KE.flag }}
                                        />}

                                        {country === "Tanzania" && <Image
                                            style={styles.imgStyle}
                                            source={{ uri: Countries.TZ.flag }}
                                        />}
                                    </View>

                                    <Picker
                                        style={{ marginLeft: -11, fontFamily: "texgyreadventor-bold" }}
                                        mode="dropdown"
                                        placeholder="Uganda"
                                        note={true}
                                        selectedValue={country}
                                        onValueChange={setCountry}
                                    >
                                        <Picker.Item label="Select Country" value="" />
                                        {renderCoutries()}
                                    </Picker>
                                </View>
                            </View>
                            <View style={styles.sectionContainer}>
                                <Text style={{ color: '#576CA8', fontFamily: "texgyreadventor-bold", fontSize: 13 }}>PAYOUT METHOD</Text>
                                <Picker
                                    style={{ marginLeft: -11, fontFamily: "texgyreadventor-bold" }}
                                    mode="dropdown"
                                    placeholder="Select Payout Method"
                                    note={true}
                                    selectedValue={payment_method}
                                    // onValueChange={(paymet_method) => this.setState({ paymet_method })}
                                    onValueChange={setPaymentMethod}
                                >
                                    <Picker.Item label="Select Payout Method" value="" />
                                    <Picker.Item label={"Bank"} value={"Bank"} />
                                    <Picker.Item label={"Mobile Money"} value={"Mobile Money"} />
                                </Picker>
                            </View>

                            {payment_method === 'Bank' && (
                                <View>
                                    <View style={styles.sectionContainer}>
                                        <TextInput
                                            autoCapitalize="none"
                                            autoCorrect={false}
                                            // keyboardType={'numeric'}
                                            value={bank_name}
                                            onChangeText={setBankName}
                                            mode="outlined"
                                            label="Bank Name"
                                            outlineColor={Constants.PrimaryColor}
                                            placeholder="Type name"
                                        />
                                    </View>
                                    <View style={styles.sectionContainer}>
                                        <TextInput
                                            autoCapitalize="none"
                                            autoCorrect={false}
                                            keyboardType={'numeric'}
                                            value={bank_account}
                                            onChangeText={setBankAccount}
                                            mode="outlined"
                                            label="Bank Account"
                                            outlineColor={Constants.PrimaryColor}
                                            placeholder="Type account"
                                        />
                                    </View>
                                    <View style={styles.sectionContainer}>
                                        <TextInput
                                            autoCapitalize="none"
                                            autoCorrect={false}
                                            contextMenuHidden={true}
                                            keyboardType={'numeric'}
                                            value={confirmAccount}
                                            onChangeText={setConfirmAccount}
                                            mode="outlined"
                                            label="Confirm Account"
                                            outlineColor={Constants.PrimaryColor}
                                            placeholder="Type account"
                                        />
                                    </View>
                                    <View style={styles.sectionContainer}>
                                        <TextInput
                                            autoCapitalize="none"
                                            autoCorrect={false}
                                            value={bank_address}
                                            onChangeText={setBankAddress}
                                            mode="outlined"
                                            label="Billing Address"
                                            outlineColor={Constants.PrimaryColor}
                                            placeholder="Type address"
                                        />
                                    </View>
                                </View>
                            )}
                            {payment_method === 'Mobile Money' && (
                                <View>
                                    <View style={styles.sectionContainer}>
                                        <PhoneInput
                                            ref={phoneInput}
                                            defaultValue={mobileNumber}
                                            defaultCode="UG"
                                            layout="first"
                                            onChangeText={setMobileNumber}
                                            onChangeFormattedText={(text) => {
                                                setFormattedValue(text);
                                                setCountryCode(phoneInput.current?.getCountryCode() || '');
                                            }}
                                            countryPickerProps={{ withAlphaFilter: true }}
                                            disabled={disabled}
                                            withDarkTheme
                                            withShadow
                                        />
                                    </View>
                                </View>
                            )}
                            <View style={styles.sectionContainer}>
                                <TextInput
                                    value={full_name}
                                    onChangeText={setFullName}
                                    mode="outlined"
                                    label="Full Name"
                                    outlineColor={Constants.PrimaryColor}
                                    placeholder="Type name"
                                />
                            </View>
                        </View>
                    </ScrollView>

                </View>

                <View style={{
                    width: '100%',
                    height: '10%',
                    backgroundColor: '#EEEEEE',
                    justifyContent: 'center'
                }}>
                    {payment_method === "" && full_name === "" && <TouchableOpacity
                        style={{ width: '100%', height: 50, justifyContent: 'center', padding: 15 }}>
                        <View style={{ backgroundColor: '#576CA8', width: '100%', height: 50, alignItems: 'center', justifyContent: 'center', borderRadius: 30, opacity: 0.8 }}>
                            <Text style={{ color: 'white', fontFamily: "texgyreadventor-bold" }}>Save</Text>
                        </View>
                    </TouchableOpacity>}
                    {payment_method !== "" && full_name !== "" && <TouchableOpacity
                        onPress={() => signUp(country, mobileNumber, full_name, bank_account, payment_method, confirmAccount, bank_name, bank_address)}
                        style={{ width: '100%', height: 50, justifyContent: 'center', padding: 15 }}>
                        <View style={{ backgroundColor: '#576CA8', width: '100%', height: 50, alignItems: 'center', justifyContent: 'center', borderRadius: 30 }}>
                            <Text style={{ color: 'white', fontFamily: "texgyreadventor-bold" }}>Save</Text>
                        </View>
                    </TouchableOpacity>}
                </View>

            </KeyboardAvoidingView>
        </Container>
        </SafeAreaView>
    );
};

const styles = StyleSheet.create({
    scrollView: {
        backgroundColor: '#EEEEEE',
    },
    logo: {
        backgroundColor: '#EEEEEE',
        alignItems: 'center',
    },
    logoSize: {
        height: 100,
        width: 100,
    },
    body: {
        // backgroundColor: '#FFFFFF',
        marginHorizontal: 8,
        borderRadius: 5,
    },
    sectionContainer: {
        marginLeft: 10,
        marginRight: 10,
        marginTop: 5,
        paddingTop: 5,
        paddingRight: 8,
        paddingLeft: 8,
        paddingBottom: 8,
        borderRadius: 5,
        backgroundColor: '#FFFFFF',
        borderWidth: 0.5,
        borderColor: '#BABABA'
    },
    sectionContainer_: {
        marginLeft: 20,
        marginRight: 28,
        // alignItems: 'center',
        // padding: 5,
        borderRadius: 2,
    },
    sectionContainerPicker: {
        margin: 5,
        paddingHorizontal: 15,
        padding: 5,
        borderWidth: 0.5,
        borderColor: '#BABABA',
        borderRadius: 5,
        backgroundColor: '#FFFFFF'
    },
    input: {
        marginTop: 8,
        fontSize: 18,
        fontWeight: '400',
        borderRadius: 5,
        fontFamily: "texgyreadventor-rgular"
    },
    highlight: {
        fontWeight: '700',
    },
    textLebal: {
        marginTop: 8,
        fontSize: 18,
        fontWeight: '400',
        color: 'black',
    },
    row1: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        // alignItems: 'center', 
        // padding: 5
    },
    screen: {
        margin: 5,
        paddingHorizontal: 15,
        padding: 5,
    },
    _input: {
        marginTop: 8,
        fontSize: 18,
        fontWeight: '400',
        borderColor: 'lightblue',
        marginVertical: 30,
        borderWidth: 1,
        borderRadius: 5,
    },
    text: {
        fontWeight: '700', alignSelf: 'center'
    },
    icon_: {
        color: "#576CA8",
        fontSize: 30,
        position: 'absolute',
        left: 0
    },
    imgStyle: {
        resizeMode: 'contain',
        width: 35,
        height: 25,
        borderWidth: 1 / PixelRatio.get(),
        borderColor: '#eee',
        opacity: 0.8,
    },
});

export default Recepients;