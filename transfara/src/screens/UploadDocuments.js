import React, { useEffect, useState } from 'react';
import { StatusBar, BackHandler, StyleSheet, View, TouchableOpacity, Image, PixelRatio, Alert, NativeModules } from 'react-native';
import { Container, Header, Left, Body, Right, Title, Button, Text } from 'native-base';
import Feather from 'react-native-vector-icons/Feather';
import { useNavigation } from '@react-navigation/native';
// import RNFetchBlob from 'rn-fetch-blob';
import Spinner from 'react-native-loading-spinner-overlay';
import { launchImageLibrary } from 'react-native-image-picker';
import DocumentPicker from 'react-native-document-picker';
import Constants from '../config/Constants';
const RNFetchBlob = NativeModules.RNFetchBlob;

const url = "http://51.38.70.212/app-backend/rnauth_api";

const UploadDocuments = () => {
  const navigation = useNavigation();
  const [spinner, setSpinner] = useState(false);
  const [proof_of_address, setProofOfAddress] = useState(null);
  const [id_photo, setIdPhoto] = useState(null);

  // useEffect(()=>{
  //   // if(navigation.isFocused()){
  //     BackHandler.removeEventListener("hardwareBackPress", ()=>{});
  //   // }
  // },[]);

  const uploadDocuments = async () => {
    // Check if any file is selected or not
    if (proof_of_address != null && id_photo != null) {
      setSpinner(true);
      // If file selected then create FormData
      const proof_of_address_upload = proof_of_address;
      const id_photo_upload = id_photo;
      { console.log("IMG DATA: ", JSON.stringify(proof_of_address_upload)) }
      { console.log("IMG DATA: ", JSON.stringify(id_photo_upload)) }
      // const form_data = new FormData();
      // form_data.append('proof_of_address', JSON.stringify(proof_of_address_upload));
      // form_data.append('id_photo', JSON.stringify(id_photo_upload));

      try {
        RNFetchBlob.fetch('POST', url + '/upload_documents.php', {
          'Content-Type': 'multipart/form-data',
        }, [
          { name: 'proof_of_address', filename: proof_of_address_upload[0].name, type: proof_of_address_upload[0].type, data: RNFetchBlob.wrap(proof_of_address_upload.toString()) },
          { name: 'id_photo', filename: id_photo_upload[0].name, type: id_photo_upload[0].type, data: RNFetchBlob.wrap(id_photo_upload.toString()) },
        ]
        ).then((resp) => {

          let responseJson = resp.json();
          if (responseJson.status == 1) {
            setSpinner(false);
            Alert.alert('Upload Successful');
          } else {
            setSpinner(false);
            console.log('Response Message = ', responseJson.msg);
            Alert.alert('Sorry!, Something went wrong');
          }

        }).catch((err) => {
          setSpinner(false);
          console.log('Response Message', err);
        })

      } catch (error) {
        setSpinner(false);
        console.log(error);
      }
    } else {
      setSpinner(false);
      // If no file selected the show alert
      Alert.alert('Sorry!, id or address fil missing');
    }
  };

  const selectPlaceFile = async () => {
    // Opening Document Picker to select one file
    try {
      const res = await DocumentPicker.pick({
        // Provide which type of file you want user to pick
        type: [DocumentPicker.types.allFiles],
      });
      // Printing the log realted to the file
      console.log('res : ' + JSON.stringify(res));
      // Setting the state to show single file attributes
      let file_data = [{
        "uri": res[0].uri,
        "type": res[0].type,
        "name": res[0].name,
        "size": res[0].size
      }];
      setProofOfAddress(file_data);
    } catch (err) {
      setProofOfAddress(null);
      // Handling any exception (If any)
      if (DocumentPicker.isCancel(err)) {
        // If user canceled the document selection
        Alert.alert('Canceled');
      } else {
        // For Unknown Error
        Alert.alert('Unknown Error: ' + JSON.stringify(err));
        throw err;
      }
    }
  };

  const selectIdFile = async () => {
    // Opening Document Picker to select one file
    try {
      const res = await DocumentPicker.pick({
        // Provide which type of file you want user to pick
        type: [DocumentPicker.types.allFiles],
      });
      // Printing the log realted to the file
      console.log('res : ' + JSON.stringify(res));
      // Setting the state to show single file attributes
      let file_data = [{
        "uri": res[0].uri,
        "type": res[0].type,
        "name": res[0].name,
        "size": res[0].size
      }];
      setIdPhoto(file_data);
    } catch (err) {
      setIdPhoto(null);
      // Handling any exception (If any)
      if (DocumentPicker.isCancel(err)) {
        // If user canceled the document selection
        Alert.alert('Canceled');
      } else {
        // For Unknown Error
        Alert.alert('Unknown Error: ' + JSON.stringify(err));
        throw err;
      }
    }
  };


  return (
    <Container>
      <Header backgroundColor={Constants.PrimaryColor} style={{ backgroundColor: Constants.PrimaryColor }}>
        <Left>
          <Button transparent>
            <Feather name='chevron-left' style={styles.icon} onPress={() => { navigation.goBack() }} />
          </Button>
        </Left>
        <Body>
          <Title style={{ fontFamily: Constants.Bold }}>Upload Documents</Title>
        </Body>
        <Right />
      </Header>
      <StatusBar backgroundColor={Constants.PrimaryColor} hidden={false} />

      <Spinner
        visible={spinner}
        textContent={'Loading...'}
        textStyle={{ color: '#FFFFFF' }}
      />

      <View style={styles.container}>

        <View style={{
          width: '100%',
          height: '85%',
          backgroundColor: '#FFFFFF',
          justifyContent: 'center',
          // alignItems: 'center'
        }}>

          <View style={styles.sectionContainer}>
            <Text style={{ fontSize: 20, fontFamily: Constants.Bold, color: Constants.PrimaryColor, textAlign: 'center' }}>Upload your documents</Text>
          </View>

          <View style={styles.sectionContainer_}>
            {/*Showing the data of selected Single file*/}
            {proof_of_address != null ? (
              <Text style={styles.textStyle}>
                File Name: {proof_of_address[0].name ? proof_of_address[0].name : ''}
              </Text>
            ) : null}
            <TouchableOpacity
              style={styles.buttonStyle}
              activeOpacity={0.5}
              onPress={selectPlaceFile}>
              <Text style={styles.buttonTextStyle}>Proof of Address</Text>
            </TouchableOpacity>

            {id_photo != null ? (
              <Text style={styles.textStyle}>
                File Name: {id_photo[0].name ? id_photo[0].name : ''}
              </Text>
            ) : null}

            <TouchableOpacity
              style={styles.buttonStyle}
              activeOpacity={0.5}
              onPress={selectIdFile}>
              <Text style={styles.buttonTextStyle}>ID Photo</Text>
            </TouchableOpacity>
          </View>


        </View>
        <View style={{
          width: '100%',
          height: '15%',
          backgroundColor: '#FFFFFF',
          justifyContent: 'center',
          padding: 5,
        }}>

          <View style={styles.sectionContainer}>
            {proof_of_address === null || id_photo === null ? <TouchableOpacity
              style={{ width: '100%', height: 50, justifyContent: 'center', padding: 10, alignItems: 'center' }}>
              <View style={{ backgroundColor: '#576CA8', width: '100%', height: 50, alignItems: 'center', justifyContent: 'center', borderRadius: 30, opacity: 0.5 }}>
                <Text style={{ color: 'white', fontFamily: Constants.PrimaryColor, }}>Upload</Text>
              </View>
            </TouchableOpacity> :
              <TouchableOpacity
                onPress={uploadDocuments}
                style={{ width: '100%', height: 50, justifyContent: 'center', padding: 10, alignItems: 'center' }}>
                <View style={{ backgroundColor: '#576CA8', width: '100%', height: 50, alignItems: 'center', justifyContent: 'center', borderRadius: 30 }}>
                  <Text style={{ color: 'white', fontFamily: Constants.PrimaryColor, }}>Upload</Text>
                </View>
              </TouchableOpacity>}

          </View>
        </View>
      </View>
    </Container>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    // backgroundColor: Constants.PrimaryColor
  },
  scrollView: {
    backgroundColor: '#EEEEEE',
  },
  logo: {
    backgroundColor: '#EEEEEE',
    alignItems: 'center',
  },
  logoSize: {
    height: 100,
    width: 100,
  },
  sectionContainer: {
    marginLeft: 20,
    marginRight: 20,
    padding: 5,
    borderRadius: 2,
  },
  sectionContainer_: {
    marginLeft: 25,
    marginRight: 25,
    marginTop: 5,
    padding: 8,
    borderRadius: 5,
    backgroundColor: '#FFFFFF',
    borderWidth: 0.5,
    borderColor: '#BABABA'
  },
  sectionContainerPicker: {
    marginLeft: 25,
    marginRight: 25,
    borderColor: Constants.PrimaryColor,
    borderWidth: 1,
    borderRadius: 5,
    fontFamily: Constants.Regular
  },
  input: {
    width: '49%',
  },
  highlight: {
    fontWeight: '700',
  },
  textLebal: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
    color: 'black',
  },
  row1: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginLeft: 20,
    marginRight: 20,
    padding: 5,
    borderRadius: 2,
  },
  screen: {
    margin: 5,
    paddingHorizontal: 15,
    padding: 5,
  },
  _input: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
    borderColor: 'lightblue',
    marginVertical: 30,
    borderWidth: 1,
    borderRadius: 5,
  },
  text: {
    fontWeight: '700',
    alignSelf: 'center'
  },
  icon: {
    color: "#FFFFFF",
    fontSize: 25,
    // position: 'absolute',
    // left: 10
  },
  buttonStyle: {
    backgroundColor: 'transparent',
    borderWidth: 0.5,
    color: '#FFFFFF',
    borderColor: Constants.PrimaryColor,
    height: 40,
    alignItems: 'center',
    borderRadius: 30,
    marginLeft: 35,
    marginRight: 35,
    marginTop: 15,
  },
  buttonTextStyle: {
    color: Constants.PrimaryColor,
    paddingVertical: 10,
    fontSize: 16,
    fontFamily: Constants.Regular
  },
  textStyle: {
    backgroundColor: '#fff',
    fontSize: 15,
    marginTop: 16,
    marginLeft: 35,
    marginRight: 35,
    textAlign: 'center',
  },
});

export default UploadDocuments;


































// import React, { useEffect, useState } from 'react';
// import { StatusBar, BackHandler, StyleSheet, View, TouchableOpacity, Image, PixelRatio } from 'react-native';
// import { Container, Header, Left, Body, Right, Title, Button, Text } from 'native-base';
// import Feather from 'react-native-vector-icons/Feather';
// import { useNavigation } from '@react-navigation/native';
// import Spinner from 'react-native-loading-spinner-overlay';
// import { launchImageLibrary } from 'react-native-image-picker';
// import Constants from '../config/Constants';

// const SERVER_URL = 'http://localhost:3000';

// const createFormData = (photo, body = {}) => {
//   const data = new FormData();

//   data.append('photo', {
//     name: photo.fileName,
//     type: photo.type,
//     uri: Platform.OS === 'ios' ? photo.uri.replace('file://', '') : photo.uri,
//   });

//   Object.keys(body).forEach((key) => {
//     data.append(key, body[key]);
//   });
//   return data;
// };

// const UploadDocuments = () => {
//   const navigation = useNavigation();
//   const [spinner, setSpinner] = useState(false);
//   const [ImageFeatured, setImageFeatured] = useState(null);
//   const [ImageTwo, setImageTwo] = useState(null);
//   const [photo, setPhoto] = useState(null);

//   // useEffect(()=>{
//   //   // if(navigation.isFocused()){
//   //     BackHandler.removeEventListener("hardwareBackPress", ()=>{});
//   //   // }
//   // },[]);

//   const handleChoosePhoto = () => {
//     const options = {
//       quality: 1.0,
//       maxWidth: 500,
//       maxHeight: 500,
//       storageOptions: {
//         skipBackup: true,
//       },
//     };
//     // { noData: true }
//     launchImageLibrary(options, (response) => {
//       console.log(response);
//       if (response) {
//         setPhoto(response);
//       }
//     });
//   };

//   // const handleUploadPhoto = () => {
//   //   fetch(`${SERVER_URL}/api/upload`, {
//   //     method: 'POST',
//   //     body: createFormData(photo, { userId: '123' }),
//   //   })
//   //     .then((response) => response.json())
//   //     .then((response) => {
//   //       console.log('response', response);
//   //     })
//   //     .catch((error) => {
//   //       console.log('error', error);
//   //     });
//   // };

//   return (
//     <Container>
//       <Header backgroundColor={Constants.PrimaryColor} style={{ backgroundColor: Constants.PrimaryColor }}>
//         <Left>
//           <Button transparent>
//             <Feather name='chevron-left' style={styles.icon} onPress={() => { navigation.goBack() }} />
//           </Button>
//         </Left>
//         <Body>
//           <Title style={{ fontFamily: Constants.Bold }}>Upload Documents</Title>
//         </Body>
//         <Right />
//       </Header>
//       <StatusBar backgroundColor={Constants.PrimaryColor} hidden={false} />

//       <Spinner
//         visible={spinner}
//         textContent={'Loading...'}
//         textStyle={{ color: '#FFFFFF' }}
//       />

//       <View style={styles.container}>

//         <View style={{
//           width: '100%',
//           height: '85%',
//           backgroundColor: '#FFFFFF',
//           justifyContent: 'center',
//           // alignItems: 'center'
//         }}>

//           <View style={styles.sectionContainer}>
//             <Text style={{ fontSize: 20, fontFamily: Constants.Bold, color: Constants.PrimaryColor, textAlign: 'center' }}>Upload your documents</Text>
//           </View>

//           <View style={styles.sectionContainer_}>
//             <Text style={{ fontFamily: Constants.Bold, color: Constants.PrimaryColor }}>Proof of ID</Text>
//             <TouchableOpacity onPress={handleChoosePhoto}>
//               <View style={{
//                 borderRadius: 5,
//                 width: 155,
//                 height: 120,
//                 margin: 5,
//                 padding: 16,
//                 borderColor: '#CB8736',
//                 justifyContent: 'center',
//                 alignItems: 'center',
//                 backgroundColor: '#F5FCFF',
//               }}>
//                 {!photo ? <Text style={{ color: '#000000', }}>ID Photo</Text> :
//                   <Image style={{
//                     borderRadius: 5,
//                     width: 155,
//                     height: 120,
//                     margin: 5,
//                     padding: 16,
//                     borderColor: '#CB8736',
//                     justifyContent: 'center',
//                     alignItems: 'center',
//                     backgroundColor: '#F5FCFF',
//                   }} source={{ uri: photo.uri }} />
//                 }
//               </View>
//             </TouchableOpacity>
//           </View>

//           <View style={styles.sectionContainer_}>
//             <Text style={{ fontFamily: Constants.Bold, color: Constants.PrimaryColor }}>Proof of Address</Text>
//             <Text style={{ fontSize: 14, fontFamily: Constants.Regular, color: Constants.PrimaryColor }}>example@gmail.com</Text>
//           </View>


//         </View>
//         <View style={{
//           width: '100%',
//           height: '15%',
//           backgroundColor: '#FFFFFF',
//           justifyContent: 'center',
//           padding: 5,
//         }}>

//           <View style={styles.sectionContainer}>
//             <TouchableOpacity
//               // onPress={() => signUp(cardName, cardNumber, moMoNumber, paymet_method, expirlyMonth, expirlyYear)}
//               style={{ width: '100%', height: 50, justifyContent: 'center', padding: 10, alignItems: 'center' }}>
//               <View style={{ backgroundColor: '#576CA8', width: '100%', height: 50, alignItems: 'center', justifyContent: 'center', borderRadius: 30, opacity: 0.5 }}>
//                 <Text style={{ color: 'white', fontFamily: Constants.PrimaryColor, }}>Upload</Text>
//               </View>
//             </TouchableOpacity>
//           </View>
//         </View>
//       </View>
//     </Container>
//   );
// };

// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//     justifyContent: 'center',
//     alignItems: 'center',
//     // backgroundColor: Constants.PrimaryColor
//   },
//   scrollView: {
//     backgroundColor: '#EEEEEE',
//   },
//   logo: {
//     backgroundColor: '#EEEEEE',
//     alignItems: 'center',
//   },
//   logoSize: {
//     height: 100,
//     width: 100,
//   },
//   sectionContainer: {
//     marginLeft: 20,
//     marginRight: 20,
//     padding: 5,
//     borderRadius: 2,
//   },
//   sectionContainer_: {
//     marginLeft: 25,
//     marginRight: 25,
//     marginTop: 5,
//     padding: 8,
//     borderRadius: 5,
//     backgroundColor: '#FFFFFF',
//     borderWidth: 0.5,
//     borderColor: '#BABABA'
//   },
//   sectionContainerPicker: {
//     marginLeft: 25,
//     marginRight: 25,
//     borderColor: Constants.PrimaryColor,
//     borderWidth: 1,
//     borderRadius: 5,
//     fontFamily: Constants.Regular
//   },
//   input: {
//     width: '49%',
//   },
//   highlight: {
//     fontWeight: '700',
//   },
//   textLebal: {
//     marginTop: 8,
//     fontSize: 18,
//     fontWeight: '400',
//     color: 'black',
//   },
//   row1: {
//     flexDirection: 'row',
//     justifyContent: 'space-between',
//     marginLeft: 20,
//     marginRight: 20,
//     padding: 5,
//     borderRadius: 2,
//   },
//   screen: {
//     margin: 5,
//     paddingHorizontal: 15,
//     padding: 5,
//   },
//   _input: {
//     marginTop: 8,
//     fontSize: 18,
//     fontWeight: '400',
//     borderColor: 'lightblue',
//     marginVertical: 30,
//     borderWidth: 1,
//     borderRadius: 5,
//   },
//   text: {
//     fontWeight: '700',
//     alignSelf: 'center'
//   },
//   icon: {
//     color: "#FFFFFF",
//     fontSize: 25,
//     // position: 'absolute',
//     // left: 10
//   },
// });

// export default UploadDocuments;