import React, { useState, useEffect } from 'react';
import { PixelRatio, Image, Alert, FlatList, StatusBar } from 'react-native';
import { Container, Header, Content, List, Row, ListItem, Text, Left, Right, Icon, Button, Title, Body, View } from 'native-base';
import Feather from 'react-native-vector-icons/Feather';
import Spinner from 'react-native-loading-spinner-overlay';
import { useNavigation } from '@react-navigation/native';
import rnauth_api from '../api/rnauth_api';
import Constants from '../config/Constants';
import AsyncStorage from '@react-native-community/async-storage';
const app_logo = require('../../assets/diamond.png');
const ManagePayments = () => {
    const navigation = useNavigation();
    const [spinner, setSpinner] = useState();
    const [paymethods, setPayMethods] = useState([]);
    const [isFetching, setIsFetching] = useState(false);

    useEffect(() => {
        _loadPayMethod();
    }, []);

    const _loadPayMethod = async () => {
        setSpinner(true);
        try {
            let id = await AsyncStorage.removeItem('userid');
            // const response = await rnauth_api.get('/view_pay_methods.php');
            const response = await rnauth_api.post('/view_pay_methods.php',{"id": id == null? 1:id});
            if (response.data !== null) {
                // console.log("payment method data ", response.data);
                setPayMethods(response.data);
                setSpinner(false);
                setIsFetching(false);
            }

        } catch (err) {
            Alert.alert("Sorry!", err);
            setSpinner(false);
        }
    };

    const deletePayMethod = (payMethodId) => {

        Alert.alert(
            "Warning!.",
            "Are you sure you want to delete?",
            [
                {
                    text: "Cancel",
                    onPress: () => console.log("Cancel Pressed"),
                    style: "cancel"
                },
                {
                    text: "Delete", onPress: () => {
                        deletePay(payMethodId);
                    }
                }
            ],
            { cancelable: false }
        )
    };

    const deletePay = async (pay_id) => {
        setSpinner(true);
        try {
            const response = await rnauth_api.post('/delete_pay_method.php', {
                "pay_method_id": pay_id
            });
            if (response.data.message === "Pay method deleted successfully") {
                setSpinner(false);
                _loadPayMethod();
            } else {
                setSpinner(false);
            }

        } catch (err) {
            Alert.alert("Sorry!", err);
            setSpinner(false);
        }
    };

    const onRefresh = () => {
        // setIsFetching(true, () => _loadPayMethod());
        setIsFetching(true);
        _loadPayMethod();
    };


    return (
        <Container>
            <Header backgroundColor={Constants.PrimaryColor} style={{backgroundColor: Constants.PrimaryColor}}>
                <Left>
                    <Button transparent>
                        <Feather name='chevron-left' style={styles.icon} onPress={() => { navigation.goBack()}}/>
                    </Button>
                </Left>
                <Body>
                    <Title style={styles.title}>Payment Methods</Title>
                </Body>
                <Right />
            </Header>
            <StatusBar backgroundColor={Constants.PrimaryColor} hidden={false} />
            <Content>
                <Spinner
                    visible={spinner}
                    textContent={'Loading...'}
                    textStyle={{ color: '#FFFFFF' }}
                />
                <List>
                    <ListItem last style={{ backgroundColor: "#cde1f9", }}>
                        <Left>
                            <View style={styles.addButton}>
                                <Feather name='plus' style={styles.iconAdd} onPress={() => navigation.navigate('_addPayMethod')} />
                            </View>
                            <Text style={{ padding: 5, margin: 5, fontFamily: Constants.Bold, alignItems: 'center' }} onPress={() => navigation.navigate('_addPayMethod')}>Add Payment Method</Text>
                        </Left>
                    </ListItem>
                    <FlatList
                        data={paymethods}
                        // extraData={paymethods}
                        keyExtractor={item => item.user_pay_method_id}
                        renderItem={({ item }) => {
                            return (
                                <>
                                    {item.method !== 'Mobile Money' ? (
                                        <ListItem avatar style={styles.listItem}>
                                            <Row>
                                                <Left>
                                                    <Image source={app_logo} style={styles.imgStyle} />
                                                </Left>
                                                <Body>
                                                    <Text style={styles.highlight}>Ending in {"..." + item.card_number.slice(-4)}</Text>
                                                    <View style={styles.bodyStyle}>
                                                        <Text note style={styles.highlight_}>Expires {item.expirly_date}</Text>
                                                    </View>
                                                </Body>
                                                <Right>
                                                    <Feather name='trash' style={styles.iconDelete} onPress={() => deletePayMethod(item.user_pay_method_id)} />
                                                </Right>
                                            </Row>
                                        </ListItem>
                                    ) : (
                                        <ListItem avatar style={styles.listItem}>
                                            <Row>
                                                <Left>
                                                    <Image source={app_logo} style={styles.imgStyle} />
                                                </Left>
                                                <Body>
                                                    <Text style={styles.highlight}>Ending in {"..." + item.phone_number.slice(-4)}</Text>
                                                    <View style={styles.bodyStyle}>
                                                        <Text note style={styles.highlight_}>{item.method}</Text>
                                                    </View>
                                                </Body>
                                                <Right>
                                                    <Feather name='trash' style={styles.iconDelete} onPress={() => deletePayMethod(item.user_pay_method_id)} />
                                                </Right>
                                            </Row>
                                        </ListItem>
                                    )}
                                </>
                            )
                        }}
                        onRefresh={() => onRefresh()}
                        refreshing={isFetching}
                    />

                </List>
            </Content>
        </Container>
    );
}

const styles = {
    container: {
        backgroundColor: '#EEEEEE'
    },
    listItem: {
        marginTop: 10,
        borderWidth: 0.8,
        borderRadius: 8,
        marginLeft: 5,
        marginRight: 5,
        marginBottom: 10,
        paddingLeft: 10,
        flexDirection: 'column',
        backgroundColor: '#FFFFFF',
    },
    highlight: {
        // fontWeight: 'bold',
        fontFamily: Constants.Bold
    },
    highlight_: {
        // fontWeight: '700',
        fontFamily: Constants.Regular
    },
    highlight_m: {
        // fontWeight: '700',
        fontFamily: Constants.Bold
    },
    bodyStyle: {
        // flexDirection: 'row',
        justifyContent: 'center'
    },
    icon: {
        color: "#FFFFFF",
        fontSize: 25
    },
    iconAdd: {
        color: Constants.PrimaryColor,
        fontSize: 30
    },
    iconDelete: {
        color: "tomato",
        fontSize: 25
    },
    title: {
        color: 'white',
        fontFamily: Constants.Bold,
        fontSize: 15
    },
    addButton: {
        margin: 5,
        padding: 5,
        backgroundColor: '#EEEEEE',
        borderRadius: 50,
    },
    imgStyle: {
        resizeMode: 'cover',
        width: 40,
        height: 40,
        borderWidth: 1 / PixelRatio.get(),
        borderColor: '#eee',
        opacity: 0.8,
        borderRadius: 25
    },
};
export default ManagePayments;