import React, {useState, useRef, useEffect} from 'react';
import {
  PixelRatio,
  StatusBar,
  View,
  TouchableOpacity,
  KeyboardAvoidingView,
  BackHandler,
  Alert,
  SafeAreaView
} from 'react-native';
import {Container, Text} from 'native-base';
import Feather from 'react-native-vector-icons/Feather';
import Constants from '../config/Constants';
import Spinner from 'react-native-loading-spinner-overlay';
import PhoneInput from 'react-native-phone-number-input';
import {useNavigationState} from '@react-navigation/native';
import auth from '@react-native-firebase/auth';

const UserLogin = ({navigation}) => {
  const [spinner, setSpinner] = useState(false);
  const [formattedValue, setFormattedValue] = useState('');
  const [valid, setValid] = useState(false);
  const [disabled, setDisabled] = useState(false);
  const phoneInput = useRef(null);
  const [mobileNumber, setMobileNumber] = useState('');
  const [countryCode, setCountryCode] = useState('UG');

  const routes = useNavigationState((state) => state.routes);
  const currentRoute = routes[routes.length - 1].name;
  // console.log('currentRoute: ', currentRoute);

  const backAction = () => {
    if(currentRoute === '_login' && navigation.isFocused()){
    Alert.alert("Hold on!", "Are you sure you want to go back?", [
      {
        text: "Cancel",
        onPress: () => null,
        style: "cancel"
      },
      { text: "YES", onPress: () => BackHandler.exitApp() }
    ]);
    return true;
  }else{
    BackHandler.removeEventListener("hardwareBackPress",()=>{});
  }
  };

  useEffect(() => {
    auth().onAuthStateChanged((user) => {
        console.log('User First: ', user);
        if (user) {
            // if user data exist
            //clear previous user session
            logOutFirebase();
        }
    });

      if(currentRoute === '_login' && navigation.isFocused()){
      BackHandler.addEventListener('hardwareBackPress', backAction);
      }else{
        BackHandler.removeEventListener("hardwareBackPress",()=>{});
      }
  }, [navigation]);

    const logOutFirebase = () => {
      auth().signOut();
    }

  const signIn = async (PhoneNumber) => {
      if (PhoneNumber === '') {
        Alert.alert('Phone Number is required');
      } else {
          setSpinner(true);
          try {
            const checkValid = phoneInput.current?.isValidNumber(PhoneNumber);
            // setShowMessage(true);
            setValid(checkValid ? checkValid : false);
            // setCountryCode(phoneInput.current?.getCountryCode() || '');
            setCountryCode(phoneInput.current?.getCountryCode());
            let getNumberAfterPossiblyEliminatingZero =
              phoneInput.current?.getNumberAfterPossiblyEliminatingZero();
            console.log(
              'Phone ',
              getNumberAfterPossiblyEliminatingZero.formattedNumber,
            );
            if (checkValid) {
              setSpinner(false);
              navigation.navigate('_otp_login', {
                Phone: getNumberAfterPossiblyEliminatingZero.formattedNumber,
                Country: countryCode,
                MobileNo: mobileNumber
              });
            } else {
              setSpinner(false);
              Alert.alert('Sorry! Ivalide phone number');
            }
          } catch (error) {
            setSpinner(false);
            // alert(error.message);
            Alert.alert('Sorry! Something went wrong');
          }
    }
  };

  const goToSignup = () => {
    navigation.navigate('_signup');
  };

  return (
    <SafeAreaView
    style={{backgroundColor: '#FFF', flex: 1}}
    forceInset={{top: 'never'}}>
    <Container>
      <StatusBar backgroundColor={Constants.PrimaryColor} hidden={false} />
      {/* <StatusBar backgroundColor={'#FFFFFF'} hidden={false} /> */}

      <Spinner
        visible={spinner}
        textContent={'Loading...'}
        textStyle={{ color: '#FFFFFF' }}
      />

      <KeyboardAvoidingView
        style={{ flex: 1 }}
        behavior={Platform.OS == 'ios' ? 'padding' : 'height'}
        keyboardVerticalOffset={Platform.OS === 'ios' ? 40 : 0}>

        <View style={styles.container}>

          <View style={{
            width: '100%',
            height: '10%',
            alignItems: 'center',
            justifyContent: 'center',
            backgroundColor: '#FFFFFF',
            flexDirection: 'row',
            borderWidth: 1.5,
            borderColor: '#EEEEEE',
          }}>
            <Feather name='chevron-left' style={styles.icon_} onPress={() => { BackHandler.exitApp() }} />
            <Text style={{ fontSize: 25, fontFamily: Constants.Bold, color: '#576CA8' }}>Welcome back!</Text>
          </View>
          <View style={{
            width: '100%',
            height: '75%',
            backgroundColor: '#FFFFFF',
            justifyContent: 'center',
            // alignItems: 'center'
          }}>

            <View style={styles.sectionContainer}>
              <PhoneInput
                ref={phoneInput}
                defaultValue={mobileNumber}
                defaultCode="UG"
                layout="first"
                onChangeText={setMobileNumber}
                onChangeFormattedText={(text) => {
                  setFormattedValue(text);
                  // setCountryCode(phoneInput.current?.getCountryCode() || '');
                  setCountryCode(phoneInput.current?.getCountryCode());
                  console.log('Country: ', countryCode);
                }}
                countryPickerProps={{withAlphaFilter: true}}
                disabled={disabled}
                withDarkTheme
                withShadow
                // autoFocus
              />
            </View>
          </View>

          <View
            style={{
              width: '100%',
              height: '15%',
              backgroundColor: '#FFFFFF',
              justifyContent: 'center',
              padding: 5,
              // marginBottom: 15
            }}>
            {mobileNumber === '' ? (
              <View style={styles.sectionContainer}>
                <TouchableOpacity
                  style={{
                    width: '100%',
                    height: 50,
                    justifyContent: 'center',
                    padding: 10,
                    alignItems: 'center',
                  }}>
                  <View
                    style={{
                      backgroundColor: '#576CA8',
                      width: '100%',
                      height: 50,
                      alignItems: 'center',
                      justifyContent: 'center',
                      borderRadius: 30,
                      opacity: 0.5,
                    }}>
                    <Text
                      style={{
                        color: 'white',
                        fontFamily: Constants.Bold,
                      }}>
                      SIGN IN
                    </Text>
                  </View>
                </TouchableOpacity>
              </View>
            ) : (
              <View style={styles.sectionContainer}>
                <TouchableOpacity
                  onPress={() => signIn(mobileNumber)}
                  // onPress={() => navigation.navigate('_dashboard')}
                  style={{
                    width: '100%',
                    height: 50,
                    justifyContent: 'center',
                    padding: 10,
                    alignItems: 'center',
                  }}>
                  <View
                    style={{
                      backgroundColor: '#576CA8',
                      width: '100%',
                      height: 50,
                      alignItems: 'center',
                      justifyContent: 'center',
                      borderRadius: 30,
                    }}>
                    <Text
                      style={{
                        color: 'white',
                        fontFamily: Constants.Bold,
                      }}>
                      SIGN IN
                    </Text>
                  </View>
                </TouchableOpacity>
              </View>
            )}

            <View style={{alignItems: 'center', marginBottom: 5}}>
              <Text style={styles.textLebal}>
              Don't have an account?{' '}
                <Text style={styles.highlight} onPress={goToSignup}>
                  Sign Up
                </Text>
              </Text>
            </View>
          </View>
        </View>
      </KeyboardAvoidingView>
    </Container>
    </SafeAreaView>
  );
};

const styles = {
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    // backgroundColor: Constants.PrimaryColor
  },
  scrollView: {
    backgroundColor: '#EEEEEE',
  },
  icon: {
    color: '#FFFFFF',
    alignItems: 'center',
    fontSize: 30,
  },
  icon_: {
    color: '#576CA8',
    fontSize: 30,
    position: 'absolute',
    left: 0,
  },
  sectionContainer: {
    marginLeft: 20,
    marginRight: 20,
    // alignItems: 'center',
    padding: 5,
    borderRadius: 2,
  },
  sectionContainer_: {
    marginLeft: 15,
    marginRight: 28,
    // alignItems: 'center',
    // padding: 5,
    borderRadius: 2,
  },
  sectionContainerPicker: {
    margin: 5,
    paddingHorizontal: 15,
    padding: 5,
    borderWidth: 0.5,
    borderColor: '#BABABA',
    borderRadius: 5,
    backgroundColor: '#FFFFFF',
  },
  imgStyle: {
    resizeMode: 'contain',
    width: 35,
    height: 25,
    borderWidth: 1 / PixelRatio.get(),
    borderColor: '#eee',
    opacity: 0.8,
  },
  button: {
    marginTop: 20,
    height: 50,
    width: 300,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#7CDB8A',
    shadowColor: 'rgba(0,0,0,0.4)',
    shadowOffset: {
      width: 1,
      height: 5,
    },
    shadowOpacity: 0.34,
    shadowRadius: 6.27,
    elevation: 10,
  },
  buttonText: {
    color: 'white',
    fontSize: 14,
  },
  message: {
    borderWidth: 1,
    borderRadius: 5,
    padding: 20,
    marginBottom: 20,
    justifyContent: 'center',
    alignItems: 'flex-start',
  },
  screen: {
    marginLeft: 20,
    marginRight: 20,
  },
  highlight: {
    fontSize: 18,
    fontFamily: Constants.Bold,
    color: Constants.PrimaryColor,
  },
  textLebal: {
    marginBottom: 8,
    fontSize: 16,
    fontFamily: Constants.Regular,
    color: 'black',
  },
};

export default UserLogin;

















// import React, { useState, useRef, useEffect, useCallback } from 'react';
// import {
//   PixelRatio,
//   StatusBar,
//   View,
//   TouchableOpacity,
//   KeyboardAvoidingView,
//   Alert,
//   BackHandler,
//   SafeAreaView
// } from 'react-native';
// import { Container, Text } from 'native-base';
// import Feather from 'react-native-vector-icons/Feather';
// import { useNavigationState } from '@react-navigation/native';
// // import RNRestart from 'react-native-restart';
// import Constants from '../config/Constants';
// import rnauth_api from '../api/rnauth_api';
// import AsyncStorage from '@react-native-community/async-storage';
// import auth from '@react-native-firebase/auth';
// import Spinner from 'react-native-loading-spinner-overlay';
// import { TextInput, HelperText } from 'react-native-paper';
// import PhoneInput from "react-native-phone-number-input";
// import RNOtpVerify from 'react-native-otp-verify';

// const UserLogin = ({ navigation }) => {
//   // const navigation = useNavigation();
//   const [spinner, setSpinner] = useState(false);
//   const [formattedValue, setFormattedValue] = useState('');
//   const [valid, setValid] = useState(false);
//   const [disabled, setDisabled] = useState(false);
//   const [showMessage, setShowMessage] = useState(false);
//   const phoneInput = useRef(null);
//   const [mobileNumber, setMobileNumber] = useState("");
//   const [countryCode, setCountryCode] = useState('');
//   const [confirm, setConfirm] = useState(null);
//   const [userId, setUserId] = useState(null);
//   const [code, setCode] = useState("");

//   // Set an initializing state whilst Firebase connects
//   const [initializing, setInitializing] = useState(true);
//   const [user, setUser] = useState(null);

//   const routes = useNavigationState(state => state.routes);
//   const currentRoute = routes[routes.length - 1].name;
//   // console.log('currentRoute: ', currentRoute);
  
//   const backAction = () => {
//     if(currentRoute === '_login' && navigation.isFocused()){
//     Alert.alert("Hold on!", "Are you sure you want to go back?", [
//       {
//         text: "Cancel",
//         onPress: () => null,
//         style: "cancel"
//       },
//       { text: "YES", onPress: () => BackHandler.exitApp() }
//     ]);
//     return true;
//   }else{
//     BackHandler.removeEventListener("hardwareBackPress",()=>{});
//   }
//   };

//     // // Handle user state changes
//     // const onAuthStateChanged = (user) =>{
//     //   setUser(user);
//     //   if (initializing) setInitializing(false);
//     // }

//     useEffect(() => {

//       if(currentRoute === '_login' && navigation.isFocused()){
//       BackHandler.addEventListener('hardwareBackPress', backAction);
//       }else{
//         BackHandler.removeEventListener("hardwareBackPress",()=>{});
//       }
//       // const subscriber = auth().onAuthStateChanged(onAuthStateChanged);
//       // console.log('User First: ', user);

//       // return subscriber; // unsubscribe on unmount

//       auth().onAuthStateChanged((user) => {
//         console.log('User First: ', user);
//         if (user) {
//             // if user data exist
//             //clear previous user session
//             logOutFirebase();
//         }
//     });
      
//   }, []);

//     const logOutFirebase = () => {
//       auth().signOut();
//     }

//    // if (initializing) return null;

//   // const confirmVerificationCode = async (code) => {
//   //   try {
//   //     // Firebase controller begins here
//   //     if(user === null){
//   //             await confirm
//   //               .confirm(code)
//   //               .then(user => {
//   //                 setUserId(user.uid);
//   //                 if (userId === null) {
//   //                   Alert.alert("Login process failed. Please try again");
//   //                 } else {
//   //                   setConfirm(null);
//   //                   setCode("");
//   //                   login();
//   //                 }
//   //               });
//   //     }else{
//   //         if (user !== null) {
//   //             setConfirm(null);
//   //             setCode("");
//   //             login();
//   //         } else {
//   //             // setSpinner(false);
//   //             Alert.alert("Sorry!, Login process failed. Please try again");
//   //         }
//   //       }

//   //   } catch (error) {
//   //     Alert.alert('Invalid code', error.message);
//   //     // Alert.alert('Invalid code');
//   //     setSpinner(false);
//   //   }
//   // }


//   const getMyHash = () => {
//     RNOtpVerify.getHash().then(console.log).catch(console.log);
//   };

//   const getMyOtp = () => {
//     RNOtpVerify.getOtp()
//       .then(p => RNOtpVerify.addListener(otpHandler))
//       .catch(p => Alert.alert(p));
//     return () => RNOtpVerify.removeListener();
//   };

//   // automatically read the otp from the message
//   const otpHandler = (message) => {
//     Alert.alert('message: ', message.toString());
//     const otp_code = /(\d{6})/g.exec(message)[1];
//     setCode(otp_code);
//     console.log('OTP: ', code);
//     // if(otp !== null){
//     //   console.log('OTP: ', otp);
//     //   confirmVerificationCode(otp);
//     // }
//     RNOtpVerify.removeListener();
//   };

//   const confirmVerificationCode = () => {
//     setConfirm(null);
//     setCode("");
//     login();
//   }


//   const signIn = async (PhoneNumber) => {
//     if (PhoneNumber === '') {
//       Alert.alert('Phone Number is required');
//     } else {
//       setSpinner(true);
//       try {
//         const checkValid = phoneInput.current?.isValidNumber(PhoneNumber);
//         // setShowMessage(true);
//         setValid(checkValid ? checkValid : false);
//         setCountryCode(phoneInput.current?.getCountryCode() || '');
//         let getNumberAfterPossiblyEliminatingZero = phoneInput.current?.getNumberAfterPossiblyEliminatingZero();
//         // console.log("Phone ",getNumberAfterPossiblyEliminatingZero);
//         if (checkValid) {
//           const confirmation = await auth().signInWithPhoneNumber(getNumberAfterPossiblyEliminatingZero.formattedNumber);
//           setConfirm(confirmation);
//           // setSpinner(false);

//           if (confirmation) {

//             getMyHash();
//             getMyOtp();

//             setTimeout(() => {
//               setSpinner(false);
//               auth().onAuthStateChanged((user) => {
//                 console.log('User Second: ', user);
//                 // setUser(user);
//                 if (user) {
//                     // if user data exist
//                     console.log('User Exists: ', user);
//                     confirmVerificationCode();
//                 }
//             });

//             }, 5000);
//           }
//         } else {
//           setSpinner(false);
//           Alert.alert('Sorry! Ivalide phone number');
//         }

//       } catch (error) {
//         setSpinner(false);
//         Alert.alert('Sorry! Something went wrong', error.message);
//         // Alert.alert('Sorry! Something went wrong');
//       }
//     }
//   };

//   // login a user after verifying phone number at both firbase and the backend
//   const login = async () => {
//     setSpinner(true);
//     try {

//       setCountryCode(phoneInput.current?.getCountryCode() || '');
//       let getNumberAfterPossiblyEliminatingZero = phoneInput.current?.getNumberAfterPossiblyEliminatingZero();

//       const response = await rnauth_api.post('/login.php', { "phone": getNumberAfterPossiblyEliminatingZero.formattedNumber });

//       if (response.data.message === "You have successfully logged in.") {
//         Alert.alert(response.data.message);
//         setSpinner(false);

//         AsyncStorage.setItem('name', response.data.first_name + " " + response.data.last_name);
//         AsyncStorage.setItem('phone', response.data.phone);
//         AsyncStorage.setItem('status', response.data.user_status);
//         AsyncStorage.setItem('userid', response.data.user_id);
//         AsyncStorage.setItem('country', response.data.country);
//         AsyncStorage.setItem('email', response.data.email);
//         AsyncStorage.setItem('firebase_ref_id', response.data.firebase_ref_id);
//         navigation.navigate('_dashboard');

//       } else {
//         setSpinner(false);
//         Alert.alert(response.data.message);
//       }
//     } catch (err) {
//       setSpinner(false);
//       console.log(err);
//       // Alert.alert('Sorry! Something went wrong_', err);
//       Alert.alert('Sorry! Something went wrong_');
//     }
//   };

//   // console.log("navigation props", navigation);

//   const goToSignup = () => {
//     // BackHandler.removeEventListener("hardwareBackPress", backAction);
//     navigation.navigate('_signup');
//   };

//   return (
//     <SafeAreaView
//     style={{backgroundColor: '#FFF', flex: 1}}
//     forceInset={{top: 'never'}}>
//     <Container>
//       <StatusBar backgroundColor={Constants.PrimaryColor} hidden={false} />
//       {/* <StatusBar backgroundColor={'#FFFFFF'} hidden={false} /> */}

//       <Spinner
//         visible={spinner}
//         textContent={'Loading...'}
//         textStyle={{ color: '#FFFFFF' }}
//       />

//       <KeyboardAvoidingView
//         style={{ flex: 1 }}
//         behavior={Platform.OS == 'ios' ? 'padding' : 'height'}
//         keyboardVerticalOffset={Platform.OS === 'ios' ? 40 : 0}>

//         {/* {showMessage && (
//           <View style={styles.message}>
//             <Text>Value : {value}</Text>
//             <Text>Code : {countryCode}</Text>
//             <Text>Formatted Value : {formattedValue}</Text>
//             <Text>Valid : {valid ? "true" : "false"}</Text>
//           </View>
//         )} */}

//         <View style={styles.container}>

//           <View style={{
//             width: '100%',
//             height: '10%',
//             alignItems: 'center',
//             justifyContent: 'center',
//             backgroundColor: '#FFFFFF',
//             flexDirection: 'row',
//             borderWidth: 1.5,
//             borderColor: '#EEEEEE',
//           }}>
//             <Feather name='chevron-left' style={styles.icon_} onPress={() => { BackHandler.exitApp() }} />
//             <Text style={{ fontSize: 25, fontFamily: Constants.Bold, color: '#576CA8' }}>Welcome back</Text>
//           </View>
//           <View style={{
//             width: '100%',
//             height: '75%',
//             backgroundColor: '#FFFFFF',
//             justifyContent: 'center',
//             // alignItems: 'center'
//           }}>

//             <View style={styles.sectionContainer}>
//               <PhoneInput
//                 ref={phoneInput}
//                 defaultValue={mobileNumber}
//                 defaultCode="UG"
//                 layout="first"
//                 onChangeText={setMobileNumber}
//                 onChangeFormattedText={(text) => {
//                   setFormattedValue(text);
//                   setCountryCode(phoneInput.current?.getCountryCode() || '');
//                 }}
//                 countryPickerProps={{ withAlphaFilter: true }}
//                 disabled={disabled}
//                 withDarkTheme
//                 withShadow
//                 autoFocus
//               />
//             </View>
//             {confirm !== null && (
//               <>
//                 <View style={styles.sectionContainer}>
//                   <TextInput
//                     // autoFocus
//                     value={code}
//                     defaultValue={code}
//                     onChangeText={setCode}
//                     keyboardType="numeric"
//                     maxLength={6}
//                     mode="outlined"
//                     // label="OTP"
//                     outlineColor={Constants.PrimaryColor}
//                     placeholder="Type Code"
//                   />
//                 </View>
//                 <View style={styles.sectionContainer}>

//                   {code === "" ?
//                     <TouchableOpacity
//                       style={{ width: '100%', height: 50, justifyContent: 'center', padding: 15 }}>
//                       <View style={{ backgroundColor: '#576CA8', width: '100%', height: 50, alignItems: 'center', justifyContent: 'center', borderRadius: 30, opacity: 0.5 }}>
//                         <Text style={{ color: 'white', fontFamily: "texgyreadventor-bold" }}>CONFIRM CODE</Text>
//                       </View>
//                     </TouchableOpacity>
//                     :
//                     <TouchableOpacity
//                       // onPress={() => confirmVerificationCode(code)}
//                       onPress={() => confirmVerificationCode()}
//                       style={{ width: '100%', height: 50, justifyContent: 'center', padding: 15 }}>
//                       <View style={{ backgroundColor: '#576CA8', width: '100%', height: 50, alignItems: 'center', justifyContent: 'center', borderRadius: 30 }}>
//                         <Text style={{ color: 'white', fontFamily: "texgyreadventor-bold" }}>CONFIRM CODE</Text>
//                       </View>
//                     </TouchableOpacity>}
//                 </View>
//               </>
//             )}

//           </View>
//           <View style={{
//             width: '100%',
//             height: '15%',
//             backgroundColor: '#FFFFFF',
//             justifyContent: 'center',
//             padding: 5,
//             // marginBottom: 15
//           }}>
//             {confirm === null && (<View style={styles.sectionContainer}>
//               {mobileNumber === "" ?
//                 <TouchableOpacity
//                   style={{ width: '100%', height: 50, justifyContent: 'center', padding: 10, alignItems: 'center' }}>
//                   <View style={{ backgroundColor: '#576CA8', width: '100%', height: 50, alignItems: 'center', justifyContent: 'center', borderRadius: 30, opacity: 0.5 }}>
//                     <Text style={{ color: 'white', fontFamily: "texgyreadventor-bold" }}>SIGN IN</Text>
//                   </View>
//                 </TouchableOpacity>
//                 :
//                 <TouchableOpacity
//                   onPress={() => signIn(mobileNumber)}
//                   // onPress={() => navigation.navigate('_dashboard')}
//                   style={{ width: '100%', height: 50, justifyContent: 'center', padding: 10, alignItems: 'center' }}>
//                   <View style={{ backgroundColor: '#576CA8', width: '100%', height: 50, alignItems: 'center', justifyContent: 'center', borderRadius: 30 }}>
//                     <Text style={{ color: 'white', fontFamily: "texgyreadventor-bold" }}>SIGN IN</Text>
//                   </View>
//                 </TouchableOpacity>}
//             </View>)}
//             <View style={{ alignItems: 'center', marginBottom: 5 }}>
//               <Text style={styles.textLebal}>
//                 Don't have an account?{' '}
//                 <Text
//                   style={styles.highlight}
//                   onPress={goToSignup}>
//                   Sign Up
//                 </Text>
//               </Text>
//             </View>
//           </View>
//         </View>

//       </KeyboardAvoidingView>
//     </Container>
//     </SafeAreaView>
//   );
// };

// const styles = {
//   container: {
//     flex: 1,
//     justifyContent: 'center',
//     alignItems: 'center',
//     // backgroundColor: Constants.PrimaryColor
//   },
//   scrollView: {
//     backgroundColor: '#EEEEEE',
//   },
//   icon: {
//     color: "#FFFFFF",
//     alignItems: 'center',
//     fontSize: 30
//   },
//   icon_: {
//     color: "#576CA8",
//     fontSize: 30,
//     position: 'absolute',
//     left: 0
//   },
//   sectionContainer: {
//     marginLeft: 20,
//     marginRight: 20,
//     // alignItems: 'center',
//     padding: 5,
//     borderRadius: 2,
//   },
//   sectionContainerPicker: {
//     margin: 5,
//     paddingHorizontal: 15,
//     padding: 5,
//     borderWidth: 0.5,
//     borderColor: '#BABABA',
//     borderRadius: 5,
//     backgroundColor: '#FFFFFF'
//   },
//   imgStyle: {
//     resizeMode: 'contain',
//     width: 35,
//     height: 25,
//     borderWidth: 1 / PixelRatio.get(),
//     borderColor: '#eee',
//     opacity: 0.8,
//   },
//   button: {
//     marginTop: 20,
//     height: 50,
//     width: 300,
//     justifyContent: 'center',
//     alignItems: 'center',
//     backgroundColor: '#7CDB8A',
//     shadowColor: 'rgba(0,0,0,0.4)',
//     shadowOffset: {
//       width: 1,
//       height: 5,
//     },
//     shadowOpacity: 0.34,
//     shadowRadius: 6.27,
//     elevation: 10,
//   },
//   buttonText: {
//     color: 'white',
//     fontSize: 14,
//   },
//   message: {
//     borderWidth: 1,
//     borderRadius: 5,
//     padding: 20,
//     marginBottom: 20,
//     justifyContent: 'center',
//     alignItems: 'flex-start',
//   },
//   screen: {
//     marginLeft: 20,
//     marginRight: 20,
//   },
//   _input: {
//     marginVertical: 15,
//   },
//   highlight: {
//     fontSize: 18,
//     fontFamily: Constants.Bold,
//     color: Constants.PrimaryColor
//   },
//   textLebal: {
//     marginBottom: 8,
//     fontSize: 16,
//     fontFamily: Constants.Regular,
//     color: 'black',
//   },
// };

// export default UserLogin;