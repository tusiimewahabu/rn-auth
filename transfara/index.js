/**
 * @format
 */

 import {AppRegistry, LogBox} from 'react-native';
 import App from './App';
 import {name as appName} from './app.json';
 LogBox.ignoreLogs([
   'VirtualizedLists should never be nested inside plain ScrollViews with the same orientation - use another VirtualizedList-backed container instead.',
   'new NativeEventEmitter()',
 ]);
 
 AppRegistry.registerComponent(appName, () => App);
 
