import React, {useEffect} from 'react';
import { StatusBar, Alert, BackHandler, Text, View, ScrollView } from 'react-native';
import { Container, Header, Left, Body, Right, Title, Button } from 'native-base';
import Feather from 'react-native-vector-icons/Feather';
import { useNavigation, useNavigationState } from '@react-navigation/native';
import Constants from '../config/Constants';
const SuccessPage = ({ route }) => {
  const navigation = useNavigation();
  // console.log("Props", route.params);

  const routes = useNavigationState(state => state.routes);
  const currentRoute = routes[routes.length - 1].name;
  console.log('currentRoute: ', currentRoute);

  useEffect(() => {
    if(currentRoute === '_successPage' && navigation.isFocused()){
    BackHandler.addEventListener('hardwareBackPress', handleBackButton);
    }else{
    BackHandler.removeEventListener("hardwareBackPress", handleBackButton); 
    }
  }, [navigation]);

  // HardWare Backbutton Controller
  const handleBackButton = () => {
    // Prompt the user before leaving the screen
    if(currentRoute === '_successPage' && navigation.isFocused()){
      navigation.navigate('_dashboard');
    }
  };

  return (
    <Container>
      <Header backgroundColor={Constants.PrimaryColor} style={{backgroundColor: Constants.PrimaryColor}}>
        <Left>
          <Button transparent>
            <Feather name='chevron-left' style={styles.icon} onPress={handleBackButton} />
          </Button>
        </Left>
        <Body>
          <Title style={styles.title}>Success Screen</Title>
        </Body>
        <Right />
      </Header>
      {/* {Alert.alert("Response Data", JSON.stringify(route.params.RESPONSE_DATA))} */}
      <StatusBar backgroundColor={Constants.PrimaryColor} hidden={false} />
      <View style={{padding: 5, margin: 5, borderRadius: 5, borderWidth: 1, borderColor: Constants.PrimaryColor}}>
        <ScrollView>
        <Text>
        {JSON.stringify(route.params.RESPONSE_DATA)}
        </Text>
        </ScrollView>
      </View>
    </Container>
  );
};

const styles = {
  icon: {
    color: "#FFFFFF",
    fontSize: 30
  },
  title: {
    color: 'white',
    fontFamily: Constants.Bold,
    fontSize: 18
  },
}

export default SuccessPage;