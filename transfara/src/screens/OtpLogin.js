import React, { useState, useEffect, useRef } from 'react';
import {
    StyleSheet,
    Text,
    View,
    StatusBar,
    Alert,
    KeyboardAvoidingView,
    TouchableOpacity
} from 'react-native';
import auth from '@react-native-firebase/auth';
import Spinner from 'react-native-loading-spinner-overlay';
import { TextInput } from 'react-native-paper';
import rnauth_api from '../api/rnauth_api';
import { Container } from 'native-base';
import { useNavigation } from '@react-navigation/native';
import Feather from 'react-native-vector-icons/Feather';
import AsyncStorage from '@react-native-community/async-storage';
import Constants from '../config/Constants';
import RNOtpVerify from 'react-native-otp-verify';
import PhoneInput from 'react-native-phone-number-input';

const OtpLogin = ({ route }) => {
    const navigation = useNavigation();
    const phoneInput = useRef(null);
    const [code, setCode] = useState("");
    const [spinner, setSpinner] = useState(false);
    const [userId, setUserId] = useState("");
    const [confirm, setConfirm] = useState(null);
    const [mobileNumber, setMobileNumber] = useState(route.params.MobileNo);
    const [countryCode, setCountryCode] = useState(route.params.Country);
    const [formattedValue, setFormattedValue] = useState('');
    const [disabled, setDisabled] = useState(true);

    console.log("Props", route.params);
    // MobileNo: mobileNumber

    useEffect(() => {
        senCode();
        // getMyHash();
        // getMyOtp();
    }, []);


  const getMyHash = () => {
    RNOtpVerify.getHash().then(console.log).catch(console.log);
  };

  const getMyOtp = () => {
    RNOtpVerify.getOtp()
      .then(p => RNOtpVerify.addListener(otpHandler))
      .catch(p => Alert.alert(p));
    return () => RNOtpVerify.removeListener();
  };

  // automatically read the otp from the message
  const otpHandler = (message) => {
    Alert.alert('message: ', message.toString());
    const otp_code = /(\d{6})/g.exec(message)[1];
    setCode(otp_code);
    console.log('OTP: ', code);
    if(code !== null){
        setSpinner(false);
        console.log('OTP_: ', code);
    //   confirmVerificationCode(otp);
    }
    RNOtpVerify.removeListener();
  };

    const senCode = async () => {
        setSpinner(true);
        try {
            if (route.params.Phone !== "") {
                const confirmation = await auth().signInWithPhoneNumber(route.params.Phone);
                setConfirm(confirmation);
                setSpinner(false);
                // if(confirm !== null){
                //     setSpinner(false);
                //     // getMyHash();
                //     // getMyOtp();
                // }
            }
        } catch (error) {
            setSpinner(false);
            // alert(error.message);
            // Alert.alert('Verification process failed. Please try again later!', error.message);
            Alert.alert('Verification process failed. Please try again later!');
            navigation.navigate('_login');
        }
    };

    const confirmVerificationCode = async  (code) => {
        try {
            await confirm
                .confirm(code)
                .then(user => {
                    setUserId(user.uid);
                    if (userId === null) {
                        Alert.alert("SignIn process failed. Please try again");
                    } else {
                        if (confirm !== null) {
                            login();
                        }
                    }
                });
        } catch (error) {
            // Alert.alert('Invalid code', error.message);
            Alert.alert('Invalid code');
            setSpinner(false);
        }
    }

// login a user after verifying phone number at both firbase and the backend
  const login = async () => {
    setSpinner(true);
    try {
      const response = await rnauth_api.post('/login.php', { "phone": route.params.Phone});
      if (response.data.message === "You have successfully logged in.") {
        Alert.alert(response.data.message);
        setSpinner(false);

        AsyncStorage.setItem('name', response.data.first_name + " " + response.data.last_name);
        AsyncStorage.setItem('phone', response.data.phone);
        AsyncStorage.setItem('status', response.data.user_status);
        AsyncStorage.setItem('userid', response.data.user_id);
        AsyncStorage.setItem('country', response.data.country);
        AsyncStorage.setItem('email', response.data.email);
        AsyncStorage.setItem('firebase_ref_id', response.data.firebase_ref_id);
        navigation.navigate('_dashboard');

      } else {
        setSpinner(false);
        Alert.alert(response.data.message);
      }
    } catch (err) {
      setSpinner(false);
      console.log(err);
    //   Alert.alert('Sorry! Verification process failed. Please try again later!', err);
      Alert.alert('Sorry! Verification process failed. Please try again later!');
    }
  };

    return (
        <Container>
            <StatusBar backgroundColor={Constants.PrimaryColor} hidden={false} />
            <Spinner
                visible={spinner}
                textContent={'Loading...'}
                textStyle={{ color: '#FFFFFF' }}
            />

            <KeyboardAvoidingView
                style={{ flex: 1 }}
                behavior={Platform.OS == 'ios' ? 'padding' : 'height'}
                keyboardVerticalOffset={Platform.OS === 'ios' ? 40 : 0}>

                <View style={styles.container}>
                    <View style={{
                        width: '100%',
                        height: '10%',
                        alignItems: 'center',
                        justifyContent: 'center',
                        backgroundColor: '#FFFFFF',
                        flexDirection: 'row',
                        marginBottom: 5,
                        borderWidth: 1.5,
                        borderColor: '#EEEEEE',
                    }}>
                        <Feather name='chevron-left' style={styles.icon_} onPress={() => { navigation.goBack() }} />
                        <Text style={{ fontSize: 20, fontFamily: Constants.Bold, color: Constants.PrimaryColor }}>Verifying Phone</Text>
                    </View>

                    <View style={{
                        width: '100%',
                        height: '75%',
                        backgroundColor: '#FFFFFF',
                        justifyContent: 'center',
                    }}>

                    {/* <View style={styles.sectionContainer}>
                    <PhoneInput
                        ref={phoneInput}
                        defaultValue={mobileNumber}
                        defaultCode={countryCode}
                        layout="first"
                        onChangeText={setMobileNumber}
                        onChangeFormattedText={(text) => {
                        setFormattedValue(text);
                        setCountryCode(phoneInput.current?.getCountryCode() || countryCode);
                        // setCountryCode(phoneInput.current?.getCountryCode());
                        console.log('Country_ : ', countryCode);
                        }}
                        countryPickerProps={{withAlphaFilter: true}}
                        disabled={disabled}
                        withDarkTheme
                        withShadow
                        // autoFocus
                    />
                    </View> */}

                    <View style={styles.sectionContainer}>
                    <Text style={{ fontSize: 20, fontFamily: Constants.Bold, color: Constants.PrimaryColor }}>Verifying {route.params.Phone}</Text>
                    </View>

                        <View style={styles.sectionContainer}>
                            <TextInput
                                value={code}
                                onChangeText={setCode}
                                keyboardType="numeric"
                                maxLength={6}
                                mode="outlined"
                                label="Enter Code"
                                outlineColor={Constants.PrimaryColor}
                                placeholder="Type Code"
                            />
                        </View>
                    </View>
                    <View style={{
                        width: '100%',
                        height: '15%',
                        backgroundColor: '#FFFFFF',
                        justifyContent: 'center',
                        padding: 5,
                    }}>

                        <View style={styles.sectionContainer}>
                            {code === "" ? <TouchableOpacity
                                style={{ width: '100%', height: 50, justifyContent: 'center', padding: 10, alignItems: 'center' }}>
                                <View style={{ backgroundColor: '#576CA8', width: '100%', height: 50, alignItems: 'center', justifyContent: 'center', borderRadius: 30, opacity: 0.5 }}>
                                    <Text style={{ color: 'white', fontFamily: Constants.Bold, }}>Submit</Text>
                                </View>
                            </TouchableOpacity>
                                :
                                <TouchableOpacity
                                    onPress={() => confirmVerificationCode(code)}
                                    style={{ width: '100%', height: 50, justifyContent: 'center', padding: 10, alignItems: 'center' }}>
                                    <View style={{ backgroundColor: '#576CA8', width: '100%', height: 50, alignItems: 'center', justifyContent: 'center', borderRadius: 30 }}>
                                        <Text style={{ color: 'white', fontFamily: Constants.Bold, }}>Submit</Text>
                                    </View>
                                </TouchableOpacity>
                            }
                        </View>
                    </View>
                </View>
            </KeyboardAvoidingView>
        </Container>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    icon_: {
        color: Constants.PrimaryColor,
        fontSize: 30,
        position: 'absolute',
        left: 10
    },
    sectionContainer: {
        marginLeft: 20,
        marginRight: 20,
        padding: 5,
        borderRadius: 2,
    },
});

export default OtpLogin;