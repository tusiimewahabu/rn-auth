import AsyncStorage from '@react-native-community/async-storage';
import createDataContext from './createDataContext';
import rnauth_api from '../api/rnauth_api';
import * as RootNavigation from '../RootNavigation';

const authReducer = (state, action) => {
  switch (action.type) {
    case 'add_error':
      return {...state, errorMessage: action.payload};
    case 'back_button_status':
      return {...state, backButtonStatus: action.payload};
    case 'sign_in_and_up':
      return {errorMessage: '', token: action.payload};
    case 'clear_error_message':
      return {...state, errorMessage: ''};
    case 'screen_tabs_status':
      return {...state, tabsStatus: 0};
    case 'signout':
      return {token: null, errorMessage: ''};
    case 'modal_status':
      return {...state, modalStatus: action.payload};
    default:
      return state;
  }
};

const backButtonHandller = (dispatch) => () => {
  // console.log('Back Status = ', "Action Excecuted");
  dispatch({type: 'back_button_status', payload: true});
};

const tryLocalSignin = (dispatch) => async () => {
  const token = await AsyncStorage.getItem('token');
  if (token) {
    dispatch({type: 'sign_in_and_up', payload: token});
    RootNavigation.navigate('mainFlow');
  } else {
    RootNavigation.navigate('loginFlow');
  }
};

const clearErrorMessage = (dispatch) => () => {
  dispatch({type: 'clear_error_message'});
};

const signup =
  (dispatch) =>
  async ({email, password}) => {
    try {
      const response = await rnauth_api.post('/signup', {email, password});
      // console.log(response.data);
      await AsyncStorage.setItem('token', response.data.token);
      dispatch({type: 'sign_in_and_up', payload: response.data.token});

      // navigate('mainFlow'); // prefered but failed
      RootNavigation.navigate('mainFlow');
    } catch (err) {
      // console.log(err.message); console.log(err.response.data);
      console.log(err.message);
      dispatch({
        type: 'add_error',
        payload: 'Something went wrong with signing up',
      });
    }
  };

const signin =
  (dispatch) =>
  async ({email, password}) => {
    try {
      const response = await rnauth_api.post('/signin', {email, password});
      await AsyncStorage.setItem('token', response.data.token);
      dispatch({type: 'sign_in_and_up', payload: response.data.token});
      RootNavigation.navigate('mainFlow');
    } catch (err) {
      console.log(err.message);
      dispatch({
        type: 'add_error',
        payload: 'Something went wrong with signing In!',
      });
    }
  };

const signout = (dispatch) => async () => {
  await AsyncStorage.removeItem('token');
  dispatch({type: 'signout'});
  RootNavigation.navigate('_login');
};

const checkTabStatus =
  (dispatch) =>
  ({status}) => {
    dispatch({
      type: 'screen_tabs_status',
      payload: status,
    });
  };

const openModal = (dispatch) => () => {
  dispatch({
    type: 'modal_status',
    payload: true,
  });
};

const closeModal = (dispatch) => () => {
  dispatch({
    type: 'modal_status',
    payload: false,
  });
};

export const {Provider, Context} = createDataContext(
  authReducer,
  {
    signin,
    signup,
    signout,
    clearErrorMessage,
    tryLocalSignin,
    backButtonHandller,
    checkTabStatus,
    openModal,
    closeModal,
  },
  {
    token: null,
    errorMessage: '',
    backButtonStatus: false,
    tabsStatus: 0,
    modalStatus: false,
  },
);
