import React, { useEffect, useState } from 'react';
import DeviceInfo from 'react-native-device-info';
import AsyncStorage from '@react-native-community/async-storage';
import Spinner from 'react-native-loading-spinner-overlay';
import NetInfo from "@react-native-community/netinfo";

var uniqueId = null;

const Controller = ({ navigation }) => {

    const [spinner, setSpinner] = useState(false);

    useEffect(() => {
        // Subscribe
        const unsubscribe = NetInfo.addEventListener(state =>{handleConnectivityChange()});
        uniqueId = DeviceInfo.getUniqueId();
        // checkIfIsFirstUse();
        // Unsubscribe
        unsubscribe();
    }, []);

    const handleConnectivityChange = () => {
        NetInfo.fetch().then(state => {
            // console.log("Connection type", state.type);
            // console.log("Is connected?", state.isConnected);
            if (!state.isConnected || !state.isInternetReachable) {
                navigation.navigate('_internetRetry');
            }else{
                checkIfIsFirstUse(); 
            }
        });
    };

    const checkIfIsFirstUse = async () => {
        setSpinner(true);
        let id = await AsyncStorage.getItem('device_id');
        if (id !== null) {
            setSpinner(false);
            checkIfAlreadyLoggedIn();
        } else {
            setSpinner(false);
            navigation.navigate('_intro');
        }
    };

    checkIfAlreadyLoggedIn = async () => {
        setSpinner(true);
        let phone = await AsyncStorage.getItem('phone');
        // if (phone === null) {
        if (phone !== null) {
            setSpinner(false);
            navigation.navigate('_dashboard');
        } else {
            setSpinner(false);
            navigation.navigate('_login');
        }
    };
    return (
        <>
            <Spinner
                    visible={spinner}
                    textContent={'Loading...'}
                    textStyle={{ color: '#FFFFFF' }}
                />
        </>
    );
};

export default Controller;