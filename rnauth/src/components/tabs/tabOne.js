import React, {useState, useEffect, useContext} from 'react';
import {
  StyleSheet,
  View,
  Alert,
  TextInput,
  FlatList,
  StatusBar,
  KeyboardAvoidingView,
  Platform,
  LogBox,
  TouchableOpacity,
  PixelRatio,
  Image,
  ScrollView,
  BackHandler,
} from 'react-native';
import {Card, List, Text} from 'native-base';
import AsyncStorage from '@react-native-community/async-storage';
import {Backdrop} from 'react-native-backdrop';
import {useNavigation, useFocusEffect} from '@react-navigation/native';
import Spinner from 'react-native-loading-spinner-overlay';
import Feather from 'react-native-vector-icons/Feather';
import {SafeAreaView} from 'react-native-safe-area-context';
import NetInfo from '@react-native-community/netinfo';
import rnauth_api from '../../api/rnauth_api';
import Constants from '../../config/Constants';
import TransactionModal from '../modals/TransactionModal';
import {Context} from '../../context/utilsContext';
import Countries from '../../config/Countries';
import cca from '../../config/cca';

LogBox.ignoreLogs(['Warning: ']);
import CurrencyConvertor from '../../config/CurrencyConvertor';

var user_name = null;
const default_flag =
  'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACgAAAAeCAMAAABpA6zvAAAABGdBTUEAALGPC/xhBQAAAAFzUkdCAK7OHOkAAAAgY0hSTQAAeiYAAICEAAD6AAAAgOgAAHUwAADqYAAAOpgAABdwnLpRPAAAASxQTFRFAAAAHxwA4MsA/+kA/+cA/+gA+9IB+9EA+tAA+tME+tIC+s8A4k4H4kwF32Eg341c0pJG4bBy5Jlp32Af3jgI3TYG3F888NHL4NjbalUyvaGC/fn978/K3jkI3jgG20sg79DI////zsfHXFNS29nb4DkI3zYE4YRp/fz+//3/nJybdHVz8e/x//7//fz9wzIHwjEG3qiZ+/n7a2xrSU1Jur2618nG9+7tGwcBGQYAn5eWwL7AR0hHT01Kn0Ms3XVe/vf4n5iXZWRl7uzusbCxS0xNpnlv0lI19eflZWVmGhoax8bH9/f3paOlsa+xp6ep2cO/47ClxsTFNTU3xMPG8vDzs7K1sbCz+ff6xMTHHBkAODUagHxhjIhtn5uBgn5j38oA3cgA4cwC4MsBYexgUAAAAAFiS0dEIcRsDRYAAAAJcEhZcwAAAEgAAABIAEbJaz4AAADaSURBVDjLY2AYAoCRSMDARCRgYCYSMLAQCRhYiQQMbOiAnYOTi5sdQ5iBBw3w8vELCAoJ86KLM4igAlExcQlJKWkZMVE0CQZZFCAnr6CopKyiqKggL4cqw6CKAtTUNTS1tHV0dfXU1VBlGPRRgIGhopGxiamZuaKhAaoMgwUKsLRSVLS2sbWzd3C0RJVBj3onPU1nF1c3dw9PAmnEy9vH188/IDDIi1BqCg4JDQuPiAwmmMyiomNi4+Kjowgns4TEpOTEhMGdzHAAjGSGC6AnM5wALZnhBtRXCAA5O0ad99aTXAAAACV0RVh0ZGF0ZTpjcmVhdGUAMjAxMy0xMC0wN1QxMzoxNDoyNyswMjowMB60MsYAAAAldEVYdGRhdGU6bW9kaWZ5ADIwMTMtMTAtMDdUMTM6MTQ6MjcrMDI6MDBv6Yp6AAAAAElFTkSuQmCC';

const TabOne = ({tabStatus}) => {
  console.log('Tabe One Status: ', tabStatus);
  const {state, backButtonHandller, openModal, closeModal} =
    useContext(Context);
  const [UserName, setUserName] = useState('');
  const [visibility, setVisibility] = useState(false);
  const [value, setValue] = useState('');
  const [data, setData] = useState(USER_DATA);
  const [user_rate, setUserRate] = useState(USER_DATA[0].rate);
  const [user_currency, setUserCurrency] = useState(USER_DATA[0].country);
  const [user_flag, setUserFlag] = useState(USER_DATA[0].flag);
  const [spinner, setSpinner] = useState(false);
  const [users, setUsers] = useState([]);
  const [originalValue, setOriginalValue] = useState('');
  const [convertedValue, setConvertedValue] = useState('');
  const [visible, setVisible] = useState(false);
  const [pay_method, setPayMethod] = useState('');
  const [recipient_phone, setRecipientPhone] = useState('');
  const [def_flag, setDefFrag] = useState(default_flag);

  const navigation = useNavigation();

  // useFocusEffect get called each time when screen comes in focus
  useFocusEffect(
    React.useCallback(() => {
      const onBackPress = () => {
        {
          tabStatus === 0
            ? Alert.alert(
                'Close your app?',
                'Are you sure you want to close the app and leave the screen?',
                [
                  {text: "Don't leave", style: 'cancel', onPress: () => {}},
                  {
                    text: 'Yes',
                    style: 'destructive',
                    onPress: () => BackHandler.exitApp(),
                  },
                ],
              )
            : null;
        }
        // Return true to stop default back navigaton
        // Return false to keep default back navigaton
        return true;
      };

      // Add Event Listener for hardwareBackPress
      BackHandler.addEventListener('hardwareBackPress', onBackPress);

      return () => {
        // Once the Screen gets blur Remove Event Listener
        BackHandler.removeEventListener('hardwareBackPress', onBackPress);
      };
    }, []),
  );

  const showModal = () => {
    setVisible(true);
    console.log('Close Status = ', visible);
  };

  const hideModal = () => {
    setVisible(false);
    setOriginalValue('');
    setConvertedValue('');
    setValue('');
    setDefFrag(default_flag);
    setUserFlag(USER_DATA[0].flag);
    console.log('Close Status = ', visible);
  };

  const showUserList = () => {
    _loadUsers();
  };
  const hideUserList = () => {
    setVisibility(false);
    setValue('');
  };

  const searchFilterFunction = (text) => {
    setValue(text);
    const newData = users.filter((item) => {
      const itemData = `${item.full_name.toUpperCase()}`;
      const textData = text.toUpperCase();
      return itemData.indexOf(textData) > -1;
    });
    // controls what data should be displayed in the list
    if (text !== '') {
      setUsers(newData);
    } else {
      setUsers(users);
    }
  };

  const renderSeparator = () => {
    return <View style={{height: 1, backgroundColor: '#EAEAEA', margin: 5}} />;
  };

  useEffect(() => {
    getFlag();
    _loadInitialState();
    const unsubscribe = NetInfo.addEventListener((state) => {
      handleConnectivityChange();
    });
    unsubscribe();
  }, [navigation]);

  const handleConnectivityChange = () => {
    NetInfo.fetch().then((state) => {
      if (!state.isConnected) {
        navigation.navigate('_internetRetry');
      }
    });
  };

  const _loadInitialState = async () => {
    user_name = await AsyncStorage.getItem('name');
    if (user_name !== null) {
      try {
        navigation.navigate('_dashboard');
        setUserName(user_name);
        const response = await rnauth_api.get('/view_users.php');
        if (response.data !== null) {
          setValue(response.data[0].full_name);
          setUserRate(response.data[0].rate);
          setUserCurrency(response.data[0].currency);
          setUserFlag(response.data[0].flag);
        }
      } catch (err) {
        Alert.alert('Sorry!', 'user initialization failed');
      }
    } else {
      navigation.navigate('_login');
      // navigation.navigate('_dashboard');
    }
  };

  const getSelectedUser = (
    name,
    user_rate,
    user_currency,
    user_flag,
    method,
    phone,
  ) => {
    setValue(name);
    setUserRate(user_rate);
    setUserCurrency(user_currency);
    setUserFlag(user_flag);
    setVisibility(false);
    setPayMethod(method);
    setRecipientPhone(phone);
  };

  const _loadUsers = async () => {
    setSpinner(true);
    try {
      const response = await rnauth_api.get('/view_users.php');
      if (response.data !== null) {
        setVisibility(true);
        setUsers(response.data);
        setSpinner(false);
      }
    } catch (err) {
      Alert.alert('Sorry!', err);
      setSpinner(false);
    }
  };

  // get user flag according the logged in user
  const getFlag = async () => {
    var country = await AsyncStorage.getItem('country');
    for (var i = 0; i < cca.length; i++) {
      if (cca[i] === country) {
        let flag = Countries[cca[i]].flag;
        setDefFrag(flag);
      } else {
        let flag = Countries.UG.flag;
        setDefFrag(flag);
      }
    }
  };

  return (
    <SafeAreaView
      style={{backgroundColor: Constants.PrimaryColor, flex: 1}}
      forceInset={{top: 'never'}}>
      <StatusBar
        backgroundColor={Constants.PrimaryColor}
        barStyle="dark-content"
        hidden={false}
      />
      <KeyboardAvoidingView
        style={{flex: 1}}
        behavior={Platform.OS == 'ios' ? 'padding' : 'height'}
        keyboardVerticalOffset={Platform.OS === 'ios' ? 40 : 0}>
        <View style={styles.container}>
          <Spinner
            visible={spinner}
            textContent={'Loading...'}
            textStyle={{color: '#FFFFFF'}}
          />
          <View
            style={{
              width: '100%',
              height: '40%',
              alignItems: 'center',
              justifyContent: 'space-around',
              backgroundColor: Constants.PrimaryColor,
            }}>
            {/* <Image source={app_logo} style={styles.logoSize} /> */}
            <Text
              style={{
                padding: 10,
                fontFamily: Constants.Bold,
                color: '#FFFFFF',
                position: 'absolute',
                top: 10,
              }}>
              Transfara
            </Text>
          </View>

          <View
            style={{
              width: '100%',
              height: '60%',
              backgroundColor: '#EEEEEE',
            }}>
            {visibility && (
              <View
                style={{
                  position: 'absolute',
                  bottom: 0,
                  width: '100%',
                  alignSelf: 'center',
                  marginTop: 5,
                }}>
                <View
                  style={{
                    flexDirection: 'row',
                    backgroundColor: '#FFFFFF',
                    padding: 5,
                    marginTop: 5,
                    marginHorizontal: 8,
                    borderRadius: 5,
                    justifyContent: 'space-between',
                  }}>
                  <View style={{margin: 5, flexDirection: 'row'}}>
                    <View style={styles.addButton}>
                      <Feather
                        name="user-plus"
                        style={styles.iconAdd}
                        onPress={() => navigation.navigate('_recepients')}
                      />
                    </View>
                    <View style={{padding: 5}}>
                      <Text
                        style={{
                          color: '#576CA8',
                          fontFamily: 'texgyreadventor-bold',
                        }}
                        onPress={() => navigation.navigate('_recepients')}>
                        Add a new account
                      </Text>
                    </View>
                  </View>
                  <View style={{padding: 5}}>
                    <Feather
                      name="chevron-right"
                      style={styles.iconAdd}
                      onPress={() => navigation.navigate('_recepients')}
                    />
                  </View>
                </View>
              </View>
            )}
          </View>

          <View
            style={{
              width: '90%',
              height: '50%',
              borderRadius: 15,
              backgroundColor: '#FFFFFF',
              position: 'absolute',
              padding: 5,
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <View
              style={{
                // flex: 1,
                width: '100%',
                height: '100%',
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <View
                style={{
                  marginTop: 15,
                  height: '40%',
                  width: '100%',
                  justifyContent: 'flex-start',
                }}>
                <View style={{margin: 5, height: '25%', marginBottom: 10}}>
                  <Text
                    style={{
                      margin: 5,
                      color: '#576CA8',
                      fontFamily: 'texgyreadventor-bold',
                    }}>
                    Send Money
                  </Text>
                  <Text
                    note
                    style={{marginLeft: 5, fontFamily: 'texgyreadventor-bold'}}>
                    To
                  </Text>
                </View>
                <View style={{paddingTop: 15, height: '25%'}}>
                  <View
                    style={{
                      flexDirection: 'row',
                      justifyContent: 'space-between',
                      height: 40,
                      margin: 5,
                    }}>
                    {visibility && (
                      <Feather name="search" style={styles.iconStyle} />
                    )}

                    <View
                      style={{flex: 1, justifyContent: 'center', marginTop: 5}}>
                      <TextInput
                        style={{
                          height: 40,
                          marginHorizontal: 5,
                          fontFamily: Constants.Regular,
                        }}
                        defaultValue={value}
                        value={value}
                        onChangeText={(text) => searchFilterFunction(text)}
                        placeholder="Receipient name"
                        onFocus={showUserList}
                        // onBlur={hideUserList}
                      />
                    </View>

                    <Feather
                      name="x"
                      style={styles.closeIcon}
                      onPress={() => hideUserList()}
                    />
                  </View>
                </View>
              </View>

              {/* {visibility && (
                <View style={styles.resultsComponent}>
                  <ScrollView showsVerticalScrollIndicator={false}>
                    <List style={{backgroundColor: '#EEEEEE'}}>
                      <Card>
                        <ListItem>
                          <Left>
                            <View style={styles.addButton}>
                              <Feather
                                name="user-plus"
                                style={styles.iconAdd}
                                onPress={() =>
                                  navigation.navigate('_recepients')
                                }
                              />
                            </View>
                            <View style={{padding: 5}}>
                              <Text
                                style={{
                                  color: '#576CA8',
                                  fontFamily: 'texgyreadventor-bold',
                                }}
                                onPress={() =>
                                  navigation.navigate('_recepients')
                                }>
                                Add a new account
                              </Text>
                            </View>
                          </Left>
                          <Right>
                            <Feather
                              name="chevron-right"
                              style={styles.iconAdd}
                              onPress={() => navigation.navigate('_recepients')}
                            />
                          </Right>
                        </ListItem>
                      </Card>
                      <View style={{padding: 5}}>
                        <Text note style={{fontFamily: 'texgyreadventor-bold'}}>
                          SUGGESTED
                        </Text>
                      </View>
                      <Card>
                        <FlatList
                          style={{marginTop: 15}}
                          showsVerticalScrollIndicator={false}
                          data={users}
                          renderItem={({item, index}) => {
                            return (
                              <TouchableOpacity
                                onPress={() =>
                                  getSelectedUser(
                                    item.full_name,
                                    item.rate,
                                    item.currency,
                                    item.flag,
                                    item.method,
                                    item.receiptient_number,
                                  )
                                }>
                                {index !== 0 && (
                                  <View style={styles.listItems}>
                                    {item.method === 'Mobile Money' && (
                                      <View style={{flexDirection: 'row'}}>
                                        <View
                                          style={{
                                            justifyContent: 'center',
                                            marginRight: 10,
                                          }}>
                                          <Image
                                            style={styles.imgStyle}
                                            source={{uri: item.flag}}
                                          />
                                        </View>
                                        <View>
                                          <Text style={styles.searchResults}>
                                            {item.full_name}
                                          </Text>
                                          <Text
                                            note
                                            style={{
                                              fontFamily:
                                                'texgyreadventor-regular',
                                            }}>
                                            {item.phone_number} {item.method}
                                          </Text>
                                        </View>
                                      </View>
                                    )}
                                    {item.method === 'Bank' && (
                                      <View style={{flexDirection: 'row'}}>
                                        <View
                                          style={{
                                            justifyContent: 'center',
                                            marginRight: 10,
                                          }}>
                                          <Image
                                            style={styles.imgStyle}
                                            source={{uri: item.flag}}
                                          />
                                        </View>
                                        <View>
                                          <Text style={styles.searchResults}>
                                            {item.full_name}
                                          </Text>
                                          <Text
                                            note
                                            style={{
                                              fontFamily:
                                                'texgyreadventor-regular',
                                            }}>
                                            {item.bank_account} {item.method}
                                          </Text>
                                        </View>
                                      </View>
                                    )}
                                    {renderSeparator()}
                                  </View>
                                )}
                              </TouchableOpacity>
                            );
                          }}
                          keyExtractor={(item) =>
                            item.receiptient_id.toString()
                          }
                        />
                      </Card>
                    </List>
                  </ScrollView>
                </View>
              )} */}

              {!visibility && (
                <>
                  <View
                    style={{
                      width: '100%',
                      height: '25%',
                      justifyContent: 'center',
                      flexDirection: 'row',
                    }}>
                    <View
                      style={{
                        borderWidth: 1,
                        borderColor: '#EEEEEE',
                        height: '93%',
                        width: '50%',
                        justifyContent: 'center',
                        borderRadius: 5,
                      }}>
                      <View style={{alignItems: 'center', marginTop: 10}}>
                        <Text note style={{fontFamily: 'texgyreadventor-bold'}}>
                          SEND
                        </Text>
                      </View>
                      <View
                        style={{
                          flexDirection: 'row',
                          alignItems: 'center',
                          paddingLeft: 5,
                        }}>
                        <View>
                          <TextInput
                            style={{
                              fontSize: 18,
                              fontFamily: 'texgyreadventor-rgular',
                              marginLeft: 5,
                            }}
                            keyboardType="numeric"
                            placeholder={'0.00'}
                            value={originalValue}
                            onChangeText={setOriginalValue}
                          />
                        </View>
                        <View style={{marginBottom: 5}}>
                          <Image
                            style={styles.imgStyle}
                            source={{uri: def_flag}}
                          />
                        </View>
                      </View>
                    </View>
                    <View
                      style={{
                        borderWidth: 1,
                        borderColor: '#EEEEEE',
                        height: '93%',
                        width: '50%',
                        justifyContent: 'center',
                        borderRadius: 5,
                      }}>
                      <View style={{alignItems: 'center', marginTop: 10}}>
                        <Text note style={{fontFamily: 'texgyreadventor-bold'}}>
                          RECEIVE
                        </Text>
                      </View>
                      <View
                        style={{
                          flexDirection: 'row',
                          alignItems: 'center',
                          paddingLeft: 5,
                        }}>
                        <View>
                          <CurrencyConvertor
                            rate={user_rate}
                            convertedValue={convertedValue}
                            originalValue={originalValue}
                            setConvertedValue={setConvertedValue}
                          />
                        </View>
                        <View style={{marginBottom: 5}}>
                          <Image
                            style={styles.imgStyle}
                            source={{uri: user_flag}}
                          />
                        </View>
                      </View>
                    </View>
                  </View>

                  <View
                    style={{
                      width: '100%',
                      height: '10%',
                      alignItems: 'center',
                      justifyContent: 'center',
                      margin: 5,
                      flexDirection: 'row',
                    }}>
                    <Text
                      note
                      style={{
                        fontFamily: Constants.Regular,
                        marginRight: 5,
                        fontSize: 13,
                      }}>
                      Exchange Rate:
                    </Text>
                    <Text
                      note
                      style={{fontFamily: Constants.Regular, fontSize: 12}}>
                      1.0 {user_currency} = {user_rate} UGX
                    </Text>
                  </View>
                  {originalValue !== '' && value !== '' ? (
                    <TouchableOpacity
                      onPress={() => setVisible(true)}
                      // onPress={showModal}
                      style={{
                        width: '95%',
                        height: '25%',
                        justifyContent: 'center',
                        marginBottom: 10,
                        alignItems: 'center',
                      }}>
                      <View
                        style={{
                          backgroundColor: '#576CA8',
                          width: '95%',
                          height: '65%',
                          alignItems: 'center',
                          justifyContent: 'center',
                          borderRadius: 30,
                          marginBottom: 10,
                        }}>
                        <Text style={{color: 'white'}}>Send</Text>
                      </View>
                    </TouchableOpacity>
                  ) : (
                    <TouchableOpacity
                      style={{
                        width: '95%',
                        height: '25%',
                        justifyContent: 'center',
                        marginBottom: 10,
                        alignItems: 'center',
                      }}>
                      <View
                        style={{
                          backgroundColor: '#576CA8',
                          width: '95%',
                          height: '65%',
                          alignItems: 'center',
                          justifyContent: 'center',
                          borderRadius: 30,
                          marginBottom: 10,
                          opacity: 0.5,
                        }}>
                        <Text
                          style={{
                            fontFamily: 'texgyreadventor-bold',
                            color: 'white',
                          }}>
                          Send
                        </Text>
                      </View>
                    </TouchableOpacity>
                  )}
                </>
              )}
            </View>
          </View>
        </View>

        {visibility && (
          <View style={styles.resultsComponent}>
            <List>
              <View style={{padding: 5}}>
                <Text note style={{fontFamily: 'texgyreadventor-bold'}}>
                  SUGGESTED
                </Text>
              </View>
              <ScrollView showsVerticalScrollIndicator={false}>
                <Card>
                  <FlatList
                    style={{marginTop: 5}}
                    showsVerticalScrollIndicator={false}
                    data={users}
                    renderItem={({item, index}) => {
                      let number = users.length - 1;
                      return (
                        <TouchableOpacity
                          onPress={() =>
                            getSelectedUser(
                              item.full_name,
                              item.rate,
                              item.currency,
                              item.flag,
                              item.method,
                              item.receiptient_number,
                            )
                          }>
                          {index !== 0 && (
                            <View style={styles.listItems}>
                              {item.method === 'Mobile Money' && (
                                <View style={{flexDirection: 'row'}}>
                                  <View
                                    style={{
                                      justifyContent: 'center',
                                      marginRight: 10,
                                    }}>
                                    <Image
                                      style={styles.imgStyle}
                                      source={{uri: item.flag}}
                                    />
                                  </View>
                                  <View>
                                    <Text style={styles.searchResults}>
                                      {item.full_name}
                                    </Text>
                                    <Text
                                      note
                                      style={{
                                        fontFamily: 'texgyreadventor-regular',
                                      }}>
                                      {item.phone_number} {item.method}
                                    </Text>
                                  </View>
                                </View>
                              )}
                              {item.method === 'Bank' && (
                                <View style={{flexDirection: 'row'}}>
                                  <View
                                    style={{
                                      justifyContent: 'center',
                                      marginRight: 10,
                                    }}>
                                    <Image
                                      style={styles.imgStyle}
                                      source={{uri: item.flag}}
                                    />
                                  </View>
                                  <View>
                                    <Text style={styles.searchResults}>
                                      {item.full_name}
                                    </Text>
                                    <Text
                                      note
                                      style={{
                                        fontFamily: 'texgyreadventor-regular',
                                      }}>
                                      {item.bank_account} {item.method}
                                    </Text>
                                  </View>
                                </View>
                              )}
                              {renderSeparator()}
                            </View>
                          )}
                          {index === number && (
                            <View style={{marginBottom: 50}}></View>
                          )}
                        </TouchableOpacity>
                      );
                    }}
                    keyExtractor={(item) => item.receiptient_id.toString()}
                  />
                </Card>
              </ScrollView>
            </List>
          </View>
        )}

        <Backdrop
          visible={visible}
          handleOpen={showModal}
          handleClose={hideModal}
          overlayColor={visible === true ? 'rgba(0,0,0,0.6)' : 'transaparent'}
          backdropStyle={{backgroundColor: 'transparent'}}
          containerStyle={{
            backgroundColor: visible === true ? '#fafafa' : 'transaparent',
            height: visible === true ? '70%' : '5%',
            borderTopLeftRadius: 25,
            borderTopRightRadius: 25,
          }}
          beforeOpen={() => {
            console.log('beforeOpen');
            setVisible(true);
          }}
          closeOnBackButton={false}>
          {visible === true && (
            <TransactionModal
              recipientName={value}
              conertedVal={convertedValue + ' ' + user_currency}
              exchangeRate={'1.0 ' + user_currency + ' = ' + user_rate + ' UGX'}
              fees={'0.00 ' + user_currency}
              totalPay={convertedValue + ' ' + user_currency}
              toRecipient={originalValue + ' UGX'}
              payMethod={pay_method}
              recipientPhone={recipient_phone}
              userCurrency={user_currency}
              userRate={user_rate}
              originalValue={originalValue}
              Fees={'0.00'}
              Flag={user_flag}
              HideModal={hideModal}
            />
          )}
        </Backdrop>
      </KeyboardAvoidingView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  textDiv: {
    // flex: 2 / 3,
    height: '20%',
    width: '100%',
    alignSelf: 'stretch',
    paddingBottom: 8,
    marginBottom: 8,
    padding: 5,
    marginTop: 5,
    height: '40%',
  },
  resultStyle: {
    flex: 8,
    alignSelf: 'stretch',
  },
  buttonStyle: {
    flex: 2 / 3,
    alignSelf: 'stretch',
    marginHorizontal: 5,
    paddingBottom: 5,
    marginBottom: 5,
  },
  textInput: {
    height: 40,
    marginHorizontal: 5,
  },
  resultsComponent: {
    flex: 1,
    backgroundColor: '#EEEEEE',
    padding: 5,
    // width: '90%'
    // marginHorizontal: 5,
    // borderBottomLeftRadius: 5,
    // borderBottomRightRadius: 5,
  },
  resultsComp: {
    flex: 1,
    marginHorizontal: 5,
    borderBottomLeftRadius: 5,
    borderBottomRightRadius: 5,
    justifyContent: 'flex-end',
    marginBottom: 5,
  },
  listItems: {
    width: 500, // to be updated
    marginHorizontal: 1,
    // height: 90,
    paddingLeft: 10,
  },
  altText: {
    height: 30,
    alignItems: 'center',
    justifyContent: 'center',
  },
  footerText: {
    padding: 5,
    margin: 5,
    fontSize: 14,
  },
  headerText: {
    padding: 5,
    margin: 5,
    fontSize: 20,
    color: '#FFFFFF',
    fontWeight: '700',
  },
  iconAdd: {
    color: '#576CA8',
    fontSize: 25,
    padding: 5,
  },
  closeIcon: {
    color: 'black',
    fontSize: 20,
    // padding: 5,
    alignSelf: 'center',
    paddingRight: 5,
    paddingTop: 5,
  },
  title: {
    color: 'black',
    fontWeight: '700',
    fontSize: 15,
  },
  addButton: {
    backgroundColor: '#EEEEEE',
    borderRadius: 50,
    marginVertical: 5,
    marginTop: 2,
  },
  searchResults: {
    padding: 5,
    fontSize: 15,
    color: '#576CA8',
    fontFamily: 'texgyreadventor-bold',
  },
  imgStyle: {
    resizeMode: 'contain',
    width: 35,
    height: 25,
    borderWidth: 1 / PixelRatio.get(),
    borderColor: '#eee',
    opacity: 0.8,
  },
  iconStyle: {
    fontSize: 25,
    alignSelf: 'center',
    paddingLeft: 5,
    paddingTop: 5,
    color: '#576CA8',
  },
  logoSize: {
    position: 'absolute',
    top: 10,
    height: 72,
    width: 72,
  },
});

const USER_DATA = [
  {
    id: 1,
    name: '',
    age: 20,
    country: 'USD',
    rate: 1.1,
    flag: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACgAAAAeCAMAAABpA6zvAAAABGdBTUEAALGPC/xhBQAAAAFzUkdCAK7OHOkAAAAgY0hSTQAAeiYAAICEAAD6AAAAgOgAAHUwAADqYAAAOpgAABdwnLpRPAAAAj1QTFRF4bm96r/B6b/B3a6y5bO15bK1wltjyF1j8unr+vDx+vDwyHB2z3J31ZSZ3JicGS9dJTplITdjGjBdKDxnHjNhGzFfKT1oHzZjQjJXtzxFvT1EGjBedYKdXWyNFStaIDVih5OrRFV7FCpZLkJsjpmvdYKeGC5cWmyNQDFWuD5Hvj9GM0dwRlh9NEdwOEtzLEBqTF2BLUFrOUx0M0dvPVB2KT9pTFJ1HDJfeIWgFy1bJjpmhI+oNklxFi1bS12BY3KRFixbUmOF9/f5////aHaUVGSGIjdjPlF3Gi9dfYqkU2SGaHeVUGKFSk5ySlt/PlB3MkZvMUVuKz9qV2eJKz9pO093QDFVtjpCvDpBFSxbgo6nUWKEEilYJjtmj5qwOUxzEihYFCtaa3mXFCxbRDpfRFZ7KDxoJzxnXW2NKT5pYXCQQFJ5UF6BTl+DHjNgcH6aO051HjRhYnGQS1yAUGCD9fT1/vv8/vv7Fy1cfYmjT1+DiZSsZ3aUFS1cRkBkO011M0ZvMERtQVN5QlR6LEBrQ1V7Ok10N0pyL0VvQTBVtTc/uzc+coCcW2qLhJCpLkJri5atc4CcWGmLR0drHzRhITZiaXiWRVd8c4GdIDZiWGiKJTpmPE92YG+PL0NtKj5pJztnIDZjTlh76c/S8dbX8dXXdoOfXm2NiZWsEylZL0JskJuxd4SfFy5cW22OQDNXukZOwUZNwEZNJDllPVB3P1F4MkdwSzxfu0ZOzdLczNHbzdHbzNDb1tPb8NXXWNrergAAAA90Uk5T/v7+/v7+/v7+/v7+/v7+6a2FXwAAAAFiS0dEQ2fQDWIAAAAJcEhZcwAAAEgAAABIAEbJaz4AAAITSURBVDjLY+AXEOQXEhbhFxUT5RcRFuIXFODnFxCXkJRCAwzSMrJy8gqKSsoqykqKCvJysqpqaqrqGppaaICBX1tZR1dP38DQyNBAX09XR9nYxMTY1IyBkQkVMPDzm1sYWFpZ29jaWFtZ2lmYq9nbqzk4OjmjAQZpF1dpNwt3D0NPQw93CzdpL29zc28fX2YWVlTAwO/nryzmFRAYFBwUGOAlpuzvFxLiFxoWHoEGGPgjpaOiY2Lj4hPi42JjoqOkE5OS5JJT2NjRAIOQY2paeoZNpn6WfqZNRnpaqmNsrGN2DgcnFypg4M/KzXPLLyjUV9UvLMh3y8stKiwsKi4pLStHBQz8FaKVVYmx1XpKetWxiVWVog41Nba1ddw8aICBv77Bv7HJsLmltaXZsKnRv6Gtvb2to7OrGw0w8Pf0ysn3NSf2T+hPbO6Tl+udqKY2cdJkXj40wMA/Zeq06XnpM7TztGek502fNjNv1qxCaSwBzp8+e9YcMeG58+bPmyssNmfW7AXFxQsWLlq8ZCkqYOBftlxJfkXqylWrV61MXSGvtHzN2rVr1q3fsHETKmAQMNLdHLtFefP8rfM3K2+J3axrJCBgtG37DgyFO3ft2rlz1+6dO/fs3LkbxATjvfuWolvtTCRg4CMSMHQTCRh4iAQM5UQCBi4iAQM7kYAhgkjAwEokID7AmYgEDFpEAgYpIgEA2hc6qEvsr/QAAAAldEVYdGRhdGU6Y3JlYXRlADIwMTMtMTAtMDdUMTM6MTU6MTUrMDI6MDDoZk8yAAAAJXRFWHRkYXRlOm1vZGlmeQAyMDEzLTEwLTA3VDEzOjE1OjE1KzAyOjAwmTv3jgAAAABJRU5ErkJggg==',
  },
];

export default TabOne;
