import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {SafeAreaProvider} from 'react-native-safe-area-context';

const Stack = createStackNavigator();

import UserLogin from './src/screens/UserLogin';
import SignUp from './src/screens/SignUp';
import Home from './src/screens/Home';
import IntroScreen from './src/screens/IntroScreen';
import Dashboard from './src/components/Dashboard';
import ScreenOne from './src/screens/ScreenOne';
import ScreenTwo from './src/screens/ScreenTwo';
import ManagePayments from './src/screens/ManagePayments';
import ChangePassword from './src/screens/ChangePassword';
import UserProfile from './src/screens/UserProfile';
import Recepients from './src/screens/Recepients';
import AddPaymentMethod from './src/screens/AddPaymentMethod';
import SuccessPage from './src/screens/SuccessPage';
import TransactionDetails from './src/screens/TransactionDetails';
import Controller from './src/screens/Controller';
import InternetRetry from './src/screens/InterntRetry';
import Otp from './src/screens/Otp';
import WebviewComponent from './src/components/WebviewComponent';
import TabThree from './src/components/tabs/tabThree';
import UploadDocuments from './src/screens/UploadDocuments';
import {Provider as UtilsProvider} from './src/context/utilsContext';
import {navigationRef} from './src/RootNavigation';

const App = () => {
  return (
    <NavigationContainer ref={navigationRef}>
      <Stack.Navigator
        initialRouteName="_controller"
        screenOptions={{
          headerShown: false,
        }}>
        <Stack.Screen
          name="_controller"
          component={Controller}
          options={{title: 'App Controller'}}
        />
        <Stack.Screen
          name="_intro"
          component={IntroScreen}
          options={{title: 'App Intro'}}
        />
        <Stack.Screen
          name="_login"
          component={UserLogin}
          options={{title: 'User Login'}}
        />
        <Stack.Screen
          name="_signup"
          component={SignUp}
          options={{title: 'User Registration'}}
        />
        <Stack.Screen
          name="_home"
          component={Home}
          options={{title: 'User Dashboard'}}
        />
        <Stack.Screen
          name="_dashboard"
          component={Dashboard}
          options={{title: 'Dashboard'}}
        />
        <Stack.Screen
          name="_screenOne"
          component={ScreenOne}
          options={{title: 'FAQs'}}
        />
        <Stack.Screen
          name="_screenTwo"
          component={ScreenTwo}
          options={{title: 'Messages'}}
        />
        <Stack.Screen
          name="_managePayments"
          component={ManagePayments}
          options={{title: 'Payments'}}
        />
        <Stack.Screen
          name="_changePassword"
          component={ChangePassword}
          options={{title: 'Change Password'}}
        />
        <Stack.Screen
          name="_userProfile"
          component={UserProfile}
          options={{title: 'User Details'}}
        />
        <Stack.Screen
          name="_recepients"
          component={Recepients}
          options={{title: 'Receipients'}}
        />
        <Stack.Screen
          name="_addPayMethod"
          component={AddPaymentMethod}
          options={{title: 'Payment Method'}}
        />
        <Stack.Screen
          name="_successPage"
          component={SuccessPage}
          options={{title: 'Success Page'}}
        />
        <Stack.Screen
          name="_transactionDetails"
          component={TransactionDetails}
          options={{title: 'Transaction Details'}}
        />
        <Stack.Screen
          name="_internetRetry"
          component={InternetRetry}
          options={{title: 'Internet Retry'}}
        />
        <Stack.Screen name="_otp" component={Otp} options={{title: 'OTP'}} />
        <Stack.Screen
          name="_uploadDocuments"
          component={UploadDocuments}
          options={{title: 'Upload Documents'}}
        />

        <Stack.Screen
          name="_webview"
          component={WebviewComponent}
          options={{title: 'Terms & Policies'}}
        />

        <Stack.Screen
          name="_tabthree"
          component={TabThree}
          options={{title: 'Tab Three'}}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

// export default App;

export default () => {
  return (
    <SafeAreaProvider>
      <UtilsProvider>
        <App />
      </UtilsProvider>
    </SafeAreaProvider>
  );
};
