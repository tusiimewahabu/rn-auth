import React, { useState, useRef, useEffect } from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  Text,
  View,
  StatusBar,
  // TextInput,
  Button,
  Image,
  Alert,
  TouchableOpacity,
  KeyboardAvoidingView
} from 'react-native';
import Spinner from 'react-native-loading-spinner-overlay';
import { TextInput } from 'react-native-paper';
import AsyncStorage from '@react-native-community/async-storage';
import { Container, Picker } from 'native-base';
import Feather from 'react-native-vector-icons/Feather';
import PhoneInput from "react-native-phone-number-input";
import Constants from '../config/Constants';
import { useNavigation } from '@react-navigation/native';

const UserProfile = ({ route }) => {
    useEffect(() => {
    load_user_profile();
}, []);

const load_user_profile = async() => {
  const name = await AsyncStorage.getItem('name');
  setName(name != null? name: "Jane Doe");
  const phone =  await AsyncStorage.getItem('phone');
  setPhoneNumber(phone != null? phone: "+256753000000");
  const id = await AsyncStorage.getItem('userid');
  setUserId(id != null? id: "1");
  const country_name = await AsyncStorage.getItem('country');
  setCountry(country_name != null? country_name: "UG");
  setCountryCode(country_name != null? country_name: "UG");
  const email_address = await AsyncStorage.getItem('email');
  setEmail(email_address != null? email_address: "example@gmail.com");
  const f_id = await AsyncStorage.getItem('firebase_ref_id');
  setFbId(f_id != null? f_id: "XXYYZZWWWCC987");
};

  const navigation = useNavigation();
  const [userAddress, setUserAddress] = useState("Plot 73 High Street, Kampala Uganda");
  const [phoneNumber, setPhoneNumber] = useState(phoneNumber);
  const [paymet_method, setPaymentMethod] = useState("");
  const [spinner, setSpinner] = useState(false);
  const [countryCode, setCountryCode] = useState("");
  const [disabled, setDisabled] = useState(false);
  const [valid, setValid] = useState(false);
  const [formattedValue, setFormattedValue] = useState('');
  const [full_name, setName] = useState("");
  const [user_id, setUserId] = useState("");
  const [country, setCountry] = useState("");
  const [email, setEmail] = useState("");
  const [fb_id, setFbId] = useState("");
  const phoneInput = useRef(null);


  return (
    <SafeAreaView
    style={{backgroundColor: Constants.PrimaryColor, flex: 1}}
    forceInset={{top: 'never'}}>
    <Container>
      <StatusBar backgroundColor={Constants.PrimaryColor} hidden={false} />
      <Spinner
        visible={spinner}
        textContent={'Loading...'}
        textStyle={{ color: '#FFFFFF' }}
      />

      <KeyboardAvoidingView
        style={{ flex: 1 }}
        behavior={Platform.OS == 'ios' ? 'padding' : 'height'}
        keyboardVerticalOffset={Platform.OS === 'ios' ? 40 : 0}>

        <View style={styles.container}>

          <View style={{
            width: '100%',
            height: '10%',
            alignItems: 'center',
            justifyContent: 'space-around',
            backgroundColor: Constants.PrimaryColor,
            flexDirection: 'row',
            marginBottom: 5
          }}>
            <Feather name='chevron-left' style={styles.icon_} onPress={() => { navigation.goBack() }} />
            <Text style={{ fontSize: 20, fontFamily: Constants.Bold, color: '#FFFFFF', }}>Account Info</Text>
          </View>

          <View style={{
            width: '100%',
            height: '75%',
            backgroundColor: '#FFFFFF',
            justifyContent: 'center',
            // alignItems: 'center'
          }}>

            <View style={styles.sectionContainer}>
              <Text style={{ fontSize: 20, fontFamily: Constants.Bold, color: Constants.PrimaryColor, textAlign: 'center' }}>Personal Info</Text>
            </View>

            
            <View style={styles.sectionContainer_id}>
              <Text style={{ fontFamily: Constants.Bold, color: Constants.PrimaryColor }}>CUSTOMER ID: {user_id+"-"+fb_id}</Text>
              {/* <Text style={{ fontSize: 14, fontFamily: Constants.Regular, color: Constants.PrimaryColor }}>{user_id}</Text> */}
            </View>

            <View style={styles.sectionContainer_}>
              <Text style={{ fontFamily: Constants.Bold, color: Constants.PrimaryColor }}>FULL NAME</Text>
              <Text style={{ fontSize: 14, fontFamily: Constants.Regular, color: Constants.PrimaryColor }}>{full_name}</Text>
            </View>

            <View style={styles.sectionContainer_}>
              <Text style={{ fontFamily: Constants.Bold, color: Constants.PrimaryColor }}>EMAIL ADDRESS</Text>
              <Text style={{ fontSize: 14, fontFamily: Constants.Regular, color: Constants.PrimaryColor }}>{email}</Text>
            </View>

            <View style={styles.sectionContainer}>
            <Text style={{ fontSize: 14, fontFamily: Constants.Regular, color: Constants.PrimaryColor }}>{phoneNumber}</Text>
              <PhoneInput
                ref={phoneInput}
                defaultValue={phoneNumber}
                value={phoneNumber}
                defaultCode={countryCode}
                layout="first"
                onChangeText={(text) => {
                  setPhoneNumber(text);
                }}
                onChangeFormattedText={(text) => {
                  setFormattedValue(text);
                  // setCountryCode(phoneInput.current?.getCountryCode() || '');
                  setCountryCode(phoneInput.current?.getCountryCode());
                  console.log("COUNTRY", countryCode);
                }}
                countryPickerProps={{ withAlphaFilter: true }}
                disabled={disabled}
                withDarkTheme
                withShadow
              />
            </View>

            <View style={styles.sectionContainer}>
              <TextInput
                autoCapitalize="none"
                autoCorrect={false}
                defaultValue={userAddress}
                value={userAddress}
                onChangeText={setUserAddress}
                mode="outlined"
                label="Address"
                outlineColor={Constants.PrimaryColor}
                placeholder="Type address"
              />
            </View>


          </View>
          <View style={{
            width: '100%',
            height: '15%',
            backgroundColor: '#FFFFFF',
            justifyContent: 'center',
            padding: 5,
          }}>

            <View style={styles.sectionContainer}>
              {paymet_method === "" ? <TouchableOpacity
                // onPress={() => signUp(cardName, cardNumber, moMoNumber, paymet_method, expirlyMonth, expirlyYear)}
                style={{ width: '100%', height: 50, justifyContent: 'center', padding: 10, alignItems: 'center' }}>
                <View style={{ backgroundColor: '#576CA8', width: '100%', height: 50, alignItems: 'center', justifyContent: 'center', borderRadius: 30, opacity: 0.5 }}>
                  <Text style={{ color: 'white', fontFamily: Constants.Regular, }}>Save</Text>
                </View>
              </TouchableOpacity> :
                <TouchableOpacity
                  // onPress={() => signUp(cardName, cardNumber, moMoNumber, paymet_method, expirlyMonth, expirlyYear)}
                  style={{ width: '100%', height: 50, justifyContent: 'center', padding: 10, alignItems: 'center' }}>
                  <View style={{ backgroundColor: '#576CA8', width: '100%', height: 50, alignItems: 'center', justifyContent: 'center', borderRadius: 30 }}>
                    <Text style={{ color: 'white', fontFamily: Constants.Regular, }}>Save</Text>
                  </View>
                </TouchableOpacity>}
            </View>
          </View>
        </View>


      </KeyboardAvoidingView>
    </Container>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    // backgroundColor: Constants.PrimaryColor
  },
  scrollView: {
    backgroundColor: '#EEEEEE',
  },
  logo: {
    backgroundColor: '#EEEEEE',
    alignItems: 'center',
  },
  logoSize: {
    height: 100,
    width: 100,
  },
  sectionContainer: {
    marginLeft: 20,
    marginRight: 20,
    padding: 5,
    borderRadius: 2,
  },
  sectionContainer_: {
    marginLeft: 25,
    marginRight: 25,
    marginTop: 5,
    padding: 8,
    borderRadius: 5,
    backgroundColor: '#FFFFFF',
    borderWidth: 0.5,
    borderColor: '#BABABA'
  },
  sectionContainer_id: {
    marginLeft: 25,
    marginRight: 25,
    marginTop: 5,
    padding: 8,
    borderRadius: 5,
    // backgroundColor: '#FFFFFF',
    // borderWidth: 0.5,
    // borderColor: '#BABABA'
  },
  sectionContainerPicker: {
    marginLeft: 25,
    marginRight: 25,
    borderColor: Constants.PrimaryColor,
    borderWidth: 1,
    borderRadius: 5,
    fontFamily: Constants.Regular
  },
  input: {
    width: '49%',
  },
  highlight: {
    fontWeight: '700',
  },
  textLebal: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
    color: 'black',
  },
  row1: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginLeft: 20,
    marginRight: 20,
    padding: 5,
    borderRadius: 2,
  },
  screen: {
    margin: 5,
    paddingHorizontal: 15,
    padding: 5,
  },
  _input: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
    borderColor: 'lightblue',
    marginVertical: 30,
    borderWidth: 1,
    borderRadius: 5,
  },
  text: {
    fontWeight: '700',
    alignSelf: 'center'
  },
  icon_: {
    color: "#FFFFFF",
    fontSize: 25,
    position: 'absolute',
    left: 10
  },
});

export default UserProfile;


















// import React, {useEffect} from 'react';
// import {StatusBar, BackHandler} from 'react-native';
// import { Container, Header, Left, Body, Right, Title, Button } from 'native-base';
// import Feather from 'react-native-vector-icons/Feather';
// import { useNavigation } from '@react-navigation/native';
// import Constants from '../config/Constants';
// const UserProfile = () => {
//   const navigation = useNavigation();
//   // useEffect(()=>{
//   //   // if(navigation.isFocused()){
//   //     BackHandler.removeEventListener("hardwareBackPress", ()=>{});
//   //   // }
//   // },[]);
//   return (
//     <Container>
//       <Header backgroundColor={Constants.PrimaryColor} style={{backgroundColor: Constants.PrimaryColor}}>
//         <Left>
//           <Button transparent>
//             <Feather name='chevron-left' style={styles.icon} onPress={() => { navigation.goBack() }} />
//           </Button>
//         </Left>
//         <Body>
//           <Title style={{fontFamily: Constants.Bold}}>Account Info</Title>
//         </Body>
//         <Right />
//       </Header>
//       <StatusBar backgroundColor={Constants.PrimaryColor} hidden={false} />
//     </Container>
//   );
// };

// const styles = {
//   icon: {
//     color: "#FFFFFF",
//     fontSize: 20
//   },
// };

// export default UserProfile;