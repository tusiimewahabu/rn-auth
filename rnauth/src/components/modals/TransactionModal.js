import React, { useState } from 'react';
import { Modal, Portal, Button, Provider } from 'react-native-paper';
import { Image, Alert, TouchableOpacity, StatusBar, ScrollView, PixelRatio } from 'react-native';
import { Container, Header, Content, SwipeRow, Card, Row, List, ListItem, Text, View, Body, Title, Left, Right, Icon, Col } from 'native-base';
import Feather from 'react-native-vector-icons/Feather';
import {Backdrop} from 'react-native-backdrop';
import Constants from '../../config/Constants';
import ConfirmPasswordModal from './ConfirmPasswordModal';
import PaymentMethodModal from './PaymentMethodModal';

// const ug_flag = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACgAAAAeCAMAAABpA6zvAAAABGdBTUEAALGPC/xhBQAAAAFzUkdCAK7OHOkAAAAgY0hSTQAAeiYAAICEAAD6AAAAgOgAAHUwAADqYAAAOpgAABdwnLpRPAAAASxQTFRFAAAAHxwA4MsA/+kA/+cA/+gA+9IB+9EA+tAA+tME+tIC+s8A4k4H4kwF32Eg341c0pJG4bBy5Jlp32Af3jgI3TYG3F888NHL4NjbalUyvaGC/fn978/K3jkI3jgG20sg79DI////zsfHXFNS29nb4DkI3zYE4YRp/fz+//3/nJybdHVz8e/x//7//fz9wzIHwjEG3qiZ+/n7a2xrSU1Jur2618nG9+7tGwcBGQYAn5eWwL7AR0hHT01Kn0Ms3XVe/vf4n5iXZWRl7uzusbCxS0xNpnlv0lI19eflZWVmGhoax8bH9/f3paOlsa+xp6ep2cO/47ClxsTFNTU3xMPG8vDzs7K1sbCz+ff6xMTHHBkAODUagHxhjIhtn5uBgn5j38oA3cgA4cwC4MsBYexgUAAAAAFiS0dEIcRsDRYAAAAJcEhZcwAAAEgAAABIAEbJaz4AAADaSURBVDjLY2AYAoCRSMDARCRgYCYSMLAQCRhYiQQMbOiAnYOTi5sdQ5iBBw3w8vELCAoJ86KLM4igAlExcQlJKWkZMVE0CQZZFCAnr6CopKyiqKggL4cqw6CKAtTUNTS1tHV0dfXU1VBlGPRRgIGhopGxiamZuaKhAaoMgwUKsLRSVLS2sbWzd3C0RJVBj3onPU1nF1c3dw9PAmnEy9vH188/IDDIi1BqCg4JDQuPiAwmmMyiomNi4+Kjowgns4TEpOTEhMGdzHAAjGSGC6AnM5wALZnhBtRXCAA5O0ad99aTXAAAACV0RVh0ZGF0ZTpjcmVhdGUAMjAxMy0xMC0wN1QxMzoxNDoyNyswMjowMB60MsYAAAAldEVYdGRhdGU6bW9kaWZ5ADIwMTMtMTAtMDdUMTM6MTQ6MjcrMDI6MDBv6Yp6AAAAAElFTkSuQmCC';

const TransactionModal = ({ conertedVal, exchangeRate, fees, totalPay, toRecipient, recipientName, payMethod, recipientPhone, userCurrency, originalValue, userRate, Fees, Flag, HideModal }) => {
    const [visible, setVisible] = useState(false);
    const [selectMethod, SetSelectMethod] = useState(false);
    const [payMethodValue, SetPayMethod] = useState('VISA.....867');
    const showModal = () => setVisible(true);
    const hideModal = () => setVisible(false);
    const showSelectedMethod = () => SetSelectMethod(true);
    const hideSelectedMethod = () => SetSelectMethod(false);

    const DATA = [
        {
            "base_currency": userCurrency,
            "conversion_currency": "UGX",
            "send_amount": conertedVal,
            "conversion_rate": userRate,
            "receive_amount": originalValue,
            "fees": Fees,
            // "customer_id":user_id,
            "recipient": recipientName,
            "payment_method": payMethodValue,
            "payout_method": payMethod
        }
    ];

    // const DATA = [
    //     { recipient_name: recipientName, exchange_rate: exchangeRate, converted_val: conertedVal, fees: fees, total_pay: totalPay, to_recipient: toRecipient },
    // ];

    return (
        <>
            <View style={styles.container}>
                <View style={{ height: '80%' }}>
                    <View style={{ justifyContent: 'flex-end', flexDirection: 'row', padding: 25 }} >
                        <Feather name="x" style={styles.icon}
                            onPress={HideModal}
                        // onPress={() => console.log("Close button pressed")}
                        />
                    </View>
                    <ScrollView showsVerticalScrollIndicator={false}>
                        <List>
                            <ListItem avatar first >
                                <Row>
                                    <Col style={{ left: -1 }}>
                                        <Text style={styles.textLabel_}>Recipient</Text>
                                    </Col>
                                    <Col style={{ paddingLeft: 5, width: '50%' }}>
                                        <Text style={styles.highlight} numberOfLines={1}>{recipientName}</Text>
                                        <Text note style={styles.highlight_} numberOfLines={1}>{recipientPhone} {payMethod}</Text>
                                    </Col>
                                    <Col style={{ justifyContent: 'center', right: -15 }}>
                                        <Image source={{ uri: Flag }} style={styles.logoSize} />
                                    </Col>
                                </Row>
                            </ListItem>
                            <ListItem>
                                <Left>
                                    <Text style={styles.textLabel}>Total sent</Text>
                                    <Text style={styles.highlight}>{conertedVal}</Text>
                                </Left>
                            </ListItem>
                            <ListItem>
                                <Left>
                                    <Text style={styles.textLabel}>Exchange rate</Text>
                                    <Text style={styles.highlight}>{exchangeRate}</Text>
                                </Left>
                            </ListItem>
                            <ListItem>
                                <Left>
                                    <Text style={styles.textLabel}>Fees</Text>
                                    <Text style={styles.highlight}>{fees}</Text>
                                </Left>
                            </ListItem>
                            <ListItem >
                                <Left>
                                    <Text style={styles.textLabel}>Total to pay</Text>
                                    <Text style={styles.highlight}>{totalPay}</Text>
                                </Left>
                            </ListItem>
                            <ListItem onPress={showSelectedMethod}>
                                <Left>
                                    <Text style={styles.textLabel}>Pay with</Text>
                                    <Text style={styles.highlight}>{payMethodValue}</Text>
                                </Left>
                            </ListItem>
                            <ListItem>
                                <Left>
                                    <Text style={styles.textLabel}>Total to recipient</Text>
                                    <Text style={styles.highlight}>{toRecipient}</Text>
                                </Left>
                            </ListItem>
                            {/* </Card> */}
                        </List>
                    </ScrollView>
                </View>

                <View style={{ height: '20%' }}>

                    <TouchableOpacity
                        onPress={showModal}
                    >
                        <View style={{ backgroundColor: '#576CA8', width: '90%', height: 50, alignSelf: 'center', alignItems: 'center', justifyContent: 'center', borderRadius: 30, marginBottom: 5, marginTop: 10 }}>
                            <Text style={{ color: 'white' }}>Confirm transfer</Text>
                        </View>
                    </TouchableOpacity>
                </View>

                
                <PaymentMethodModal visible={selectMethod} hideModal={hideSelectedMethod} setPaymentMethod={SetPayMethod} />
                <ConfirmPasswordModal visible={visible} _hideModal={hideModal} transaction_data={DATA} />

            </View>
        </>
    );
};

const styles = {
    container: {
        backgroundColor: '#EEEEEE',
        borderTopLeftRadius: 25,
        borderTopRightRadius: 25,
        width: '100%',
        height: '100%',
    },
    listItem: {
        borderWidth: 0.8,
        borderRadius: 8,
        marginLeft: 0,
        marginBottom: 10,
        paddingLeft: 10,
        flexDirection: 'column',
        backgroundColor: '#FFFFFF',
    },
    logo: {
        backgroundColor: '#EEEEEE',
        borderRadius: 50,
        marginVertical: 10,
        marginTop: 2,
        // marginRight: 10
    },
    logoSize: {
        resizeMode: 'cover',
        width: 40,
        height: 40,
        borderWidth: 1 / PixelRatio.get(),
        borderColor: '#eee',
        opacity: 0.8,
        // borderRadius: 25
        borderRadius: 5
    },
    highlight: {
        fontFamily: Constants.Bold,
        color: Constants.PrimaryColor,
        left: 0
    },
    highlight_: {
        fontFamily: Constants.Regular
    },
    highlight_m: {
        fontFamily: Constants.Regular
    },
    bodyStyle: {
        // flexDirection: 'row',
        justifyContent: 'center'
    },
    icon_: {
        color: "#FFFFFF",
        fontSize: 22
    },
    icon: {
        color: "#000000",
        fontSize: 20,
        // marginLeft: 20
    },
    cardItem: {
        borderRadius: 8
    },
    sectionTitle: {
        fontFamily: Constants.Bold,
        marginLeft: 5,
        fontSize: 13
    },
    textLabel: {
        fontFamily: Constants.Regular,
        marginRight: 10
    },
    textLabel_: {
        fontFamily: Constants.Regular
    }
};

export default TransactionModal;