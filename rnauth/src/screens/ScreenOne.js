import React from 'react';
import {StatusBar} from 'react-native';
import { Container, Header, Left, Body, Right, Title, Button } from 'native-base';
import Feather from 'react-native-vector-icons/Feather';
import { useNavigation } from '@react-navigation/native';
import Constants from '../config/Constants';
const ScreenOne = () => {
  const navigation = useNavigation();
  return (
    <Container>
      <Header backgroundColor={Constants.PrimaryColor} style={{backgroundColor: Constants.PrimaryColor}}>
        <Left>
          <Button transparent>
            <Feather name='chevron-left' style={styles.icon} onPress={() => { navigation.goBack() }} />
          </Button>
        </Left>
        <Body>
          <Title style={{fontFamily: Constants.Bold}}>FAQs</Title>
        </Body>
        <Right />
      </Header>
      <StatusBar backgroundColor={Constants.PrimaryColor} hidden={false} />
    </Container>
  );
};

const styles = {
  icon: {
    color: "#FFFFFF",
    fontSize: 25
  },
};

export default ScreenOne;