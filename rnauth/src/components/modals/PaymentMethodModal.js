import React, { useState, useEffect } from 'react';
import { Modal } from 'react-native-paper';
import { Alert, TouchableOpacity, PixelRatio, View, FlatList, Image } from 'react-native';
import { Row, List, ListItem, Text, Left, Right, Body } from 'native-base';
import Feather from 'react-native-vector-icons/Feather';
import Constants from '../../config/Constants';
import { TextInput, HelperText } from 'react-native-paper';
import rnauth_api from '../../api/rnauth_api';
import auth from '@react-native-firebase/auth';
import { useNavigation } from '@react-navigation/native';
import Spinner from 'react-native-loading-spinner-overlay';
import AsyncStorage from '@react-native-community/async-storage';
const app_logo = require('../../../assets/diamond.png');

const PaymentMethodModal = ({ visible, hideModal, setPaymentMethod }) => {
    const [spinner, setSpinner] = useState(false);
    const [paymethods, setPayMethods] = useState([]);
    const navigation = useNavigation();

    useEffect(() => {
        _loadPayMethod();
    }, []);

    const _loadPayMethod = async () => {
        setSpinner(true);
        try {
            let id = await AsyncStorage.removeItem('userid');
            // const response = await rnauth_api.get('/view_pay_methods.php');
            const response = await rnauth_api.post('/view_pay_methods.php',{"id": id == null? 1:id});
            if (response.data !== null) {
                // console.log("payment method data ", response.data);
                setPayMethods(response.data);
                setSpinner(false);
            }

        } catch (err) {
            Alert.alert("Sorry!", err);
            setSpinner(false);
        }
    };
    const containerStyle = {
        width: '90%',
        height: '70%',
        borderRadius: 15,
        backgroundColor: '#FFFFFF',
        position: 'absolute',
        paddingHorizontal: 5,
        top: -100,
        alignSelf: 'center'
    };

    return (
        <Modal visible={visible} onDismiss={hideModal} contentContainerStyle={containerStyle}>
            <Spinner
                visible={spinner}
                textContent={'Loading...'}
                textStyle={{ color: '#FFFFFF' }}
            />
            <View style={{ height: '10%', position: 'absolute', top: 8, right: 10, alignItems: 'center'}}>
                 <View style={{ justifyContent: 'flex-end', flexDirection: 'row', padding: 5, }}>
                 <Feather name="x" style={styles.iconClose} onPress={() => {hideModal()}} />
             </View>
            </View>
            <View style={{ height: '20%', justifyContent: 'center', padding: 10 }}>

                <Text style={styles.highlightTitle}>Select payment method</Text>

            </View>
            <View style={{ height: '50%', justifyContent: 'center' }}>
                <List>
                    <FlatList
                        data={paymethods}
                        // extraData={paymethods}
                        keyExtractor={item => item.user_pay_method_id}
                        renderItem={({ item }) => {
                            return (
                                <>
                                    {item.method !== 'Mobile Money' ? (
                                        <ListItem avatar style={styles.listItem}>
                                            <Row onPress={() => {
                                                let my_method = item.method + "..." + item.card_number.slice(-4);
                                                setPaymentMethod(my_method);
                                                hideModal();
                                            }}>
                                                <Body>
                                                    <Text style={styles.highlight}>{item.method} {"..." + item.card_number.slice(-4)}</Text>
                                                </Body>
                                                <Right>
                                                    <Feather name='chevron-right' style={styles.iconDelete} />
                                                </Right>
                                            </Row>
                                        </ListItem>
                                    ) : (
                                        <ListItem avatar style={styles.listItem}>
                                            <Row onPress={() => {
                                                let my_method = item.method + "..." + item.phone_number.slice(-4);
                                                setPaymentMethod(my_method);
                                                hideModal();
                                            }}>
                                                <Body>
                                                    <Text style={styles.highlight}>{item.method} {"..." + item.phone_number.slice(-4)}</Text>
                                                </Body>
                                                <Right>
                                                    <Feather name='chevron-right' style={styles.iconDelete} />
                                                </Right>
                                            </Row>
                                        </ListItem>
                                    )}
                                </>
                            )
                        }}
                    />
                </List>
            </View>
        </Modal>
    );
};

const styles = {
    container: {
        backgroundColor: '#EEEEEE'
    },
    listItem: {
        // marginTop: 10,
        // borderWidth: 0.8,
        // borderRadius: 8,
        marginLeft: 5,
        marginRight: 5,
        // marginBottom: 10,
        // paddingLeft: 10,
        flexDirection: 'column',
        backgroundColor: '#FFFFFF',
    },
    highlight: {
        // fontWeight: 'bold',
        fontFamily: Constants.Bold
    },
    highlightTitle: {
        fontFamily: Constants.Bold,
        color: Constants.PrimaryColor,
        alignSelf: 'center'
    },
    highlight_: {
        // fontWeight: '700',
        fontFamily: Constants.Regular
    },
    highlight_m: {
        // fontWeight: '700',
        fontFamily: Constants.Bold
    },
    bodyStyle: {
        // flexDirection: 'row',
        justifyContent: 'center'
    },
    icon: {
        color: "#FFFFFF",
        fontSize: 30
    },
    iconAdd: {
        color: Constants.PrimaryColor,
        fontSize: 30
    },
    iconDelete: {
        color: "black",
        fontSize: 20
    },
    title: {
        color: 'white',
        fontFamily: Constants.Bold,
        fontSize: 15
    },
    addButton: {
        margin: 5,
        padding: 5,
        backgroundColor: '#EEEEEE',
        borderRadius: 50,
    },
    imgStyle: {
        resizeMode: 'cover',
        width: 40,
        height: 40,
        borderWidth: 1 / PixelRatio.get(),
        borderColor: '#eee',
        opacity: 0.8,
        borderRadius: 25
    },
    iconClose: {
        color: "#000000",
        fontSize: 20
    },
};

export default PaymentMethodModal;