import React from 'react';
import {StatusBar, View} from 'react-native';
import {Container, Header, Left, Body, Right, Title, Button} from 'native-base';
import Feather from 'react-native-vector-icons/Feather';
import {useNavigation} from '@react-navigation/native';
import {WebView} from 'react-native-webview';
import Constants from '../config/Constants';

const WebviewComponent = ({route}) => {
  const navigation = useNavigation();
  return (
    <Container>
      <Header
        backgroundColor={Constants.PrimaryColor}
        style={{backgroundColor: Constants.PrimaryColor}}>
        <Left>
          <Button transparent>
            <Feather
              name="chevron-left"
              style={styles.icon}
              onPress={() => {
                navigation.goBack();
              }}
            />
          </Button>
        </Left>
        <Body>
          <Title style={styles.title}>{route.params.titleText}</Title>
        </Body>
        <Right />
      </Header>
      <StatusBar backgroundColor={Constants.PrimaryColor} hidden={false} />
      <View style={styles.container}>
        <WebView
          source={{
            uri: route.params.URL,
          }}
          style={{marginTop: 10}}
        />
      </View>
    </Container>
  );
};

const styles = {
  container: {
    flex: 1,
    padding: 5,
    margin: 5,
  },
  icon: {
    color: '#FFFFFF',
    fontSize: 30,
  },
  title: {
    color: 'white',
    fontFamily: Constants.Bold,
    fontSize: 18,
  },
};

export default WebviewComponent;
