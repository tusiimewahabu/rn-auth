import React, {useContext, useEffect, useState} from 'react';
import {
  Image,
  Alert,
  StatusBar,
  BackHandler,
} from 'react-native';
import {
  Container,
  Header,
  Content,
  Card,
  List,
  ListItem,
  Text,
  View,
  Body,
  Left,
  Right,
  Button,
} from 'native-base';
import Feather from 'react-native-vector-icons/Feather';
import {useNavigation, useFocusEffect} from '@react-navigation/native';
import auth from '@react-native-firebase/auth';
import AsyncStorage from '@react-native-community/async-storage';
import Communications from 'react-native-communications';
import Constants from '../../config/Constants';
import {SafeAreaView} from 'react-native-safe-area-context';
import {Context} from '../../context/utilsContext';

const app_logo = require('../../../assets/icon_512.png');
const app_url = 'http://51.38.70.212/app-backend';
const terms = 'https://transfara.com/terms';
const privacy_policy = 'https://transfara.com/privacy_policy';
const faqs_url = 'https://transfara.com/faqs';

const TabThree = () => {
  const navigation = useNavigation();
  const [full_name, setName] = useState('');

  const {state, backButtonHandller} = useContext(Context);
  useEffect(() => {
    load_user_name();
    backButtonHandller();
    console.log('check back button status:: ', state.backButtonStatus);
  }, [navigation]);

  const signOut = async () => {
    try {
      auth().signOut();
      await AsyncStorage.removeItem('phone');
      await AsyncStorage.removeItem('name');

      Alert.alert('Logged out successsfully');
      navigation.navigate('_login');
      // navigation.navigate('_intro');
      return true;
    } catch (exception) {
      return false;
    }
  };

  const load_user_name = async () => {
    let full_name = await AsyncStorage.getItem('name');
    setName(full_name != null ? full_name : 'Jane Doe');
  };

        // useFocusEffect get called each time when screen comes in focus
        useFocusEffect(
          React.useCallback(() => {
            const onBackPress = () => {
                navigation.navigate('_dashboard');
              // Return true to stop default back navigaton
              // Return false to keep default back navigaton
              return true;
            };
      
            // Add Event Listener for hardwareBackPress
            BackHandler.addEventListener(
              'hardwareBackPress',
              onBackPress
            );
      
            return () => {
              // Once the Screen gets blur Remove Event Listener
              BackHandler.removeEventListener(
                'hardwareBackPress',
                onBackPress
              );
            };
          }, []),
        );

  return (
    <SafeAreaView
      style={{backgroundColor: Constants.PrimaryColor, flex: 1}}
      forceInset={{top: 'never'}}>
      <Container style={styles.container}>
        <Header style={{backgroundColor: Constants.PrimaryColor}}>
          <Left>
            <Button transparent>
              <Feather name="settings" style={styles.icon_} />
            </Button>
          </Left>
          <Body>
            <Text
              style={{
                color: '#FFFFFF',
                fontSize: 20,
                fontFamily: Constants.Bold,
              }}>
              Settings
            </Text>
          </Body>
          <Right />
        </Header>

        <StatusBar backgroundColor={Constants.PrimaryColor} hidden={false} />

        <Content padder>
          <List>
            <ListItem
              avatar
              first
              style={styles.listItem}
              onPress={() => {
                navigation.navigate('_userProfile');
                BackHandler.removeEventListener('hardwareBackPress', () => {});
              }}>
              <View
                style={{flexDirection: 'row', padding: 5}}
                onPress={() => navigation.navigate('_userProfile')}>
                <Left>
                  <View style={styles.logo}>
                    <Image source={app_logo} style={styles.logoSize} />
                  </View>
                </Left>

                <View
                  style={{
                    justifyContent: 'flex-start',
                    alignSelf: 'center',
                    marginLeft: 5,
                  }}>
                  <Text style={styles.highlight} numberOfLines={2}>
                    {full_name}
                  </Text>
                </View>

                <Right>
                  <Feather
                    name="chevron-right"
                    style={styles.icon}
                    onPress={() => navigation.navigate('_userProfile')}
                  />
                </Right>
              </View>
            </ListItem>

            <Text note style={styles.sectionTitle}>
              HELP & SUPPORT
            </Text>

            <Card style={styles.cardItem}>
              <ListItem
                //   onPress={() => navigation.navigate('_screenOne')}
                onPress={() => {
                  // Communications.web(faqs_url);
                  navigation.navigate('_webview', {URL: faqs_url, titleText: 'FAQs'});
                }}>
                <Left>
                  <Text style={styles.textLabel}>FAQs</Text>
                </Left>
                <Right>
                  <Feather
                    name="chevron-right"
                    style={styles.icon}
                    // onPress={() => navigation.navigate('_screenOne')}
                    onPress={() => {
                      // Communications.web(faqs_url);
                      navigation.navigate('_webview', {URL: faqs_url, titleText: 'FAQs'});
                    }}
                  />
                </Right>
              </ListItem>
              <ListItem
                // onPress={() => navigation.navigate('_screenTwo')}
                onPress={() => Communications.text('+256393515226')}>
                <Left>
                  <Text style={styles.textLabel}>Send Us a Messsage</Text>
                </Left>
                <Right>
                  <Feather
                    name="chevron-right"
                    style={styles.icon}
                    // onPress={() => navigation.navigate('_screenTwo')}
                    onPress={() => Communications.text('+256393515226')}
                  />
                </Right>
              </ListItem>
              <ListItem
                onPress={() => Communications.phonecall('+256393515226', true)}>
                <Left>
                  <Text style={styles.textLabel}>Call Us</Text>
                </Left>
                <Right>
                  <Feather
                    name="chevron-right"
                    style={styles.icon}
                    onPress={() =>
                      Communications.phonecall('+256393515226', true)
                    }
                  />
                </Right>
              </ListItem>
            </Card>

            <Text note style={styles.sectionTitle}>
              PAYMENTS
            </Text>

            <Card style={styles.cardItem}>
              <ListItem onPress={() => navigation.navigate('_managePayments')}>
                <Left>
                  <Text style={styles.textLabel}>Manage Payments</Text>
                </Left>
                <Right>
                  <Feather
                    name="chevron-right"
                    style={styles.icon}
                    onPress={() => navigation.navigate('_managePayments')}
                  />
                </Right>
              </ListItem>
              <ListItem onPress={() => navigation.navigate('_changePassword')}>
                <Left>
                  <Text style={styles.textLabel}>Change Password</Text>
                </Left>
                <Right>
                  <Feather
                    name="chevron-right"
                    style={styles.icon}
                    onPress={() => navigation.navigate('_changePassword')}
                  />
                </Right>
              </ListItem>
            </Card>

            <Text note style={styles.sectionTitle}>
              LIMITS
            </Text>
            <Card style={styles.cardItem}>
              <ListItem onPress={() => navigation.navigate('_uploadDocuments')}>
                <Left>
                  <Text style={styles.textLabel}>Raise Limits</Text>
                </Left>
                <Right>
                  <Feather
                    name="chevron-right"
                    style={styles.icon}
                    onPress={() => navigation.navigate('_uploadDocuments')}
                  />
                </Right>
              </ListItem>
            </Card>

            <Text note style={styles.sectionTitle}>
              POLICY
            </Text>
            <Card style={styles.cardItem}>
              <ListItem
                onPress={() => {
                  //   Communications.web(privacy_policy);
                  navigation.navigate('_webview', {URL: privacy_policy, titleText: 'Privacy Policy'});
                }}>
                <Left>
                  <Text style={styles.textLabel}>Privacy</Text>
                </Left>
                <Right>
                  <Feather
                    name="chevron-right"
                    style={styles.icon}
                    onPress={() => {
                      // Communications.web(privacy_policy);
                      navigation.navigate('_webview', {URL: privacy_policy, titleText: 'Privacy Policy'});
                    }}
                  />
                </Right>
              </ListItem>
              <ListItem
                onPress={() => {
                  //   Communications.web(terms);
                  navigation.navigate('_webview', {URL: terms, titleText: 'Terms & Conditions'});
                }}>
                <Left>
                  <Text style={styles.textLabel}>Terms & Conditions</Text>
                </Left>
                <Right>
                  <Feather
                    name="chevron-right"
                    style={styles.icon}
                    onPress={() => {
                      // Communications.web(terms);
                      navigation.navigate('_webview', {URL: terms, titleText: 'Terms & Conditions'});
                    }}
                  />
                </Right>
              </ListItem>
            </Card>

            <Text note style={styles.sectionTitle}>
              EXIT
            </Text>
            <Card style={styles.cardItem}>
              <ListItem onPress={signOut}>
                <Left>
                  <Text style={styles.textLabel}>Log Out</Text>
                </Left>
                <Right>
                  <Feather
                    name="chevron-right"
                    style={styles.icon}
                    onPress={signOut}
                  />
                </Right>
              </ListItem>
            </Card>
          </List>
        </Content>
      </Container>
    </SafeAreaView>
  );
};
const styles = {
  container: {
    backgroundColor: '#EEEEEE',
  },
  listItem: {
    borderWidth: 0.8,
    borderRadius: 8,
    marginLeft: 0,
    marginBottom: 10,
    paddingLeft: 5,
    // flexDirection: 'column',
    backgroundColor: '#FFFFFF',
  },
  logo: {
    backgroundColor: '#EEEEEE',
    borderRadius: 50,
    // marginVertical: 10,
    // marginTop: 2
  },
  logoSize: {
    resizeMode: 'contain',
    height: 70,
    width: 70,
  },
  highlight: {
    fontFamily: Constants.Bold,
  },
  highlight_: {
    fontFamily: Constants.Regular,
  },
  highlight_m: {
    fontFamily: Constants.Regular,
  },
  bodyStyle: {
    // flexDirection: 'row',
    justifyContent: 'center',
  },
  icon_: {
    color: '#FFFFFF',
    fontSize: 22,
  },
  icon: {
    color: '#000000',
    fontSize: 25,
  },
  cardItem: {
    borderRadius: 8,
  },
  sectionTitle: {
    fontFamily: Constants.Bold,
    marginLeft: 5,
    fontSize: 13,
  },
  textLabel: {
    fontFamily: Constants.Regular,
  },
};
export default TabThree;
